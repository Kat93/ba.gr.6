const nextJest = require("next/jest");
const createJestConfig = nextJest({
  dir: "./",
});

  const customJestConfig = {
    moduleDirectories: ["node_modules", "<rootDir>/"],
    testEnvironment: "jest-environment-jsdom",
    collectCoverageFrom: ["src/**/*.js", "!**/node_modules/**"],
    coverageReporters: ["html", "text", "text-summary", "cobertura"],
    testMatch: ["**/*.test.js"]
  };
  module.exports = createJestConfig(customJestConfig);
  