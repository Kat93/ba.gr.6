# Welcome to Morningwood

This application is the result is the product of a bachelor project at UCL. It is a virtual forest that displays trees to represent the number of trees planted by Morningscore.


## Relavant links

[Staging site](https://staging-morningwood.netlify.app/https:/)


## How to run Morningwood locally

1. Clone the project repository.
   `git clone https://gitlab.com/Kat93/ba.gr.6.git`
2. Install dependencies
   `npm install`
3. Run application in development mode
   `npm run dev`


## How to run Storybook

Storybook is a component library that visualizes the UI elements of the application.
To run Storybook you must make sure dependencies are installed and then run:
    `npm run storybook`
