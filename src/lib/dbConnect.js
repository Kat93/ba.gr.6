import mongoose from "mongoose";

const ME_CONFIG_MONGODB_URL = `${"mongodb://root:example@mongo:27017/"}`;

let cached = global.mongoose;

if (!cached) {
  cached = global.mongoose = { conn: null, promise: null };
}

async function dbConnect() {
  console.log('######################### HERE IT STARTS #####################################')
  console.log(ME_CONFIG_MONGODB_URL);
  // console.log(cached.conn)
  console.log('cached.promise', !cached.promise)

  if (cached.conn) {
    return cached.conn;
  }

  // console.log('cached.promise', cached.promise)

  if (!cached.promise) {
    const opts = {
      bufferCommands: false,
    };

    cached.promise = mongoose
      .connect(ME_CONFIG_MONGODB_URL, opts)
      .then((mongoose) => {
        console.log('mong')
        return mongoose;
      });
      
  }
  console.log('hej')
  cached.conn = await cached.promise;
  return cached.conn;
}

export default dbConnect;
