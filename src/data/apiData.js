export const apiData =
{
totalTrees: 21584,
forests: [
{id: 1, name: "Lottes Butik", treesPlanted: 123, url:"lottes-butik"},
{id: 2, name: "Jysk Healer Service", treesPlanted: 345, url:"jysk-healer-service"},
{id: 3, name: "UCL", treesPlanted: 43221, url:"ucl"},
{id: 4, name: "Some Company", treesPlanted: 14443, url:"some-company"},
{id: 5, name: "Some other company", treesPlanted: 34, url:"ome-other-company"},
{id: 6, name: "Fynsk Healer Service", treesPlanted: 12, url:"fynsk-healer-service"},
{id: 7, name: "Kage 4 Dig", treesPlanted: 6, url:"kage-4-dig"},
{id: 8, name: "Candy shop", treesPlanted: 1000, url:"candy-shop"},
{id: 9, name: "Beer shop", treesPlanted: 436, url:"beer-shop"}
]
}