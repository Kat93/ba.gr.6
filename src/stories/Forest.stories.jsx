import Forest from '/src/components/partials/Forest.js'

export default {
  title: "Components/Forest/Forest",
  component: Forest,
};

const Template = args => <Forest {...args} />

export const forest = Template.bind({})
forest.args = {
    trees: 100,
    zoom: 1
}


export const smallForest = Template.bind({})
smallForest.args = {
  trees: 12,
  zoom: 1
}

export const mediumForest = Template.bind({})
mediumForest.args = {
  trees: 5326,
  zoom: 1
}

export const largeForest = Template.bind({})
largeForest.args = {
  trees: 75432,
  zoom: 1
}