import mongoose from "mongoose";

const ForestSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please give your forest a name"],
  },
  treesPlanted: {
    type: Number,
  },
});

export default mongoose.models.Forest ||
  mongoose.model("Forest", ForestSchema);
