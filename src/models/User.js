import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please give your user a name"],
  },
  slug: {
    type: String,
    required: [true, "Please make a slug"]
  },
  email: {
    type: String,
  },
  password: {
    type: String
  },
  treesPlanted: {
    type: Number
  }
});

export default mongoose.models.User ||
  mongoose.model("User", UserSchema);
