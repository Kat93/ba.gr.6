import Image from 'next/image'
import React, { useEffect, useRef, useState } from 'react'
import SpeechBubble from '../../ui/SpeechBubble/SpeechBubble'
import * as SpacemanFigure from '../../ui/svg/spaceman'
import Heading from '../../ui/text/Heading/Heading'
import Text from '../../ui/text/Text/Text'
import ButtonIcon from '../../ui/buttons/ButtonIcon/ButtonIcon'
import Button from '../../ui/buttons/Button/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark, faTree, faMagnifyingGlassPlus, faMagnifyingGlassMinus } from '@fortawesome/free-solid-svg-icons'

export default function Spaceman() {
  const [isWaving, setIsWaving] = useState(true)


  const handleClick = () => {
    setIsWaving(false)
  }

  useEffect(() => {
    if(isWaving) {
      document.addEventListener("mousedown", handleClick)
    }

    return () => document.removeEventListener("mousedown", handleClick)

  }, [isWaving])
  return isWaving ? (
    <>
      <SpeechBubble>
        <Heading headingLevel={"h4"} text={"Hi Captain"} />
        <Heading headingLevel={"h4"} text={"Let's explore Morningwood!"} />
        <Text text={"Use the forest switcher and zoom buttons in the left corner to navigate."} />
        <div>
          <ButtonIcon layout={"secondary"} border={"glowing"} size={"small"}><FontAwesomeIcon icon={faMagnifyingGlassPlus} /></ButtonIcon>
          <ButtonIcon layout={"secondary"} border={"glowing"} size={"small"}><FontAwesomeIcon icon={faMagnifyingGlassMinus} /></ButtonIcon>
          <ButtonIcon layout={"secondary"} border={"glowing"} size={"small"}><FontAwesomeIcon icon={faTree} /></ButtonIcon>
        </div>
      </SpeechBubble>
      <SpacemanFigure.Waving />
    </>
    
  ) : (
    <SpacemanFigure.Standing  />
  )
}
