import Tile from '../IsometricTile/IsometricTile'
import styles from './IsometricGrid.module.scss'
import useTreePlanter from '../../../hooks/useTreePlanter'
import Spaceman from '../Spaceman/Spaceman'
import PropTypes from 'prop-types'

export default function IsometricGrid({trees, zoom}) {
  const { plantedTrees } = useTreePlanter(trees)

  const zoomStyle ={
    transform:`scale(${zoom}) `,
    transition: `transform .5s ease-out`
    
  }

  const tiles = Array.from({ length: 100}, (_, i) => {
    return {id: i + 1, value: i * 10}
  })
  
  
  return (
    <div style={zoomStyle} className={styles.container}>
      <div className={styles.squeeze}>
        <div className={styles.grid}>
          {tiles.map((tile, index) => {
            const spacemanTile = plantedTrees?.length
            return (
              <Tile number={tile.id} key={tile.id}>
                {index !== spacemanTile + 8
                  ? plantedTrees[index]
                  : <Spaceman />
                }
              </Tile>
            )
          })}
        </div>
      </div>
    </div>
  )
}


IsometricGrid.propTypes = {
  trees: PropTypes.number,
  zoom: PropTypes.number
}