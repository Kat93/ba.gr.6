import PropTypes from 'prop-types'
import DecentTree from '../../ui/svg/trees/decentTree'
import GiantTree from '../../ui/svg/trees/giantTree'
import HugeTree from '../../ui/svg/trees/hugeTree'
import LargeTree from '../../ui/svg/trees/largeTree'
import MediumTree from '../../ui/svg/trees/mediumTree'
import SmallTree from '../../ui/svg/trees/smallTree'
import TinyTree from '../../ui/svg/trees/tinyTree'

export default function TreeSelector(props) {
    switch (props.value) {
		case 1 :
			return <TinyTree {...props} />
        case 5: 
            return <SmallTree {...props} />
        case 10:
            return <DecentTree {...props} />
        case 20:
            return <MediumTree {...props} />
        case 50:
            return <LargeTree {...props} />
        case 100: 
            return <HugeTree {...props} />
        case 1000: 
            return <GiantTree {...props} />
        default:
            return null
	}
}

TreeSelector.propTypes = {
    value: PropTypes.number // Number of planted trees the SVG tree represents. 
}