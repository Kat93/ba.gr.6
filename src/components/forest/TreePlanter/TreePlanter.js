import React from 'react'
import TreeSelector from '../TreeSelector/TreeSelector'


// If 2000 trees left: value = 1000
// If 500 trees left: value = 100
// If 400 trees left: value = 100
// If 300 trees left: value = 100
// If 200 trees left: value = 100
// If 100 trees left: value = 50
// If 50 trees left: value = 20
// If 30 trees left: value = 20
// If 10 trees left: value = 10
// If 9 trees left: value = 5
// If 4 trees left: value = 1


// If treesleft is equal to last value, lower the value

export default function TreePlanter() {
    const [trees, setTrees] = useState(props.trees)
  
    useEffect(() => {
      const renderTrees = []
      for (let treesLeft = trees; treesLeft > 0; treesLeft--) {
        let value = 0
        if(treesLeft >= 2000) {
          value = 1000
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 200) {
          value = 100
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 100) {
          value = 50
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 30) {
          value = 20
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 10) {
          value = 10
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 9) {
          value = 5
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 4) {
          value = 1
          treesLeft -= (value-1)
        }
        
        // console.log(`This tree is worth ${value} trees. Trees left ${treesLeft}`)
        renderTrees.push(<TreeSelector width={"50%"} value={value} />)
      }
      
      setTrees(renderTrees)
      
    }, [trees])
  
  return (
    {trees}
  )
}
