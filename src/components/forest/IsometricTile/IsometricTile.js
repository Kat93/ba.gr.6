import styles from './IsometricTile.module.scss'
import 'react-tippy/dist/tippy.css';
import { Tooltip } from 'react-tippy';
import React, { useState } from 'react'

export default function IsometricTile({children}) {
const [title, setTitle] = useState('text');

function addTextToToolTipBasedOnTree(event) {
  console.log(event.target.className)
  console.log('funktion kører')
  if (event.target.className.baseVal === "tiny-tree-1" | event.target.className.baseVal === "tiny-tree-2" | event.target.className.baseVal === "tiny-tree-3") {
    setTitle("1 tree")
    return 
  } 

  if (event.target.className.baseVal === "small-1" | event.target.className.baseVal === "small-2" | event.target.className.baseVal === "small-3" | event.target.className.baseVal === "small-4") {
    setTitle("5 trees")
    return 
  } 

  if (event.target.className.baseVal === "decent-1" | event.target.className.baseVal === "decent-2" | event.target.className.baseVal === "decent-3" | event.target.className.baseVal === "decent-4" | event.target.className.baseVal === "decent-5" | event.target.className.baseVal === "decent-6")  {
    setTitle("10 trees")
    return 
  } 

  if (event.target.className.baseVal === "medium-1" | event.target.className.baseVal === "medium-2" | event.target.className.baseVal === "medium-3"| event.target.className.baseVal === "medium-4") {
    setTitle("20 trees")
    return 
  } 

  if (event.target.className.baseVal === "large-1" | event.target.className.baseVal === "large-2" | event.target.className.baseVal === "large-3" | event.target.className.baseVal === "large-4" | event.target.className.baseVal === "large-5" | event.target.className.baseVal === "large-6" | event.target.className.baseVal === "large-7" | event.target.className.baseVal === "large-8" | event.target.className.baseVal === "large-9" | event.target.className.baseVal === "large-10") {
    setTitle("50 trees")
    return 
  } 

  if (event.target.className.baseVal === "huge-1" | event.target.className.baseVal === "huge-2" | event.target.className.baseVal === "huge-3"| event.target.className.baseVal === "huge-4") {
    setTitle("100 trees")
    return 
  } 

  if (event.target.className.baseVal === "giant-1" | event.target.className.baseVal === "giant-2" | event.target.className.baseVal === "giant-3"| event.target.className.baseVal === "giant-4" | event.target.className.baseVal === "giant-5" | event.target.className.baseVal === "giant-6" | event.target.className.baseVal === "giant-7" | event.target.className.baseVal === "giant-8" | event.target.className.baseVal === "giant-9" | event.target.className.baseVal === "giant-10" | event.target.className.baseVal === "giant-11") {
    setTitle("1000 trees")
    return 
  } 

  else {
    return  setTitle("not a tree")
  }  
} 

  return (
    <div className={styles.tile} onClick={addTextToToolTipBasedOnTree}>
      <Tooltip title={title} position="top" trigger="click" arrow="true" distance="25" theme="transparent">
      <div className={styles.content}>
        {children}
      </div>
      </Tooltip>
    </div>
  )
}