import React from 'react'
import Text from '../ui/text/Text/Text'

export default function FooterInfo() {
  return (
    <>
        <Text text={"2022 Morningscore."} /> 
        <Text text={"All rigths reserved."}  /> 
        <Text text={"info@morningscore.io " }  /> 
        <Text text={"+45 71 74 76 15."}/>
        <Text text={"Terms and Conditions"}  />
        <Text text={"Privacy policy"}  />	
    </>
  )
}
