import React from "react";
import ContentSection from "../layout/ContentSection/ContentSection";
import WithBackground from "../ui/backgrounds/WithBackground/WithBackground";
import Button from "../ui/buttons/Button/Button";
import IconBoxesRich from "../ui/IconBoxesRich/IconBoxesRich";
import Heading from "../ui/text/Heading/Heading";
import Text from "../ui/text/Text/Text";
import { BLOCKS, MARKS, INLINES } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { useRouter } from "next/router";

export default function MainContent(props) {
  const { 
    background,
    content,
    user = false
  } = props;

  const router = useRouter()
  const edenUrl = "https://edenprojects.org/"

  const { title, body, buttonText, secondTitle, cards } = content.fields;
  const userForestTitle = <h2>Welcome, {user.name}! You have planted <span>{user.treesPlanted} trees</span>.</h2>
  
  return (
    <>
      <WithBackground src={background}>
        <ContentSection>
          <>
            {user
            ? userForestTitle
            : documentToReactComponents(title, options)
            }
            {documentToReactComponents(body, options)}
            <Button onClick={() => router.push(edenUrl) } text={buttonText} />
          </>
          <>
            <Heading headingLevel={"h2"} text={secondTitle} />
            <IconBoxesRich cards={cards} />
          </>

        </ContentSection>
      </WithBackground>
    </>
  );
}

const style = {
  color: "#563AC9",
  fontStyle: "normal",
};

const Bold = ({ children }) => <span style={style}>{children}</span>;

const options = {
  renderMark: {
    [MARKS.ITALIC]: (text) => <Bold>{text}</Bold>,
  },
};
