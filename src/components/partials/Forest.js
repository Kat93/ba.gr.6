import React, { useEffect, useRef, useState } from 'react'
import IsometricGrid from '../forest/IsometricGrid/IsometricGrid'
import AnimatedFullscreenWrapper from '../layout/AnimatedFullscreenWrapper/AnimatedFullscreenWrapper'
import Controls from '../layout/Controls/Controls'
import ForestSwitcherButton from '../ui/buttons/ForestSwitchterButton/ForestSwitcherButton'
import ZoomButtons from '../ui/buttons/ZoomButtons/ZoomButtons'
import Legend from '../ui/Legend/Legend'
import TreeList from '../TreeList/TreeList'
import Draggable from 'react-draggable'
import ProgressBar from '../ui/ProgressBar/ProgressBar'
import FixedBottom from '../layout/FixedBottom/FixedBottom'
import { useSession } from 'next-auth/react'
import LoginButton from '../ui/buttons/LoginButton/LoginButton'
import AddTreeButton from '../ui/buttons/AddTreeButton/AddTreeButton'

export default function Forest(props) {
    const {
        trees,
    } = props
    
    const session = useSession()
    const [zoom, setZoom] = useState(1)
    const intervalRef = useRef(null)

    console.log(session)
    const startCounterZoomIn = () => {
      if (intervalRef.current) return;
      intervalRef.current = setInterval(() => {
        setZoom((prevZoom) => prevZoom + 0.1);
      }, 100);
    };
    const startCounterZoomOut = () => {
      if (intervalRef.current) return;
      intervalRef.current = setInterval(() => {
        setZoom((prevZoom) => prevZoom - 0.1);
      }, 100);
    };
    const stopCounter = () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
        intervalRef.current = null;
      }
    };

  return (
    <AnimatedFullscreenWrapper>
        <Legend>
            <TreeList />
        </Legend>
        <LoginButton loggedIn={session.status === "authenticated"} />
        <AddTreeButton/>
        <Draggable>
            <div>
                <IsometricGrid trees={trees} zoom={zoom} />
            </div>
        </Draggable>
        <Controls>
          <ZoomButtons 
            startCounterZoomIn={startCounterZoomIn}
            startCounterZoomOut={startCounterZoomOut} 
            stopCounter={stopCounter} 
            onZoomIn={() => setZoom(zoom + 0.1) }  
            onZoomOut={() => setZoom(zoom - 0.1) }   />  
            {session.status === "authenticated" && <ForestSwitcherButton forestUrl={session.data.user.slug} /> }      
        </Controls>
        <FixedBottom>
          <ProgressBar goal={1000000} treesPlanted={trees}/>  
        </FixedBottom>
    </AnimatedFullscreenWrapper>
  )
}
