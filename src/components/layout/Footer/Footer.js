import styles from "./Footer.module.scss";

export default function Footer(props) {
  const { layout = "small", children } = props;

  const layoutClass =
    layout === "small"
      ? styles.small
      : layout === "large"
      ? styles.large
      : null;

  return (
    <footer className={`${styles.footer} ${layoutClass}`}>{children}</footer>
  );
}
