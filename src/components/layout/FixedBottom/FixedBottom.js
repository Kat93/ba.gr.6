import styles from "./FixedBottom.module.scss";

export default function FixedBottom(props) {
  const { children } = props;

  return (
    <div className={`${styles.fixedBottom}`}>{children}</div>
  );
}