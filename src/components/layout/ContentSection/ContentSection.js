import Button from "../../ui/buttons/Button/Button"
import IconBox from "../../ui/IconBox/IconBox"
import styles from './ContentSection.module.scss'

export default function ContentSection({children}) {
    const [content, contributions] = children
    return (
        <section className={styles.section}>
            <div className={styles.content}>
                {content}
            </div>
            <div className={styles.contributions}>
                {contributions}
            </div>
        </section>
    )
}