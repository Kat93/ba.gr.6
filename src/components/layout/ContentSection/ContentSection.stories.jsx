import ContentSection from './ContentSection'


export default {
    title: "Components/Layout/ContentSection",
    component: ContentSection,
  };

  const childrenStyle = {
    backgroundColor: "purple",
    height: "150px",
    color: "white",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
  
  const Template = args => (
    <ContentSection {...args}>
        <div style={childrenStyle}>Upper content</div>
        <div style={childrenStyle}>Lower content</div>
    </ContentSection>
    )
  
  export const contentSection = Template.bind({})
  contentSection.args = {
  }
  