import AnimatedFullscreenWrapper from "./AnimatedFullscreenWrapper";
import "./AnimatedFullscreenWrapper.module.scss"

export default {
    title: "Components/Layout",
    component: AnimatedFullscreenWrapper
  };
  
  const Template = (args) => <AnimatedFullscreenWrapper {...args} />;

  export const AnimatedFullscreenWrapperWithColor = Template.bind({})
  AnimatedFullscreenWrapperWithColor.args ={
    background: "galaxy"
  }

