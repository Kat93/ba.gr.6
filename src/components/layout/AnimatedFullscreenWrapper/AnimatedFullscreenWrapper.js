import styles from "./AnimatedFullscreenWrapper.module.scss";

export default function AnimatedFullscreenWrapper({ children}) {

  return (
    <div className={styles["animated-wrapper"] }>
      {children}
      
    </div>
  );
}
