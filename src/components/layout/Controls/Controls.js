import style from './Controls.module.scss'
export default function Controls({children}) {
	return (
		<div className={style.controls}>
      		{children}
		</div>
	)
}
