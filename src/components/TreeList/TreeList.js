import DecentTree from "../ui/svg/trees/decentTree"
import GiantTree from "../ui/svg/trees/giantTree"
import HugeTree from "../ui/svg/trees/hugeTree"
import LargeTree from "../ui/svg/trees/largeTree"
import MediumTree from "../ui/svg/trees/mediumTree"
import SmallTree from "../ui/svg/trees/smallTree"
import TinyTree from "../ui/svg/trees/tinyTree"
import styles from './TreeList.module.scss'

export default function TreeList(props) {
  const trees = [
    {tree: <TinyTree height="100%" />, value: 1},
    {tree: <SmallTree height="100%" />, value: 5},
    {tree: <DecentTree height="100%" />, value: 10},
    {tree: <MediumTree height="100%" />, value: 20},
    {tree: <LargeTree height="100%" />, value: 50}, 
    {tree: <HugeTree height="100%" />, value: 100}, 
    {tree: <GiantTree height="100%" />, value: 1000} 
  ]
  return (
    <div className={styles.container}>
      {trees.map((tree) => {
        return (
          <div className={styles.tree} key={tree.value}>
            <div className={styles.icon}>
              {tree.tree} 
            </div>
            <div className={styles.value}>
              = {tree.value} {tree.value === 1 ? 'tree' : 'trees'}
            </div>
          </div> 
        )
      })}
    </div>
  )
}