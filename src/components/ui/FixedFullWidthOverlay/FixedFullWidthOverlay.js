import styles from './FixedFullWidthOverlay.module.scss'

export default function Overlay(props) {
  const {
    className, 
    children,
    zIndex
  } = props
  return (
    <div style={{zIndex}} className={`${styles.overlay} ${className}`}>
      {children}
    </div>
  )
}