import React from 'react'
import IconBox from '../IconBox/IconBox'
import * as Icons from '../icons'
import styles from './IconBoxesRich.module.scss'

export default function IconBoxesRich(props) {
    const {
        cards,
        iconHeight = "40px"
    } = props

    const iconMap = {
        environment: <Icons.Environment height={iconHeight} />,
        nature: <Icons.Nature height={iconHeight} />,
        local: <Icons.Local height={iconHeight} />
    }


    return (
        <div className={styles.container}>
            {cards.map((card) => {
                return <IconBox key={card.fields.icon} text={card.fields.text} icon={iconMap[card.fields.icon]} />
            })}
        </div>
    )
}
