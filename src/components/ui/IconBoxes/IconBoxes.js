import React from 'react'
import IconBox from '../IconBox/IconBox'
import * as Icons from '../icons'
import styles from './IconBoxes.module.scss'

export default function IconBoxes(props) {
    const {
        cards,
        iconHeight = "40px"
    } = props

    const iconMap = {
        environment: <Icons.Environment height={iconHeight} />,
        nature: <Icons.Nature height={iconHeight} />,
        local: <Icons.Local height={iconHeight} />
    }


    return (
        <div className={styles.container}>
            {cards.map((card) => {
                return <IconBox key={card.id} text={card.text} icon={iconMap[card.icon]} />
            })}
        </div>
    )
}
