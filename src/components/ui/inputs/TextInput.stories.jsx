import TextInput from "./TextInput";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "Components/UI/Inputs",
  component: TextInput,
  argTypes: {},
};

const Template = (args) => <TextInput {...args} />;
export const PrimaryTextInput = Template.bind({});
export const EmailInput = Template.bind({});
export const PasswordInput = Template.bind({});

PrimaryTextInput.args = {
  type: "text",
  placeholder: "Indtast venligst din text",
  labelText: "Text",
};

EmailInput.args = {
  type: "email",
  placeholder: "Indtast venligst din email",
  labelText: "Email",
  inputId: "email",
  autoComplete: "on",
};

PasswordInput.args = {
  type: "password",
  placeholder: "Indtast venligst dit password",
  labelText: "Password",
  inputId: "password",
};
