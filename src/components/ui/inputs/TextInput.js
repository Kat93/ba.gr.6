import styles from "./TextInput.module.scss";
import PropTypes from "prop-types";
import { useState } from "react";

export default function TextInput(props) {
  const {
    value,
    type,
    placeholder,
    inputId,
    maxLength,
    autoComplete,
    labelText,
    handleChange
  } = props;


  return (
    <>
      <div className={styles.inputField}>
        <label htmlFor={inputId}>{labelText}</label>
        <input
          maxLength={maxLength}
          id={inputId}
          placeholder={placeholder}
          value={value}
          type={type}
          autoComplete={autoComplete}
          onChange={(e) => handleChange(e.target.value)}
        />
      </div>
    </>
  );
}

TextInput.propTypes = {
  value: PropTypes.string,
  type: PropTypes.oneOf(["text", "email", "password"]),
  placeholder: PropTypes.string,
  maxLength: PropTypes.string,
  id: PropTypes.string,
  autoComplete: PropTypes.oneOf(["on", "off"]),
  labelText: PropTypes.string,
};
