import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import style from './Legend.module.scss'
import { useEffect, useRef, useState } from "react";
import Tree from "../icons/Tree";
import ButtonIcon from "../buttons/ButtonIcon/ButtonIcon";


export default function Legend({children}) {
    const [show, setShow] = useState(false)
    const legend = useRef(null)

    const toggleLegend = ()=>{
        setShow(!show)
    }

    useEffect(() => {
        if(show) {
            document.addEventListener('click', function clickOutside(e) {
                if(!legend.current.contains(e.target)) {
                    setShow(false)
                    document.removeEventListener('click', clickOutside)
                }
            })
        }
    }, [show])

    return (
        <div className={`${style.legend} legend`} ref={legend}>
            <ButtonIcon layout={"secondary"} border={"glowing"}  data-testid="button" handleClick={toggleLegend} className={style.icon} icon={faQuestionCircle}>
              <Tree />
              <FontAwesomeIcon  icon={show ? faChevronUp : faChevronDown} />
            </ButtonIcon>
            <div className={style.tableContainer} style={show ? {opacity: 1} : {opacity: 0, height: 0, overflow: "hidden"} }>
                {children}
            </div>
        </div>
    )
}