import Legend from "./Legend"
import { render} from '@testing-library/react'


test('the legend will show its children', () => {

//act
const {getByText} = render(
    <Legend>
    <div>
    test
    </div>
    </Legend>,
)

//assert

expect(getByText('test')).toBeTruthy()

})