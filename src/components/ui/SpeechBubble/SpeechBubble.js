import React from 'react'
import styles from './SpeechBubble.module.scss'

export default function SpeechBubble({children}) {
  return (
    <div className={styles.container}>
        <div className={styles.inner}>
            {children}
        </div>
    </div>
  )
}
