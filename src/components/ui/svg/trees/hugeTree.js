export default function HugeTree(props) {
  const {
    width = '100%',
    height = null
  } = props

  const css = `
    .huge-1 {
      fill: url(#Ikke-navngivet_forløb_18);
    }

    .huge-2 {
      fill: url(#Ikke-navngivet_forløb_56);
    }

    .huge-3 {
      fill: url(#Ikke-navngivet_forløb_55);
    }

    .huge-4 {
      fill: url(#Ikke-navngivet_forløb_56-2);
    }
  `
  return (
    <svg id="Lag_2" data-name="Lag 2" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 217.56 454.06">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="Ikke-navngivet_forløb_18" data-name="Ikke-navngivet forløb 18" x1="81.96" y1="355.96" x2="121.94" y2="356.53" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#0a0a4d"/>
      <stop offset=".2" stopColor="#30228b"/>
      <stop offset=".37" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".66" stopColor="#3b299d"/>
      <stop offset="1" stopColor="#0a0a4d"/>
    </linearGradient>
    <radialGradient id="Ikke-navngivet_forløb_56" data-name="Ikke-navngivet forløb 56" cx="-2703.35" cy="-213.6" fx="-2703.35" fy="-213.6" r="128.11" gradientTransform="translate(1726.19 372.59) scale(.61 .71)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fd585e"/>
      <stop offset=".13" stopColor="#f05566"/>
      <stop offset=".36" stopColor="#ce4d7c"/>
      <stop offset=".67" stopColor="#97429f"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
    <radialGradient id="Ikke-navngivet_forløb_56-2" data-name="Ikke-navngivet forløb 56" cx="-2708.84" cy="-323.92" fx="-2708.84" fy="-323.92" r="154.44" xlinkHref="#Ikke-navngivet_forløb_56"/>
    <radialGradient id="Ikke-navngivet_forløb_55" data-name="Ikke-navngivet forløb 55" cx="-2718.21" cy="-451.9" fx="-2718.21" fy="-451.9" r="184.07" gradientTransform="translate(1726.19 372.59) scale(.61 .71)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fd585e"/>
      <stop offset=".12" stopColor="#f35564"/>
      <stop offset=".32" stopColor="#d85075"/>
      <stop offset=".57" stopColor="#ad4690"/>
      <stop offset=".87" stopColor="#723ab6"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="huge-1" d="M85.1,448.79l14.74-190.11h18.47l6.78,190.11s-6.1,4.91-19.99,5.25c-13.89,.34-19.99-5.25-19.99-5.25Z"/>
      <path className="huge-2" d="M183.09,273.73c.47,30.36-151.41,24.27-151.88,0-.94-48.91,34-88.58,75.94-88.58s75.18,39.67,75.94,88.58Z"/>
      <path className="huge-4" d="M200.16,205.81c.04,36.87-182.65,31.79-182.7,0-.08-58.85,40.9-106.56,91.35-106.56s91.28,47.71,91.35,106.56Z"/>
      <path className="huge-3" d="M217.56,126.9C217.61,169.46,.05,167.45,0,126.9-.09,56.81,48.7,0,108.78,0s108.7,56.81,108.78,126.9Z"/>
    </g>
  </g>
</svg>
  )
}