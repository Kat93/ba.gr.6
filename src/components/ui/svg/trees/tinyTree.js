export default function TinyTree(props) {
  const { 
    width = '100%', 
    height = null
  } = props

  const css = `
  .tiny-tree-1 {
    fill: url(#tiny-tree-gradient_18);
  }

  .tiny-tree-2 {
    fill: url(#tiny-tree-gradient_12);
  }

  .tiny-tree-3 {
    fill: url(#tiny-tree-gradient_12-2);
  }
  `

  return (
    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 32.25 72.6">
      <defs>
        <style>
          {css}
        </style>
        <linearGradient id="tiny-tree-gradient_18" data-name="Ikke-navngivet forløb 18" x1="10.33" y1="51.14" x2="19.07" y2="51.26" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#0a0a4d"/>
          <stop offset=".2" stopColor="#30228b"/>
          <stop offset=".37" stopColor="#4b33b7"/>
          <stop offset=".45" stopColor="#563ac9"/>
          <stop offset=".66" stopColor="#3b299d"/>
          <stop offset="1" stopColor="#0a0a4d"/>
        </linearGradient>
        <radialGradient id="tiny-tree-gradient_12" data-name="Ikke-navngivet forløb 12" cx="10.88" cy="19.39" fx="10.88" fy="19.39" r="19.97" gradientTransform="translate(0 1.01) scale(1 1.16)" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#74ffee"/>
          <stop offset=".11" stopColor="#72f2eb"/>
          <stop offset=".31" stopColor="#6dd0e5"/>
          <stop offset=".57" stopColor="#6498db"/>
          <stop offset=".9" stopColor="#594dcd"/>
          <stop offset="1" stopColor="#5634c9"/>
        </radialGradient>
        <radialGradient id="tiny-tree-gradient_12-2" data-name="Ikke-navngivet forløb 12" cx="12.33" cy="23.47" fx="12.33" fy="23.47" r="13.42" gradientTransform="translate(0 -19.61) scale(1 1.15)" xlinkHref="#tiny-tree-gradient_12"/>
      </defs>
      <g id="Lag_1-2" data-name="Lag 1">
          <path className="tiny-tree-1" d="M11.01,71.44l3.22-41.59h4.04l1.48,41.59s-1.33,1.07-4.37,1.15-4.37-1.15-4.37-1.15Z"/>
          <ellipse className="tiny-tree-2" cx="16.12" cy="29.86" rx="16.12" ry="18.68"/>
          <path className="tiny-tree-3" d="M28.14,15.99c0,7.9-23.86,8.31-23.86,.41S9.1,0,16,0s12.15,8.08,12.15,15.99Z"/>
      </g>
    </svg>
  )
}