export default function LargeTree(props) {
  const {
    width = '100%',
    height = null
  } = props

  const css = `
    .large-1 {
      fill: url(#large_19);
    }

    .large-2 {
      fill: url(#large_18);
    }

    .large-3 {
      fill: url(#large_58);
    }

    .large-4 {
      fill: url(#large_58-3);
    }

    .large-5 {
      fill: url(#large_58-5);
    }

    .large-6 {
      fill: url(#large_58-2);
    }

    .large-7 {
      fill: url(#large_58-4);
    }

    .large-8 {
      fill: url(#large_19-4);
    }

    .large-9 {
      fill: url(#large_19-3);
    }

    .large-10 {
      fill: url(#large_19-2);
    }
  `
  return (
    <svg id="Lag_2" data-name="Lag 2" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 286.52 268.14">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="large_18" data-name="Ikke-navngivet forløb 18" x1="130.96" y1="157.44" x2="159.42" y2="157.85" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#0a0a4d"/>
      <stop offset=".2" stopColor="#30228b"/>
      <stop offset=".37" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".66" stopColor="#3b299d"/>
      <stop offset="1" stopColor="#0a0a4d"/>
    </linearGradient>
    <linearGradient id="large_19" data-name="Ikke-navngivet forløb 19" x1="188.61" y1="120.74" x2="205.4" y2="157.14" gradientUnits="userSpaceOnUse">
      <stop offset=".09" stopColor="#0a0a4d"/>
      <stop offset=".25" stopColor="#30228b"/>
      <stop offset=".38" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".52" stopColor="#3b299d"/>
      <stop offset=".63" stopColor="#0a0a4d"/>
    </linearGradient>
    <linearGradient id="large_19-2" data-name="Ikke-navngivet forløb 19" x1="4985.05" y1="139.08" x2="4997.66" y2="166.42" gradientTransform="translate(5099.96) rotate(-180) scale(1 -1)" xlinkHref="#large_19"/>
    <linearGradient id="large_19-3" data-name="Ikke-navngivet forløb 19" x1="125.5" y1="112.68" x2="141.38" y2="70.95" xlinkHref="#large_19"/>
    <linearGradient id="large_19-4" data-name="Ikke-navngivet forløb 19" x1="10836.49" y1="103.1" x2="10864.96" y2="28.31" gradientTransform="translate(11015.63) rotate(-180) scale(1 -1)" xlinkHref="#large_19"/>
    <radialGradient id="large_58" data-name="Ikke-navngivet forløb 58" cx="1865.57" cy="-1345.74" fx="1865.57" fy="-1345.74" r="54.51" gradientTransform="translate(-2776.84 1749.06) rotate(-8.19) scale(1.63 .9)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#0a77f3"/>
      <stop offset=".24" stopColor="#166beb"/>
      <stop offset=".67" stopColor="#384dd9"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
    <radialGradient id="large_58-2" data-name="Ikke-navngivet forløb 58" cx="9197.26" cy="70.31" fx="9197.26" fy="70.31" r="63.71" gradientTransform="translate(13595.78 927.18) rotate(-176.27) scale(1.46 -.84)" xlinkHref="#large_58"/>
    <radialGradient id="large_58-3" data-name="Ikke-navngivet forløb 58" cx="116.53" cy="-794.54" fx="116.53" fy="-794.54" r="68.07" gradientTransform="translate(39.29 591.88) rotate(-3.42) scale(1 .71)" xlinkHref="#large_58"/>
    <radialGradient id="large_58-4" data-name="Ikke-navngivet forløb 58" cx="1101.92" cy="-550.07" fx="1101.92" fy="-550.07" r="54.51" gradientTransform="translate(-1162.09 561.52) scale(1.21 .93)" xlinkHref="#large_58"/>
    <radialGradient id="large_58-5" data-name="Ikke-navngivet forløb 58" cx="1122.37" cy="-1448.07" fx="1122.37" fy="-1448.07" r="54.51" gradientTransform="translate(-1137.7 1595.35) rotate(-8.19) scale(1.25 .92)" xlinkHref="#large_58"/>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="large-2" d="M132.64,262.19l10.49-214.87h13.14l4.82,214.87s-4.34,5.55-14.23,5.94c-9.89,.38-14.23-5.94-14.23-5.94Z"/>
      <polygon className="large-1" points="214.37 129.09 151 148.84 154.64 163.64 222.86 134.46 234.38 103.4 228.15 94.39 214.37 129.09"/>
      <polygon className="large-10" points="99.08 144.34 141.53 161.02 139.82 172.52 91.8 150.59 82.14 122.69 87.99 120.57 99.08 144.34"/>
      <polygon className="large-9" points="146.8 101.71 121.6 89.58 111.94 89.58 132.57 101.71 146.8 115.41 146.8 101.71"/>
      <polygon className="large-8" points="155.37 82.31 178.94 64.44 187.97 64.44 168.67 82.31 155.37 102.52 155.37 82.31"/>
      <path className="large-3" d="M164.14,128.23C167.74,153.26,3.87,176.84,.26,151.81c-3.6-25.03,30.16-50.6,75.42-57.12,45.25-6.51,84.86,8.5,88.46,33.53Z"/>
      <path className="large-6" d="M112.8,113.21c-1.79,27.42,171.86,38.76,173.65,11.33,1.79-27.42-35.63-52.19-83.59-55.32-47.95-3.13-88.28,16.56-90.07,43.99Z"/>
      <path className="large-4" d="M207.62,41.41c1.48,24.87-125.5,32.45-126.98,7.58C79.16,24.11,106.38,2.25,141.45,.16s64.69,16.37,66.18,41.24Z"/>
      <path className="large-7" d="M254.34,74.92c0,26.18-123.38,26.18-123.38,0,0-26.18,27.62-47.4,61.69-47.4,34.07,0,61.69,21.22,61.69,47.4Z"/>
      <path className="large-5" d="M148.99,84.9c3.7,25.71-122.45,43.86-126.15,18.15s21.54-50.61,56.38-55.62c34.84-5.01,66.07,11.76,69.77,37.47Z"/>
    </g>
  </g>
</svg>
  )
}