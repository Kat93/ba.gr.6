export default function DecentTree(props) {

  const {
    width = '100%',
    height = null
  } = props

  const css = `
    .decent-1 {
      fill: url(#decent_19);
    }

    .decent-2 {
      fill: url(#decent_43);
    }

    .decent-3 {
      fill: url(#decent_18);
    }

    .decent-4 {
      fill: url(#decent_43-3);
    }

    .decent-5 {
      fill: url(#decent_43-2);
    }

    .decent-6 {
      fill: url(#decent_19-2);
    }
  `
  return (
    <svg id="Lag_2" data-name="Lag 2" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 100.68 131.68">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="decent_18" data-name="Ikke-navngivet forløb 18" x1="39.34" y1="90.84" x2="55.98" y2="91.08" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#0a0a4d"/>
      <stop offset=".2" stopColor="#30228b"/>
      <stop offset=".37" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".66" stopColor="#3b299d"/>
      <stop offset="1" stopColor="#0a0a4d"/>
    </linearGradient>
    <linearGradient id="decent_19" data-name="Ikke-navngivet forløb 19" x1="64.43" y1="51.42" x2="75.6" y2="75.63" gradientUnits="userSpaceOnUse">
      <stop offset=".09" stopColor="#0a0a4d"/>
      <stop offset=".25" stopColor="#30228b"/>
      <stop offset=".38" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".52" stopColor="#3b299d"/>
      <stop offset=".63" stopColor="#0a0a4d"/>
    </linearGradient>
    <radialGradient id="decent_43" data-name="Ikke-navngivet forløb 43" cx="36.99" cy="-418.67" fx="36.99" fy="-418.67" r="51.51" gradientTransform="translate(0 319.9) scale(1 .72)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#00ebaa"/>
      <stop offset=".12" stopColor="#06deac"/>
      <stop offset=".33" stopColor="#16bcb1"/>
      <stop offset=".62" stopColor="#2f85bb"/>
      <stop offset=".97" stopColor="#5339c8"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
    <radialGradient id="decent_43-2" data-name="Ikke-navngivet forløb 43" cx="2026.79" cy="-988.92" fx="2026.79" fy="-988.92" r="23.22" gradientTransform="translate(2111.85 1181.24) rotate(-180) scale(1 -1.16)" xlinkHref="#decent_43"/>
    <linearGradient id="decent_19-2" data-name="Ikke-navngivet forløb 19" x1="3815.76" y1="52.73" x2="3830.94" y2="85.65" gradientTransform="translate(3849.72) rotate(-180) scale(1 -1)" xlinkHref="#decent_19"/>
    <radialGradient id="decent_43-3" data-name="Ikke-navngivet forløb 43" cx="1771.13" cy="-604.15" fx="1771.13" fy="-604.15" r="23.22" gradientTransform="translate(714.28 -1723.85) rotate(90) scale(1 -1.16)" xlinkHref="#decent_43"/>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="decent-3" d="M40.64,129.49l6.14-79.14h7.69l.63,17.53,2.2,61.61s-2.54,2.05-8.32,2.19c-5.78,.14-8.32-2.19-8.32-2.19Z"/>
      <polygon className="decent-1" points="73.59 59.03 54.12 70.24 54.12 75.81 76.67 60.51 81.2 41.04 78.45 39.56 73.59 59.03"/>
      <ellipse className="decent-2" cx="50.52" cy="29.83" rx="41.59" ry="29.83"/>
      <ellipse className="decent-5" cx="78.97" cy="42.87" rx="18.75" ry="21.72" transform="translate(31.04 118.88) rotate(-86.27)"/>
      <polygon className="decent-6" points="21.51 63.07 47.98 78.32 47.98 85.9 17.32 65.08 11.15 38.61 14.89 36.59 21.51 63.07"/>
      <ellipse className="decent-4" cx="21.71" cy="53.38" rx="21.72" ry="18.75" transform="translate(-3.83 1.72) rotate(-4.17)"/>
    </g>
  </g>
</svg>
  )
}