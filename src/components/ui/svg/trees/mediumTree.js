export default function MediumTree(props) {

  const {
    width = '100%',
    height = null
  } = props

  const css = `
    .medium-1 {
      fill: url(#medium-gradient-1);
    }

    .medium-2 {
      fill: url(#medium-gradient-2);
    }

    .medium-3 {
      fill: url(#medium-gradient-3);
    }

    .medium-4 {
      fill: url(#medium-gradient-1-2);
    }
  `
  return (
    <svg id="Lag_2" data-name="Lag 2" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 203.42 228.23">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="medium-gradient-2" data-name="Ikke-navngivet forløb 18" x1="88.46" y1="172.45" x2="111.19" y2="172.78" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#0a0a4d"/>
      <stop offset=".2" stopColor="#30228b"/>
      <stop offset=".37" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".66" stopColor="#3b299d"/>
      <stop offset="1" stopColor="#0a0a4d"/>
    </linearGradient>
    <radialGradient id="medium-gradient-3" data-name="Ikke-navngivet forløb 42" cx="68.62" cy="-290.98" fx="68.62" fy="-290.98" r="108.86" gradientTransform="translate(0 301.94) scale(1 .71)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#c338e5"/>
      <stop offset=".18" stopColor="#b637e1"/>
      <stop offset=".51" stopColor="#9436d8"/>
      <stop offset=".94" stopColor="#5d34ca"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
    <radialGradient id="medium-gradient-1" data-name="Ikke-navngivet forløb 40" cx="76.38" cy="-347.23" fx="76.38" fy="-347.23" r="82.55" gradientTransform="translate(0 301.94) scale(1 .71)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#ea14f3"/>
      <stop offset=".14" stopColor="#dd16ef"/>
      <stop offset=".39" stopColor="#bb1ee5"/>
      <stop offset=".74" stopColor="#8429d6"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
    <radialGradient id="medium-gradient-1-2" data-name="Ikke-navngivet forløb 40" cx="85.79" cy="-228.53" fx="85.79" fy="-228.53" r="54.95" gradientTransform="translate(0 210.59) scale(1 .83)" xlinkHref="#medium-gradient-1"/>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="medium-2" d="M90.24,225.23l8.38-108.08h10.5l3.85,108.08s-3.47,2.79-11.37,2.99c-7.9,.19-11.37-2.99-11.37-2.99Z"/>
      <path className="medium-3" d="M203.42,134.09c0,39.84-203.42,39.84-203.42,0S45.54,61.94,101.71,61.94s101.71,32.3,101.71,72.14Z"/>
      <path className="medium-1" d="M178.6,84.87c0,30.21-154.25,30.21-154.25,0S58.88,30.17,101.47,30.17s77.13,24.49,77.13,54.71Z"/>
      <path className="medium-4" d="M153.83,42.83c0,23.65-102.68,23.65-102.68,0S74.14,0,102.5,0s51.34,19.17,51.34,42.83Z"/>
    </g>
  </g>
</svg>
  )
}