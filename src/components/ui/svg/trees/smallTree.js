export default function SmallTree(props) {
  const {
    width = '100%',
    height = null
  } = props

  const css = `
    .small-1 {
      fill: url(#small_18);
    }

    .small-2 {
      fill: url(#small_74);
    }

    .small-3 {
      fill: url(#small_74-2);
    }

    .small-4 {
      fill: url(#small_74-3);
    }  
  `
  return (
    <svg id="Lag_2" data-name="Lag 2" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 53.38 110.08">
      <defs>
        <style>
          {css}
        </style>
        <linearGradient id="small_18" data-name="Ikke-navngivet forløb 18" x1="17.75" y1="74.44" x2="32.28" y2="74.64" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#0a0a4d"/>
          <stop offset=".2" stopColor="#30228b"/>
          <stop offset=".37" stopColor="#4b33b7"/>
          <stop offset=".45" stopColor="#563ac9"/>
          <stop offset=".66" stopColor="#3b299d"/>
          <stop offset="1" stopColor="#0a0a4d"/>
        </linearGradient>
        <radialGradient id="small_74" data-name="Ikke-navngivet forløb 74" cx="23.6" cy="-369.92" fx="23.6" fy="-369.92" r="38.64" gradientTransform="translate(211.56 264.07) rotate(-27.25) scale(.91 .75) skewX(14.82)" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#e596ed"/>
          <stop offset=".13" stopColor="#de91eb"/>
          <stop offset=".32" stopColor="#cd85e6"/>
          <stop offset=".53" stopColor="#af71df"/>
          <stop offset=".76" stopColor="#8755d5"/>
          <stop offset="1" stopColor="#5634c9"/>
        </radialGradient>
        <radialGradient id="small_74-2" data-name="Ikke-navngivet forløb 74" cx="19.16" cy="-392.88" fx="19.16" fy="-392.88" r="38.64" gradientTransform="translate(347.33 317.39) rotate(-32.42) scale(.89 1.06) skewX(21.95)" xlinkHref="#small_74"/>
        <radialGradient id="small_74-3" data-name="Ikke-navngivet forløb 74" cx="18.94" cy="-419.01" fx="18.94" fy="-419.01" r="38.64" gradientTransform="translate(370.06 316.99) rotate(-32.42) scale(.89 1.06) skewX(21.95)" xlinkHref="#small_74"/>
      </defs>
      <g id="Lag_1-2" data-name="Lag 1">
        <g>
          <path className="small-1" d="M18.89,108.16l5.36-69.06h6.71l2.46,69.06s-2.22,1.79-7.26,1.91-7.26-1.91-7.26-1.91Z"/>
          <path className="small-2" d="M53.37,58.58c0,10.53-11.9,14.59-26.58,14.59S.22,69.11,.22,58.58s11.9-19.06,26.58-19.06,26.58,8.53,26.58,19.06Z"/>
          <path className="small-3" d="M53.38,37.8c0,10.53-11.9,14.59-26.58,14.59S.23,48.33,.23,37.8,12.13,18.74,26.8,18.74s26.58,8.53,26.58,19.06Z"/>
          <path className="small-4" d="M53.16,19.06c0,10.53-11.9,14.59-26.58,14.59S0,29.59,0,19.06,11.9,0,26.58,0s26.58,8.53,26.58,19.06Z"/>
        </g>
      </g>
    </svg>
  )
}