export default function GiantTree(props) {
  const {
    width = '100%',
    height = null
  } = props

  const css = `
  .giant-1 {
    fill: url(#giant_19);
  }

  .giant-2 {
    fill: url(#giant_18);
  }

  .giant-3 {
    fill: url(#giant_50);
  }

  .giant-4 {
    fill: url(#giant_50-7);
  }

  .giant-5 {
    fill: url(#giant_50-6);
  }

  .giant-6 {
    fill: url(#giant_50-4);
  }

  .giant-7 {
    fill: url(#giant_50-5);
  }

  .giant-8 {
    fill: url(#giant_50-2);
  }

  .giant-9 {
    fill: url(#giant_50-3);
  }

  .giant-10 {
    fill: url(#giant_19-3);
  }

  .giant-11 {
    fill: url(#giant_19-2);
  }
  `
  return (
    <svg id="Lag_2" data-name="Lag 2" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={width} height={height} viewBox="0 0 540.6 746.65">
  <defs>
    <style>
      {css}
    </style>
    <radialGradient id="giant_50" data-name="Ikke-navngivet forløb 50" cx="-1675.93" cy="-626.85" fx="-1675.93" fy="-626.85" r="148.37" gradientTransform="translate(1444.33 504.54) scale(.72)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#f1c227"/>
      <stop offset=".13" stopColor="#e4b633"/>
      <stop offset=".37" stopColor="#c49855"/>
      <stop offset=".68" stopColor="#8f698c"/>
      <stop offset="1" stopColor="#5634c9"/>
    </radialGradient>
    <radialGradient id="giant_50-2" data-name="Ikke-navngivet forløb 50" cx="-1935.99" cy="-430.54" fx="-1935.99" fy="-430.54" r="148.37" xlinkHref="#giant_50"/>
    <radialGradient id="giant_50-3" data-name="Ikke-navngivet forløb 50" cx="-1576.32" cy="-568.77" fx="-1576.32" fy="-568.77" r="148.2" xlinkHref="#giant_50"/>
    <linearGradient id="giant_18" data-name="Ikke-navngivet forløb 18" x1="221.2" y1="496.04" x2="309.5" y2="497.31" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#0a0a4d"/>
      <stop offset=".2" stopColor="#30228b"/>
      <stop offset=".37" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".66" stopColor="#3b299d"/>
      <stop offset="1" stopColor="#0a0a4d"/>
    </linearGradient>
    <linearGradient id="giant_19" data-name="Ikke-navngivet forløb 19" x1="347.85" y1="280.06" x2="414.06" y2="423.58" gradientUnits="userSpaceOnUse">
      <stop offset=".09" stopColor="#0a0a4d"/>
      <stop offset=".25" stopColor="#30228b"/>
      <stop offset=".38" stopColor="#4b33b7"/>
      <stop offset=".45" stopColor="#563ac9"/>
      <stop offset=".52" stopColor="#3b299d"/>
      <stop offset=".63" stopColor="#0a0a4d"/>
    </linearGradient>
    <linearGradient id="giant_19-2" data-name="Ikke-navngivet forløb 19" x1="6572.1" y1="358.34" x2="6621.78" y2="466.02" gradientTransform="translate(6784.1) rotate(-180) scale(1 -1)" xlinkHref="#giant_19"/>
    <radialGradient id="giant_50-4" data-name="Ikke-navngivet forløb 50" cx="-1445.32" cy="-455.24" fx="-1445.32" fy="-455.24" r="171.33" xlinkHref="#giant_50"/>
    <linearGradient id="giant_19-3" data-name="Ikke-navngivet forløb 19" x1="9118.59" y1="122.39" x2="9184.8" y2="265.92" gradientTransform="translate(9346.27) rotate(-180) scale(1 -1)" xlinkHref="#giant_19"/>
    <radialGradient id="giant_50-5" data-name="Ikke-navngivet forløb 50" cx="-1804.75" cy="-542.32" fx="-1804.75" fy="-542.32" r="214.37" xlinkHref="#giant_50"/>
    <radialGradient id="giant_50-6" data-name="Ikke-navngivet forløb 50" cx="-1634.87" cy="-366.15" fx="-1634.87" fy="-366.15" r="187.66" xlinkHref="#giant_50"/>
    <radialGradient id="giant_50-7" data-name="Ikke-navngivet forløb 50" cx="-1863.92" cy="-275.63" fx="-1863.92" fy="-275.63" r="148.37" xlinkHref="#giant_50"/>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <ellipse className="giant-3" cx="272.04" cy="85.65" rx="85.8" ry="85.65"/>
      <ellipse className="giant-8" cx="85.8" cy="225.99" rx="85.8" ry="85.65"/>
      <ellipse className="giant-9" cx="343.35" cy="127.14" rx="85.71" ry="85.56"/>
      <path className="giant-2" d="M227.67,733.18l32.56-485.92h40.79l14.97,485.92s-13.47,12.56-44.16,13.43c-30.69,.87-44.16-13.43-44.16-13.43Z"/>
      <polygon className="giant-1" points="409 315.09 284.64 376.38 291.78 422.33 425.66 331.75 448.27 235.36 436.04 207.4 409 315.09"/>
      <polygon className="giant-11" points="172.78 379.35 261.43 431.12 257.86 466.83 157.57 398.75 137.4 312.16 149.63 305.57 172.78 379.35"/>
      <ellipse className="giant-6" cx="441.51" cy="212.86" rx="99.08" ry="98.91"/>
      <polygon className="giant-10" points="173.4 167.47 288.81 233.96 288.81 267 155.13 176.25 128.24 60.84 144.55 52.06 173.4 167.47"/>
      <ellipse className="giant-7" cx="192.2" cy="159.09" rx="123.97" ry="123.75"/>
      <ellipse className="giant-5" cx="308.84" cy="279.77" rx="108.53" ry="108.33"/>
      <ellipse className="giant-4" cx="137.41" cy="336.74" rx="85.8" ry="85.65"/>
    </g>
  </g>
</svg>
  )
}