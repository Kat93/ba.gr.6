import React from 'react'

function Wondering (props) {
  const {
    className,
    width = '100%',
    height = '100%'
  } = props


  const css = `
  .spaceman-wondering-1 {
    fill: url(#Ikke-navngivet_forløb_104);
  }

  .spaceman-wondering-2 {
    fill: url(#Ikke-navngivet_forløb_115);
  }

  .spaceman-wondering-3 {
    fill: url(#Ikke-navngivet_forløb_128);
  }

  .spaceman-wondering-4 {
    fill: url(#Ikke-navngivet_forløb_133);
  }

  .spaceman-wondering-5 {
    fill: #676a82;
  }

  .spaceman-wondering-6 {
    fill: #e6e6e6;
  }

  .spaceman-wondering-7 {
    fill: #fff;
  }

  .spaceman-wondering-8 {
    fill: #999;
  }

  .spaceman-wondering-9 {
    fill: #b3b3b3;
  }

  .spaceman-wondering-10 {
    fill: #32345c;
  }

  .spaceman-wondering-11 {
    fill: #a2f4f0;
  }

  .spaceman-wondering-12 {
    fill: #bfbfbf;
  }

  .spaceman-wondering-13 {
    fill: #0d064e;
  }

  .spaceman-wondering-14 {
    fill: #6f718d;
  }

  .spaceman-wondering-15 {
    fill: #563ac9;
  }

  .spaceman-wondering-16 {
    fill: #63667f;
  }

  .spaceman-wondering-17 {
    fill: #5d6075;
  }

  .spaceman-wondering-18 {
    fill: url(#gråligt_forløb-2);
  }

  .spaceman-wondering-19 {
    fill: url(#gråligt_forløb);
  }

  .spaceman-wondering-20 {
    fill: url(#Ikke-navngivet_forløb_115-2);
  }

  .spaceman-wondering-21 {
    fill: url(#Ikke-navngivet_forløb_11);
  }

  .spaceman-wondering-22 {
    fill: url(#Ikke-navngivet_forløb_32);
  }

  .spaceman-wondering-23 {
    fill: url(#Ikke-navngivet_forløb_20);
  }

  .spaceman-wondering-24 {
    fill: url(#Ikke-navngivet_forløb_25);
  }

  .spaceman-wondering-25 {
    fill: url(#Ikke-navngivet_forløb_26);
  }

  .spaceman-wondering-26 {
    fill: url(#Ikke-navngivet_forløb_96);
  }

  .spaceman-wondering-27 {
    fill: url(#Ikke-navngivet_forløb_98);
  }

  .spaceman-wondering-28 {
    fill: url(#Ikke-navngivet_forløb_73);
  }

  .spaceman-wondering-29 {
    fill: url(#Ikke-navngivet_forløb_71);
  }

  .spaceman-wondering-30 {
    fill: url(#Ikke-navngivet_forløb_51);
  }

  .spaceman-wondering-31 {
    fill: url(#Ikke-navngivet_forløb_88);
  }

  .spaceman-wondering-32 {
    opacity: .45;
  }

  .spaceman-wondering-33 {
    opacity: .72;
  }

  .spaceman-wondering-34 {
    fill: url(#Ikke-navngivet_forløb_51-2);
  }

  .spaceman-wondering-35 {
    fill: url(#Ikke-navngivet_forløb_11-2);
  }

  .spaceman-wondering-36 {
    fill: url(#Ikke-navngivet_forløb_71-2);
  }
  `

  return (
    <svg className={className} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox="0 0 554.88 802.06">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="Ikke-navngivet_forløb_133" data-name="Ikke-navngivet forløb 133" x1="389.28" y1="444.16" x2="389.28" y2="423.48" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#e6e6e6"/>
      <stop offset=".49" stopColor="#e4e4e4"/>
      <stop offset=".64" stopColor="#ddd"/>
      <stop offset=".74" stopColor="#d1d1d1"/>
      <stop offset=".83" stopColor="silver"/>
      <stop offset=".9" stopColor="#aaa"/>
      <stop offset=".97" stopColor="#8f8f8f"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_71" data-name="Ikke-navngivet forløb 71" x1="398.36" y1="423.5" x2="398.36" y2="369.84" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#ccc"/>
      <stop offset=".51" stopColor="#cacaca"/>
      <stop offset=".67" stopColor="#c3c3c3"/>
      <stop offset=".78" stopColor="#b7b7b7"/>
      <stop offset=".88" stopColor="#a6a6a6"/>
      <stop offset=".95" stopColor="#909090"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_115" data-name="Ikke-navngivet forløb 115" x1="823.56" y1="1271.06" x2="680.35" y2="1271.06" gradientTransform="translate(-811.35 -203.99) rotate(-20.9)" gradientUnits="userSpaceOnUse">
      <stop offset=".3" stopColor="#fff"/>
      <stop offset=".5" stopColor="#fcfcfc"/>
      <stop offset=".65" stopColor="#f2f2f2"/>
      <stop offset=".78" stopColor="#e2e2e2"/>
      <stop offset=".9" stopColor="#ccc"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_128" data-name="Ikke-navngivet forløb 128" x1="904.29" y1="1247.69" x2="797.88" y2="1198.67" gradientTransform="translate(-845.68 -214.9) rotate(-21.66)" gradientUnits="userSpaceOnUse">
      <stop offset=".21" stopColor="#ccc"/>
      <stop offset=".24" stopColor="#d2d2d2"/>
      <stop offset=".37" stopColor="#e5e5e5"/>
      <stop offset=".52" stopColor="#f4f4f4"/>
      <stop offset=".7" stopColor="#fcfcfc"/>
      <stop offset="1" stopColor="#fff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_104" data-name="Ikke-navngivet forløb 104" x1="291.54" y1="625.6" x2="351.54" y2="625.6" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#999"/>
      <stop offset=".05" stopColor="#a1a1a1"/>
      <stop offset=".27" stopColor="#bfbfbf"/>
      <stop offset=".49" stopColor="#d4d4d4"/>
      <stop offset=".69" stopColor="#e1e1e1"/>
      <stop offset=".89" stopColor="#e6e6e6"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_20" data-name="Ikke-navngivet forløb 20" x1="220.51" y1="635.63" x2="322.67" y2="635.63" gradientUnits="userSpaceOnUse">
      <stop offset=".29" stopColor="#ccc"/>
      <stop offset=".3" stopColor="#cdcdcd"/>
      <stop offset=".41" stopColor="#e3e3e3"/>
      <stop offset=".54" stopColor="#f2f2f2"/>
      <stop offset=".68" stopColor="#fcfcfc"/>
      <stop offset=".89" stopColor="#fff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_26" data-name="Ikke-navngivet forløb 26" x1="174.03" y1="640.47" x2="247.45" y2="640.47" gradientUnits="userSpaceOnUse">
      <stop offset=".29" stopColor="#999"/>
      <stop offset=".89" stopColor="#fff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_25" data-name="Ikke-navngivet forløb 25" x1="376.72" y1="504.9" x2="220.51" y2="504.9" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".23" stopColor="#e4e4e4"/>
      <stop offset=".75" stopColor="#aeaeae"/>
      <stop offset="1" stopColor="#999"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_51" data-name="Ikke-navngivet forløb 51" x1="383.24" y1="459.02" x2="237.26" y2="459.02" gradientUnits="userSpaceOnUse">
      <stop offset=".15" stopColor="#fff"/>
      <stop offset=".32" stopColor="#f9f9f9"/>
      <stop offset=".54" stopColor="#eaeaea"/>
      <stop offset=".78" stopColor="#d1d1d1"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="gråligt_forløb" data-name="gråligt forløb" x1="145.16" y1="495.5" x2="62.32" y2="495.5" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".24" stopColor="#ededed"/>
      <stop offset=".71" stopColor="#c1c1c1"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="gråligt_forløb-2" data-name="gråligt forløb" x1="361.83" y1="406.6" x2="24.74" y2="406.6" xlinkHref="#gråligt_forløb"/>
    <linearGradient id="Ikke-navngivet_forløb_96" data-name="Ikke-navngivet forløb 96" x1="411.41" y1="339.09" x2="0" y2="339.09" gradientUnits="userSpaceOnUse">
      <stop offset=".04" stopColor="#ccc"/>
      <stop offset=".11" stopColor="#e3e3e3"/>
      <stop offset=".19" stopColor="#f7f7f7"/>
      <stop offset=".24" stopColor="#fff"/>
      <stop offset=".72" stopColor="#ccc"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_73" data-name="Ikke-navngivet forløb 73" x1="18.33" y1="270.84" x2="310.92" y2="196.43" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".28" stopColor="#e1e1e1"/>
      <stop offset=".65" stopColor="#bfbfbf"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_32" data-name="Ikke-navngivet forløb 32" x1="75.44" y1="191.07" x2="435.1" y2="191.07" gradientUnits="userSpaceOnUse">
      <stop offset=".17" stopColor="#563ac9"/>
      <stop offset=".34" stopColor="#6b56d2"/>
      <stop offset=".71" stopColor="#a19eeb"/>
      <stop offset=".98" stopColor="#ccd7ff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_11" data-name="Ikke-navngivet forløb 11" x1="63.64" y1="148.67" x2="155.04" y2="125.42" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".35" stopColor="#d9d9d9"/>
      <stop offset=".66" stopColor="#bdbdbd"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_11-2" data-name="Ikke-navngivet forløb 11" x1="181.2" y1="368.96" x2="321.21" y2="333.35" xlinkHref="#Ikke-navngivet_forløb_11"/>
    <linearGradient id="Ikke-navngivet_forløb_51-2" data-name="Ikke-navngivet forløb 51" x1="361.49" y1="535.27" x2="338.73" y2="535.27" xlinkHref="#Ikke-navngivet_forløb_51"/>
    <linearGradient id="Ikke-navngivet_forløb_88" data-name="Ikke-navngivet forløb 88" x1="160.21" y1="567.61" x2="160.21" y2="426.4" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#ccc"/>
      <stop offset=".39" stopColor="#adadad"/>
      <stop offset=".78" stopColor="#8c8c8c"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_98" data-name="Ikke-navngivet forløb 98" x1="189.96" y1="519.92" x2="189.96" y2="437.29" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#e6e6e6"/>
      <stop offset=".41" stopColor="#bebebe"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_71-2" data-name="Ikke-navngivet forløb 71" x1="202.37" y1="558.49" x2="202.37" y2="499.3" xlinkHref="#Ikke-navngivet_forløb_71"/>
    <linearGradient id="Ikke-navngivet_forløb_115-2" data-name="Ikke-navngivet forløb 115" x1="316.95" y1="747.03" x2="145.16" y2="747.03" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlinkHref="#Ikke-navngivet_forløb_115"/>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="spaceman-wondering-6" d="M430.48,441.02s20.18,21.33,43.24,28.09c23.07,6.76,36.41,8.59,60.44,0,24.02-8.59,24.02-28.13,15.65-40.69-8.37-12.56-38.8-21.32-46.61-21.82-7.81-.51-34.32-6.09-34.32-6.09l-29.66-6.3-11.1,22.52,2.36,24.29Z"/>
      <path className="spaceman-wondering-6" d="M500.26,360.81s-7.87-2.03-14.74,0c-6.87,2.03-15.03,9.63-22.11,16.99-7.08,7.36-15.45,12.94-18.24,12.94h-7.32l-1.38,3.46,40.22,8.98,8.94,1.51s5.83-4.19,9.2-9.77,8.94-18.14,8.94-20.93,1.4-7.75,0-9.46-3.51-3.73-3.51-3.73Z"/>
      <path className="spaceman-wondering-12" d="M471.91,408.25s26.28,3.42,34.65,6.22c8.37,2.79,29.3,8.37,32.09,18.14,2.79,9.77-1.4,18.14-2.79,19.53s-8.37,9.77-25.12,9.77-34.41-6.16-38.84-9.36c-4.42-3.2-20.1-16.39-22.03-17.47-1.93-1.08,7.91-24.67,10.66-26.83s11.36,0,11.36,0Z"/>
      <path className="spaceman-wondering-4" d="M375.35,445.84s9.81,2.11,14,2.11,15.52-1.06,15.52-1.06l1.34-15.07-10.4-6.21h-12.58l-10.9,4.25,3.02,15.97Z"/>
      <path className="spaceman-wondering-29" d="M391.94,375.39s23.43,6.41,28.56,11.57c5.13,5.17-14.28,44.85-14.28,44.85,0,0-12.78,3.41-23.9,0-11.11-3.41-5.61-36.89-5.61-36.89l15.22-19.53Z"/>
      <path className="spaceman-wondering-2" d="M364.48,680.98s9.01,4.03,12.01,5.37c3,1.34,17.47,5.6,21.7,7.9,4.23,2.3,11.6,4.29,18.59,12.82,7,8.54,7.21,22.15,7.21,22.15,0,0,.96,5.78-32.6,12.45-33.56,6.67-87.23,3.1-95.56,.84s-20.97-9.27-21.49-10.62c-.52-1.36-2.72-13.18-2.51-19.38,.21-6.2,8.98-20.15,8.98-20.15l83.66-11.39Z"/>
      <path className="spaceman-wondering-12" d="M272.42,721.09s-2.16,3.35-2,7.02,1.4,6.94,1.4,6.94c0,0,19.69,13.44,33.71,13.28,14.02-.17,44.83,6.54,67.14,3,22.31-3.54,51.08-19.51,51.08-19.51,0,0,2.69-6.01,1.03-10.35l-1.66-4.35s-33.68,17.41-55.99,18.68c-22.31,1.27-56.77,.33-67.34-3.53s-27.36-11.17-27.36-11.17Z"/>
      <polygon className="spaceman-wondering-13" points="378.29 659.81 364.5 655.78 351.4 673.12 369.45 669.77 378.29 659.81"/>
      <path className="spaceman-wondering-3" d="M346.04,522.9s-5.21,5.55,4.37,14.78c9.58,9.23,33.16,38.11,39.77,54.77s-7.2,49.91-12.75,58.62c-5.55,8.71-15.6,21.2-15.6,21.2l-50.62,12.89,33.88-55.03-46.81-76.26,47.75-30.98Z"/>
      <path className="spaceman-wondering-1" d="M322.67,572.14s24.48,23.94,27.44,32.9c2.96,8.96,1.02,28.93-2.89,34.29-3.91,5.37-24.84,38.17-24.84,38.17l-30.83,1.56s19.84-37.15,21.85-43.43c2.01-6.28-17.85-63.27-17.85-63.27l27.13-.22Z"/>
      <path className="spaceman-wondering-14" d="M291.74,670.32s12.84,7.35,27.55,6.94c14.71-.42,38.74-7.01,45.79-8.48,7.04-1.47,13.21-8.97,13.21-8.97,0,0,8.68,6.71,9.14,12.39,.46,5.67-25.3,20.52-41.06,22.12-15.76,1.61-43.07,1.17-54.05-2.95-10.97-4.12-12.16-9.16-12.16-9.16,0,0-2.57-10.75,1.18-13.94s10.4,2.06,10.4,2.06Z"/>
      <path className="spaceman-wondering-23" d="M307.02,562.37s18.14,75.83,15.35,88.15-13.37,29.17-19.94,31.91c-6.57,2.74-28.9,19.42-34.48,20.15s-23.72,6.31-23.72,6.31l-23.72-6.31s43.26-19.58,47.44-26.87c4.19-7.29,19.53-38.76,19.53-49.54s-10.07-38.06-10.07-38.06l29.6-25.74Z"/>
      <path className="spaceman-wondering-9" d="M220.51,702.57s44.9-7.26,51.75-19.31c6.85-12.05,11.04-30.19,11.04-30.19,0,0-21.22-4.94-30.84-22.7-9.62-17.76-8.24-25.33-8.24-25.33,0,0-5.58,42.53-6.97,48.77-1.4,6.24-19.53,18.8-25.12,24.38-5.58,5.58-15.35,17.67-15.35,17.67l23.72,6.72Z"/>
      <path className="spaceman-wondering-25" d="M196.79,695.86l-13.71-8.4s37-42.25,30.92-33.66c-.47,.66-10.23-10.5-10.23-10.5,0,0-2.11-3.46-11.16-12.56-9.05-9.1-18.58-34.44-18.58-34.44l12.44-11.08,20.09-.14,30.7,20.89,8.37,24.78,1.82,23.06-10.19,11.83-40.47,30.22Z"/>
      <path className="spaceman-wondering-13" d="M167.53,565.34l-8.87-17.82s-3.73,.9-5.13,2.3c-1.4,1.4-2.79,5.58-2.79,5.58l1.4,5.58s4.35,4.53,4.27,4.36,8.29,1.17,8.29,1.17l2.83-1.17Z"/>
      <path className="spaceman-wondering-10" d="M167.53,565.34s-8.27,1.22-12.53-2.96c-4.26-4.19-4.26-6.98-4.26-6.98,0,0-.58,15.35,1.1,20.93,1.69,5.58,7.49,17.19,7.49,17.19,0,0,10.48,3.48,28.85,3.61,18.37,.13,0-30.28,0-30.28l-20.66-1.51Z"/>
      <path className="spaceman-wondering-24" d="M220.51,545.63s35.01,9.99,51.63,6.98c16.62-3.02,47.98-6.07,61.4-15.35,13.41-9.28,34.88-39.07,34.88-39.07l8.3-41.56-78.1,35.76-78.1,6.7v46.55Z"/>
      <path className="spaceman-wondering-30" d="M237.26,450.74l2.79,64.6s34.8,7.56,43.22,7.76c8.41,.2,32.31-10.18,49.66-22.43,17.36-12.25,43.79-44.05,43.79-44.05,0,0,7.26-24.76,6.46-36.95-.8-12.19-6.46-24.75-6.46-24.75l-58.04,26.75-68.45,4.99-12.97,12.04v12.05Z"/>
      <path className="spaceman-wondering-19" d="M62.32,469.1s1.91,13.73,1.91,19.31c0,2.89-.26,5.41-.5,7.17-.21,1.47,.76,2.83,2.21,3.12,7.43,1.45,28.52,5.58,38.76,7.85,12.56,2.79,40.47,15.35,40.47,15.35l-8.75-28.57-32.67-15.46-41.42-8.78Z"/>
      <path className="spaceman-wondering-18" d="M24.74,330.26s4.96,50.04,5.48,69.91c.46,17.37,.12,49.8,.02,57.7-.01,1.05,.67,1.97,1.67,2.27l77.29,22.81s-35.05-29.77-22.57-44.24c12.48-14.47,13.12-21.23,22.57-24.13,9.44-2.9,20.35-4.7,24.67-4.5,4.31,.21,31.05,6.62,38.61,9.69,7.56,3.07,20.12,11.45,38.26,19.82,18.14,8.37,26.51,11.16,26.51,11.16,0,0,19.53-8.37,41.86-8.37s37.67-1.8,43.26-3.69,22.21-11.67,26.45-13.06c4.24-1.39,13-9.8,13-9.8,0,0-167.87-36.36-168.55-36.3S24.74,330.26,24.74,330.26Z"/>
      <path className="spaceman-wondering-26" d="M13.73,252.57S-.05,260.22,0,275.95c.05,15.73,17.26,47.89,34.47,66,17.21,18.1,68.8,50.91,91.14,58.23,22.35,7.32,81.74,20.34,106.46,21.51,24.72,1.16,60.53,1.85,71.23,3.6s51.26-3.22,65.87-13.3c14.6-10.07,30.6-38,33.06-47.06,2.47-9.06,8.67-21.85,9.06-28.71,.39-6.86-.29-29.69-.29-29.69L13.73,252.57Z"/>
      <path className="spaceman-wondering-28" d="M358.92,385.16s-24.6,17.14-31.27,19.73c-6.67,2.59-31.53,7.67-55.38,3.74-23.85-3.93-23.85-2.54-30.82-2.54s-27.43,3.68-40.23,2.54-52.32-12.3-76.51-23.47c-24.19-11.16-59.08-36.28-84.19-73.95C15.4,273.53,4.48,228.88,7.84,195.39c3.37-33.49,11.74-80.93,29.88-107.44,18.14-26.51,45.08-46.47,45.08-46.47L358.92,385.16Z"/>
      <path className="spaceman-wondering-22" d="M115.86,235.86c-17.34-27.79-35.26-56.5-39.07-97.67-1.61-17.35-3.83-41.41,6.98-65.58C114.51,3.86,221.29,.15,226.09,.04c87.32-1.9,144.22,58.25,153.49,68.37,7.65,8.35,64.01,71.97,54.42,161.86-1.51,14.18-8.5,79.7-61.4,122.79-12.28,10-29.19,23.78-54.42,27.91-55.27,9.04-103.26-36-143.72-73.95-18.21-17.08-39.41-40.41-58.6-71.16Z"/>
      <g className="spaceman-wondering-32">
        <path className="spaceman-wondering-7" d="M316.35,56.57c18.27,.67,20.36,.47,36.91,8.54,18.45,9,32.29,26.49,39.99,46.09,7.7,19.6,9.69,41.19,8.77,62.34-.14,3.13-.35,6.31-1.45,9.23-3.4,8.99-13.97,12.52-23.27,12.63-16.72,.19-32.87-6.98-46.66-16.79-21.09-15.01-37.83-36.53-47.51-61.1-4.56-11.57-7.59-24.48-4.37-36.52,3.22-12.04,25.65-26.03,37.59-24.4"/>
      </g>
      <g className="spaceman-wondering-33">
        <path className="spaceman-wondering-7" d="M336.3,70.93c8.5,.31,9.95,1.22,17.64,4.97,8.58,4.18,15.02,12.32,18.6,21.43,3.58,9.11,4.51,19.16,4.08,28.99-.06,1.45-.16,2.93-.67,4.29-1.58,4.18-6.5,5.82-10.82,5.87-7.78,.09-15.28-3.25-21.7-7.81-9.81-6.98-17.59-16.99-22.09-28.42-2.12-5.38-3.53-11.38-2.03-16.98,1.5-5.6,7.74-13.1,13.29-12.34"/>
      </g>
      <path className="spaceman-wondering-10" d="M110.28,244.23c3.72-3.73,16.17-16.24,34.88-15.35,17.1,.82,31.42,12.44,37.67,26.51,7.19,16.19,1.78,31.3,0,36.28-1.49,4.16-8.26,23.07-25.12,29.3-24.75,9.15-60.13-13.17-62.79-40.47-1.85-19,12.8-33.71,15.35-36.28Z"/>
      <path className="spaceman-wondering-11" d="M121.44,251.21c-9.77,4.19-13.95,11.16-18.14,19.53-5.18,10.36-4.55,28.35,9.77,40.47,11.53,9.76,28.8,10.71,41.86,2.79,14.02-8.5,19.84-25.18,16.74-39.07-2.81-12.64-14.08-21.9-23.72-25.12-4.19-1.4-15.92-3.15-26.51,1.4Z"/>
      <path className="spaceman-wondering-21" d="M110.28,244.23l6.97-6.16s-27.04-32.76-33.75-66.32c-6.71-33.56-8.8-66.12-3.57-88.91,5.23-22.79,35.31-48.88,44.69-54.67,9.38-5.79,2.45-1.48,2.45-1.48,0,0-27.96,4.06-34.94,8.24-6.98,4.19-29.3,19.53-32.09,41.86-2.79,22.33-4.19,48.84,5.58,78.14,9.77,29.3,19.99,54.2,28.14,66.87,8.14,12.67,16.51,22.43,16.51,22.43Z"/>
      <path className="spaceman-wondering-35" d="M167.53,315.23l6.94-8.21s41.83,42.41,59.98,50.51c18.15,8.1,64.62,24.85,87.44,22.75,22.81-2.1,3.19,.18,3.19,.18l4.27,14.47s-61.97-10.58-79.12-17.15c-17.16-6.57-36.69-20.52-49.25-30.29-12.56-9.77-33.45-32.26-33.45-32.26Z"/>
      <polygon className="spaceman-wondering-17" points="325.2 394.21 322.37 375.39 331.28 377.8 332.92 394.21 329.35 394.93 325.2 394.21"/>
      <path className="spaceman-wondering-16" d="M322.37,375.39s24.64-1.3,39.53-13.91c14.89-12.6,21.04-17.67,21.04-17.67l5.01,.88s-23.39,21.87-30.7,25.12c-12.56,5.58-25.97,7.99-25.97,7.99l-8.91-2.41Z"/>
      <path className="spaceman-wondering-14" d="M332.92,394.21s15.1-2.07,25.99-9.04c10.9-6.98,26.25-26.51,26.25-26.51l2.79-13.95s-24.33,21.4-27.52,23.26c-3.18,1.86-29.15,9.85-29.15,9.85l1.64,16.4Z"/>
      <path className="spaceman-wondering-10" d="M338.98,523.86s-17.56,12.2-31.74,16.99c-14.17,4.78-25.34,7.57-39.29,10.37-11.11,2.22-41.86,4.19-41.86,4.19l11.16,40.91s62.37-11.61,77.93-17.19c15.56-5.58,33.7-15.35,33.7-15.35l-9.9-39.91Z"/>
      <path className="spaceman-wondering-34" d="M361.49,572.14s-7.55,1.4-12.6-2.79c-5.05-4.19-8.37-13.95-9.77-26.51-.92-8.32-.08-16.46,1.36-25.67,1.43-9.21,7.01-14.79,8.41-16.19,1.4-1.4,5.58-2.79,5.58-2.79l7.02,.23v73.72Z"/>
      <ellipse className="spaceman-wondering-7" cx="361.49" cy="535.28" rx="16.79" ry="36.86"/>
      <path className="spaceman-wondering-15" d="M347.22,534.63c-.16-16.21,6.04-30.86,14.1-30.89,8.21-.03,14.76,13.92,14.7,31.52-.06,19.19-6.44,31.71-14.62,31.58-8.76-.13-14.19-15.99-14.18-32.21Zm13.49,28.21c1.29-.62,2.63-.93,3.86-1.92,3.65-2.94,6.37-8.24,7.42-16.63,1.29-10.32-1.16-22.39-5.42-27.75-7.18-9.04-16.13,3.28-16.79,21.31-.5,13.51,4.6,24.63,10.92,24.97Z"/>
      <path className="spaceman-wondering-15" d="M362.97,551.85c-3.37-.02-6.28-5.29-6.69-12.1-.45-7.48,1.71-14.59,5.08-16.29,2.66-1.34,5.14-.16,7.12,4.09,2.09,4.5,2.43,12.99-.03,19.04-1.39,3.43-3.33,5.28-5.5,5.26Z"/>
      <g>
        <g>
          <path className="spaceman-wondering-5" d="M265.16,572.14s2.79-10.16,2.79-12.06-13.95-6.08-13.95-6.08l-26.79,5.65-10.89,18.08-9.77,19.53,1.51,18.9s12.44-6.35,22.21-13.32c9.77-6.98,34.88-30.7,34.88-30.7Z"/>
          <path className="spaceman-wondering-31" d="M155.78,445.84s-7.15-3.73-15.16-4.3c-8.01-.57-16.01-.83-16.9,0s4.7,18.97,6.09,24.55c1.4,5.58,6.59,27.24,6.59,27.24,0,0,4.57,21.59,8.75,28.57s12.56,23.72,15.35,29.3c2.79,5.58,8.37,18.14,15.35,27.91,6.98,9.77,10.61,14.4,10.61,14.4l10.32-24.16s-16.52-14.7-24.31-24.79c-7.79-10.09-12.27-45.09-12.27-45.09l-4.43-53.63Z"/>
          <path className="spaceman-wondering-27" d="M155.78,445.84s-2.29,17.9-3.66,23.26-4.49,13.83-4.49,13.83c0,0,15.68,34.78,17.08,41.76s0,10.38,0,10.38c0,0,2.79-10.38,12.56-17.36,9.77-6.98,26.51-13.95,36.28-11.16,9.77,2.79,18.14,13.95,18.14,13.95,0,0,1.4-10.88,0-17.3-1.4-6.42-4.19-18.98-4.19-18.98,0,0-30.21-22.52-44.41-28-14.2-5.48-27.31-10.39-27.31-10.39Z"/>
          <path className="spaceman-wondering-36" d="M164.7,535.08s13.95,20.32,16.74,23.11c2.79,2.79,15.35,11.16,15.35,11.16,0,0,9.77-18.14,16.74-20.93,6.98-2.79,11.01-6.89,18.76-8.33l7.75-1.44s-1.4-11.16-8.37-18.14c-6.98-6.98-6.98-9.77-18.14-13.95-11.16-4.19-26.88,4.39-30.7,6.98-2.2,1.49-10,5.35-13.95,11.16-3.95,5.82-4.19,10.38-4.19,10.38Z"/>
          <path className="spaceman-wondering-6" d="M200.18,587.14s23.12,29.65,37.08,39.42c13.95,9.77,21.18,17.18,25.24,19.05,4.06,1.88,12.43,6.06,20.8,7.46,8.37,1.4,18.14,1.4,25.12-2.79,6.98-4.19,11.16-13.95,6.98-20.93-4.19-6.98-51.63-55.81-57.21-60-5.58-4.19-16.74-11.16-16.74-11.16,0,0-6.98-4.55-13.95-.88-6.98,3.67-18.14,14.83-20.93,19.02-2.79,4.19-6.38,10.81-6.38,10.81Z"/>
          <path className="spaceman-wondering-8" d="M228.44,592.33s7.42,13.29,14.4,20.27c6.98,6.98,29.3,22.33,36.28,25.12,6.98,2.79,18.62,1.4,21.17,0,2.55-1.4,4.77-11.68-5.41-21.89-10.18-10.21-31.11-26.95-33.9-29.74-2.79-2.79-23.72-15.35-23.72-15.35l-16.74,8.37,7.93,13.22Z"/>
          <path className="spaceman-wondering-12" d="M245.63,595.86s5.55,8.88,11.16,1.4c4.19-5.58,1.48-18.26-2.79-20.93-11.16-6.98-25.74-7.03-32.09-9.77-4.12-1.78-2.79-4.19-2.79-4.19l-12.56,13.95s12.56,8.37,19.53,11.16c1.75,.7,8.13,2.13,9.77,2.79,4.9,1.96,9.77,5.58,9.77,5.58Z"/>
          <path className="spaceman-wondering-6" d="M252.46,600.26s-10.4,1.61-16.99,0c-6.59-1.61-20.54-11.37-24.73-14.16-4.19-2.79-6.4-6.25-6.4-6.25l2.21-3.52s32.09,11.16,37.67,13.95c5.58,2.79,8.23,9.98,8.23,9.98Z"/>
          <path className="spaceman-wondering-14" d="M186.47,585.22s.49,8.21,1.72,11.9c1.23,3.69,4.92,11.02,7.38,12.89s6.15,6.78,9.83,6.78,4.92-2.46,4.92-6.15-2.46-11.06,2.46-19.67c4.92-8.6,12.29-20.9,15.98-24.58,3.69-3.69,14.75-12.29,25.81-9.83,11.06,2.46,12.29,3.69,12.29,3.69,0,0,3.69,0-2.46-7.38-6.15-7.38-12.29-12.29-14.75-13.52-2.46-1.23-15.98-2.46-15.98-2.46,0,0-17.52,1.98-27.82,12.05-10.29,10.08-19.38,36.28-19.38,36.28Z"/>
        </g>
        <path className="spaceman-wondering-14" d="M136.41,493.34s-28.16-8.68-38.31-17.27c-10.15-8.58-12.94-23.93-12.94-23.93,0,0,7.42-14.22,19.53-15.35,12.12-1.13,12.56,1.4,12.56,1.4l6.98,2.79,12.18,52.36Z"/>
        <path className="spaceman-wondering-14" d="M232.27,512.3s3.59,3.46,7.78,3.05c4.19-.42,12.42-3.05,12.42-3.05,0,0-13.3-17.01-18.44-21.84-5.14-4.83-6.81-3.22-6.81-3.22,0,0,3.07,9.84,4.46,15.98,1.4,6.14,.59,9.09,.59,9.09Z"/>
        <path className="spaceman-wondering-6" d="M252.46,512.3s-2.87-35.05-5.55-41.33c-2.68-6.27-9.66-20.23-9.66-20.23,0,0-73.95-44.65-106.05-41.86-16.74,1.46-32.14,5.3-40.47,16.74-11.16,15.35-5.58,26.51-5.58,26.51,0,0,10.56-13.79,39.07-11.16,28.51,2.63,58.6,13.95,66.98,19.53,7.76,5.17,41.31,29.29,51.35,40.46,10.04,11.17,9.9,11.33,9.9,11.33Z"/>
      </g>
      <path className="spaceman-wondering-20" d="M267.95,717.26s8.37,8.37,11.16,11.16c2.79,2.79,17.18,13.75,20.93,18.14,3.75,4.38,11.16,9.77,15.35,22.33,4.19,12.56-1.4,27.91-1.4,27.91,0,0-1.4,6.89-41.86,0-40.47-6.89-99.07-33.85-107.44-39.95-8.37-6.1-19.53-19.36-19.53-21.1s2.6-15.93,5.48-22.79c2.89-6.86,18.68-18.74,18.68-18.74l98.62,23.05Z"/>
      <path className="spaceman-wondering-14" d="M183.08,682.61s9.52,11.33,23.47,16.01c13.95,4.68,38.78,6.77,45.9,7.82s15.49-3.87,15.49-3.87c0,0,5.84,9.29,4.31,14.78-1.52,5.48-30.82,10.54-46.17,6.62-15.35-3.92-40.84-13.75-49.72-21.39-8.88-7.64-8.26-12.79-8.26-12.79,0,0,1.29-10.98,5.91-12.68s9.05,5.51,9.05,5.51Z"/>
      <path className="spaceman-wondering-12" d="M147.62,722.8s-3.85,2.83-5.25,7.01c-1.4,4.19-1.4,8.37-1.4,8.37,0,0,16.31,23.49,32.09,29.3,15.78,5.81,47.44,26.51,73.95,32.09,26.51,5.58,65.58,0,65.58,0,0,0,5.58-5.58,5.58-11.16v-5.58s-45.2,5.1-70.74-3.03c-25.54-8.13-63.75-23.93-73.95-32.77-10.2-8.85-25.88-24.23-25.88-24.23Z"/>
      <path className="spaceman-wondering-14" d="M404.7,452.14s-1.4-13.88,0-18.8,13.95-32.83,13.95-35.62,0-15.35,1.4-18.14c1.4-2.79,4.19-6.98,4.19-6.98,0,0,8.37,0,12.56,1.4,4.19,1.4,15.49,6.53,16.12,8.15,.63,1.62-5.68,8.55-6.71,12.06s-8.01,28.63-8.01,34.21-1.4,19.53-1.4,22.33,1.09,8.27,1.09,8.27c0,0-6.48,2.12-9.36,1.81-2.89-.31-10.97-2.51-15.3-4.2-4.33-1.69-8.52-4.49-8.52-4.49Z"/>
    </g>
  </g>
</svg>
  
  )
}

export default React.memo(Wondering)
