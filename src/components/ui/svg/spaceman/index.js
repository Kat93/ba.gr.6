export { default as Flying } from './Flying'
export { default as Leaning } from './Leaning'
export { default as Running } from './Running'
export { default as Showing } from './Showing'
export { default as Standing } from './Standing'
export { default as Walking } from './Walking'
export { default as Waving } from './Waving'
export { default as Wondering } from './Wondering'