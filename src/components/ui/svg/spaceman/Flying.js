import React from 'react'

function Flying (props) {
  const {
    className,
    width = '100%',
    height = '100%'
  } = props


  const css = `
  .cls-1 {
    fill: url(#Ikke-navngivet_forløb_115);
  }

  .cls-2 {
    fill: url(#Ikke-navngivet_forløb_110);
  }

  .cls-3 {
    fill: #e6e6e6;
  }

  .cls-4 {
    fill: #fff;
  }

  .cls-5 {
    fill: #999;
  }

  .cls-6 {
    fill: #b3b3b3;
  }

  .cls-7 {
    fill: #32345c;
  }

  .cls-8 {
    fill: #8e8e8e;
  }

  .cls-9 {
    fill: #a2f4f0;
  }

  .cls-10 {
    fill: #bfbfbf;
  }

  .cls-11 {
    fill: #0d064e;
  }

  .cls-12 {
    fill: #6f718d;
  }

  .cls-13 {
    fill: #563ac9;
  }

  .cls-14 {
    fill: url(#gråligt_forløb-2);
  }

  .cls-15 {
    fill: url(#gråligt_forløb);
  }

  .cls-16 {
    fill: url(#Ikke-navngivet_forløb_115-2);
  }

  .cls-17 {
    fill: url(#Ikke-navngivet_forløb_62);
  }

  .cls-18 {
    fill: url(#Ikke-navngivet_forløb_68);
  }

  .cls-19 {
    fill: url(#Ikke-navngivet_forløb_11);
  }

  .cls-20 {
    fill: url(#Ikke-navngivet_forløb_10);
  }

  .cls-21 {
    fill: url(#Ikke-navngivet_forløb_14);
  }

  .cls-22 {
    fill: url(#Ikke-navngivet_forløb_13);
  }

  .cls-23 {
    fill: url(#Ikke-navngivet_forløb_43);
  }

  .cls-24 {
    fill: url(#Ikke-navngivet_forløb_18);
  }

  .cls-25 {
    fill: url(#Ikke-navngivet_forløb_32);
  }

  .cls-26 {
    fill: url(#Ikke-navngivet_forløb_20);
  }

  .cls-27 {
    fill: url(#Ikke-navngivet_forløb_93);
  }

  .cls-28 {
    fill: url(#Ikke-navngivet_forløb_75);
  }

  .cls-29 {
    fill: url(#Ikke-navngivet_forløb_71);
  }

  .cls-30 {
    fill: url(#Ikke-navngivet_forløb_88);
  }

  .cls-31 {
    opacity: .45;
  }

  .cls-32 {
    opacity: .72;
  }
  `

  return (
    <svg className={className} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox="0 0 666.56 725.8">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="Ikke-navngivet_forløb_62" data-name="Ikke-navngivet forløb 62" x1="209.39" y1="1225.56" x2="267.16" y2="1225.56" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".11" stopColor="#ccc"/>
      <stop offset=".66" stopColor="#e3e3e3"/>
      <stop offset="1" stopColor="#ececec"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_68" data-name="Ikke-navngivet forløb 68" x1="249.1" y1="1206.48" x2="300.18" y2="1206.48" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#ccc"/>
      <stop offset=".2" stopColor="#e2e2e2"/>
      <stop offset=".41" stopColor="#f2f2f2"/>
      <stop offset=".66" stopColor="#fcfcfc"/>
      <stop offset=".98" stopColor="#fff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_115" data-name="Ikke-navngivet forløb 115" x1="219.71" y1="1290.97" x2="353.11" y2="1290.97" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".3" stopColor="#fff"/>
      <stop offset=".5" stopColor="#fcfcfc"/>
      <stop offset=".65" stopColor="#f2f2f2"/>
      <stop offset=".78" stopColor="#e2e2e2"/>
      <stop offset=".9" stopColor="#ccc"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_14" data-name="Ikke-navngivet forløb 14" x1="342.54" y1="991.76" x2="371.89" y2="968.55" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#fff"/>
      <stop offset=".47" stopColor="#fdfdfd"/>
      <stop offset=".61" stopColor="#f6f6f6"/>
      <stop offset=".72" stopColor="#eaeaea"/>
      <stop offset=".8" stopColor="#d9d9d9"/>
      <stop offset=".87" stopColor="#c3c3c3"/>
      <stop offset=".93" stopColor="#a8a8a8"/>
      <stop offset=".98" stopColor="#888"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_13" data-name="Ikke-navngivet forløb 13" x1="325.63" y1="988.53" x2="325.63" y2="1045.75" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".17" stopColor="#e9e9e9"/>
      <stop offset=".54" stopColor="#bdbdbd"/>
      <stop offset=".83" stopColor="#a2a2a2"/>
      <stop offset="1" stopColor="#999"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_93" data-name="Ikke-navngivet forløb 93" x1="306.37" y1="1101.14" x2="45.42" y2="1101.14" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".23" stopColor="#e5e5e5"/>
      <stop offset=".53" stopColor="#c9c9c9"/>
      <stop offset=".8" stopColor="#b8b8b8"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_10" data-name="Ikke-navngivet forløb 10" x1="319.59" y1="1045.36" x2="42.77" y2="1045.36" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".31" stopColor="#fff"/>
      <stop offset=".4" stopColor="#eaeaea"/>
      <stop offset=".53" stopColor="#d1d1d1"/>
      <stop offset=".67" stopColor="silver"/>
      <stop offset=".82" stopColor="#b6b6b6"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_75" data-name="Ikke-navngivet forløb 75" x1="76.57" y1="1071" x2="76.57" y2="995.77" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#e6e6e6"/>
      <stop offset=".14" stopColor="#cecece"/>
      <stop offset=".38" stopColor="#acacac"/>
      <stop offset=".61" stopColor="#939393"/>
      <stop offset=".82" stopColor="#858585"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_88" data-name="Ikke-navngivet forløb 88" x1="45.42" y1="1123.57" x2="45.42" y2="974.97" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#ccc"/>
      <stop offset=".39" stopColor="#adadad"/>
      <stop offset=".78" stopColor="#8c8c8c"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_71" data-name="Ikke-navngivet forløb 71" x1="78.14" y1="1118.13" x2="78.14" y2="1063.58" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#ccc"/>
      <stop offset=".51" stopColor="#cacaca"/>
      <stop offset=".67" stopColor="#c3c3c3"/>
      <stop offset=".78" stopColor="#b7b7b7"/>
      <stop offset=".88" stopColor="#a6a6a6"/>
      <stop offset=".95" stopColor="#909090"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="gråligt_forløb" data-name="gråligt forløb" x1="314.88" y1="966.16" x2="-42.78" y2="966.16" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".24" stopColor="#ededed"/>
      <stop offset=".71" stopColor="#c1c1c1"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_110" data-name="Ikke-navngivet forløb 110" x1="359.8" y1="903.14" x2="-59.38" y2="903.14" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".35" stopColor="#fcfcfc"/>
      <stop offset=".55" stopColor="#f4f4f4"/>
      <stop offset=".71" stopColor="#e6e6e6"/>
      <stop offset=".85" stopColor="#d2d2d2"/>
      <stop offset=".97" stopColor="#b9b9b9"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_18" data-name="Ikke-navngivet forløb 18" x1="-49.1" y1="775.08" x2="132.86" y2="775.08" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".04" stopColor="#f3f3f3"/>
      <stop offset=".13" stopColor="#dcdcdc"/>
      <stop offset=".23" stopColor="#c9c9c9"/>
      <stop offset=".35" stopColor="#bcbcbc"/>
      <stop offset=".51" stopColor="#b5b5b5"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_20" data-name="Ikke-navngivet forløb 20" x1="51.78" y1="1213.91" x2="181.47" y2="1213.91" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".29" stopColor="#ccc"/>
      <stop offset=".3" stopColor="#cdcdcd"/>
      <stop offset=".41" stopColor="#e3e3e3"/>
      <stop offset=".54" stopColor="#f2f2f2"/>
      <stop offset=".68" stopColor="#fcfcfc"/>
      <stop offset=".89" stopColor="#fff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_115-2" data-name="Ikke-navngivet forløb 115" x1="98.69" y1="1289.73" x2="219.02" y2="1289.73" xlinkHref="#Ikke-navngivet_forløb_115"/>
    <linearGradient id="Ikke-navngivet_forløb_32" data-name="Ikke-navngivet forløb 32" x1="-17.91" y1="762.52" x2="388.9" y2="762.52" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset=".17" stopColor="#563ac9"/>
      <stop offset=".34" stopColor="#6b56d2"/>
      <stop offset=".71" stopColor="#a19eeb"/>
      <stop offset=".98" stopColor="#ccd7ff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_11" data-name="Ikke-navngivet forløb 11" x1="-47.26" y1="690.16" x2="190.95" y2="629.58" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".35" stopColor="#d9d9d9"/>
      <stop offset=".66" stopColor="#bdbdbd"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="gråligt_forløb-2" data-name="gråligt forløb" x1="148.1" y1="995.14" x2="42.72" y2="812.61" xlinkHref="#gråligt_forløb"/>
    <linearGradient id="Ikke-navngivet_forløb_43" data-name="Ikke-navngivet forløb 43" x1="-8.2" y1="1052.06" x2="27.82" y2="1052.06" gradientTransform="translate(865.97 -449.38) rotate(44.27)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".02" stopColor="#f5f5f5"/>
      <stop offset=".1" stopColor="#d8d8d8"/>
      <stop offset=".19" stopColor="#c1c1c1"/>
      <stop offset=".29" stopColor="#afafaf"/>
      <stop offset=".42" stopColor="#a2a2a2"/>
      <stop offset=".58" stopColor="#9b9b9b"/>
      <stop offset=".93" stopColor="#999"/>
    </linearGradient>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="cls-12" d="M226.47,654.96s3.63-.18,3.7-3.79c.07-3.61-7.24-4.98-7.24-4.98l-7.31-1.37,10.86,10.14Z"/>
      <path className="cls-17" d="M183.5,633.41s-14.84-8.42-22.15-9.79c-7.31-1.37-27.64-5.5-27.64-5.5,0,0,8.47-10.31,12.72-20.57,5.58-13.48,18.61-27.96,18.61-27.96l47.66-7.48,11.02,4.18-40.22,67.13Z"/>
      <path className="cls-18" d="M226.74,569.22s22.39,12.94,24.93,12.65,8.78,.17,8.78,.17c0,0-3.95,10.42-7.47,18.52-3.51,8.1-12.89,19.13-20.09,26.52-7.2,7.39-15.88,13.33-17.28,17.73-1.4,4.4-2.84,5.87-2.84,5.87l-29.26-17.28s10.05-22.63,14.93-30.34c4.87-7.71,18.09-29.4,18.09-29.4l10.22-4.45Z"/>
      <path className="cls-1" d="M127.77,622.59s1.37,7.78,.04,10.46-5.94,4.77-11.56,11.85c-5.62,7.08-12.86,11.55-14.18,21.78-1.33,10.23,1.7,18.95,1.7,18.95,0,0,40.58-28.37,63.98-20.58,23.4,7.79,57.12,20.5,61.59,27.74,4.47,7.24,5.95,8.68,5.95,8.68,0,0,8.72-3.03,8.6-11.78-.11-8.75-1.74-21.86-28.29-44.87-4.68-4.05-35.2-14.14-39.59-15.55-4.4-1.4-48.24-6.68-48.24-6.68Z"/>
      <path className="cls-5" d="M103.77,685.64s-1.23,2.71-1.3,5.01-5.83,8.21,13.3,21.1c19.13,12.89,58.58,16.76,71.65,12.22,13.07-4.54,46.54-12.27,49.34-21.06,2.81-8.79-10.53-24.67-20.8-28.92-10.27-4.25-33.76-15.62-46.91-16.91-13.15-1.29-39.09,6.08-44.97,11.39-5.88,5.31-20.33,17.17-20.33,17.17Z"/>
      <path className="cls-10" d="M101.63,694.46s-3.44-14.89,.56-19.01c4-4.12,28.27-24.4,40.97-24.94s33.05-6.64,56.54,4.73c23.49,11.37,29.36,14.22,36.79,24.34,7.42,10.12,6.21,16.02,4.71,19.66-1.5,3.64-6.27,6.43-6.27,6.43,0,0,1.85-2.75,.3-10.02s-16.29-18.76-23.64-23.05-27.89-12.78-36.64-12.67c-8.75,.11-22.49-.05-30.18,3.13-7.68,3.18-28.25,12.46-32.32,16.63-4.07,4.17-10.82,14.77-10.82,14.77Z"/>
      <path className="cls-12" d="M127.24,634.95c-7.27-3.66-10.38-17.22-2.41-18.87,8.57-1.77,31.13-4.2,34.88-4.24,18.15-.18,51.88,10.5,58.68,14.42,6.8,3.92,9.33,8.22,10.85,14.07,1.51,5.85,.92,10.82,.92,10.82,0,0-26.47-10.41-39.96-14.34-13.49-3.93-45.23-2.56-45.23-2.56l-17.74,.69Z"/>
      <path className="cls-8" d="M184.29,674.2s13.55,17.6,15.52,25.15c1.97,7.55,2.84,13.53,2.84,13.53l-22.12-35.52,3.76-3.15Z"/>
      <path className="cls-8" d="M166.79,677.15s12.95,17.89,14.97,25.46c2.02,7.57,2.98,13.54,2.98,13.54l-21.32-35.97,3.37-3.04Z"/>
      <path className="cls-8" d="M146.9,678.6s11.99,17.08,13.93,24.26c1.93,7.18,2.89,12.84,2.89,12.84l-19.83-34.28,3.02-2.83Z"/>
      <path className="cls-8" d="M131.03,684.27s10.02,15.25,11.76,21.58c1.74,6.33,2.66,11.29,2.66,11.29l-16.73-30.49,2.32-2.38Z"/>
      <path className="cls-21" d="M414.42,481.73s-2,10.47-1.93,16.24c.07,5.77,13.66,24.71,13.66,24.71,0,0,16.9-3.46,22.7-.65,5.8,2.81-5.7-20.1-10.28-27.26-4.58-7.16-4.88-15.42-4.88-15.42l-19.27,2.38Z"/>
      <path className="cls-5" d="M428.04,552.94l-.54-9.01s-9.62,4.45-17.7,9.6c-8.08,5.15-10.89,10.95-10.89,10.95l1.35-7.23s2.07,25.99,12.12,33.77c10.05,7.77,13.79,19.47,13.79,19.47l6.88,6.7,8.1-49.23-13.11-15.04Z"/>
      <path className="cls-22" d="M357.51,484.25l56.91-2.52s-1.97,13.36,.99,19.09c2.96,5.73,10.74,21.86,10.74,21.86,0,0-13.33,1.25-20.48,5.66-7.15,4.42-18.54,16.1-18.54,16.1,0,0-17.31-19.89-19.09-27.16-1.78-7.27-10.52-33.04-10.52-33.04Z"/>
      <path className="cls-12" d="M387.12,544.45s-2.26,1.77,.15,11.54c2.41,9.77,6.89,7.84,11.65,8.51,4.75,.67,0,0,2.85-2.92,9-9.24,22.69-14.91,22.69-14.91l41.45,7.58-2.3-6.44s7.84-8.66-.59-18.7c-8.43-10.03-36.87-6.42-36.87-6.42,0,0-11.91-.21-20.48,5.66s-18.54,16.1-18.54,16.1Z"/>
      <path className="cls-10" d="M434.32,513.56s-10.53,3.52-11.62,12.66c-.14,1.14-.21,2.32-.24,3.5-.18,8.07,1.85,16.06,5.57,23.22,.87,1.67,1.76,3.52,2.52,5.38,2.39,5.84,12.84,8.05,10.26,12.85s-7.38,12.32-11.36,27.17c-3.97,14.85-9.59,33.17-6.6,38.43,2.99,5.26,3.1,13.91,11.79,16.69l8.69,2.77,15.04-65.09,.33-36.71-24.39-40.87Z"/>
      <path className="cls-3" d="M428.63,519.4s7.01-15.95,20.22,2.63c13.22,18.58,19.21,35.81,21.21,43.46,3.91,14.93,2.5,22.37,4.14,29.67,1.64,7.29,0,17.09-2.61,25.94-2.61,8.85-9.62,24.8-13.89,29.18-4.27,4.38-14.78,6.04-14.78,6.04,0,0,.07-28.93,4.19-44.84,4.12-15.92,7.04-27.64,8.21-34.72,1.17-7.08-7.4-27.15-16.32-35.85s-10.37-21.5-10.37-21.5Z"/>
      <path className="cls-7" d="M99.03,427.43s-5.37-19.62-4.33-24.38c1.04-4.76,14.19-14.99,14.19-14.99l3.52,23.77-13.38,15.6Z"/>
      <path className="cls-7" d="M239.64,529.33s15.71,13.28,28.51,17.19c13.37,4.08,25.31,3.93,27.96,3.89,5.84-.07,11.37-2,14.55-3.1,4.36-1.51,11.67-5.92,11.67-5.92,0,0-15.3,18.79-24.99,25.63-9.69,6.84-18.37,12.79-18.37,12.79,0,0-26.55,5.54-37.39-.57-10.84-6.11-12.65-9.38-24.97-14.88-12.32-5.49-66.68-74.47-66.68-74.47l50.64-26.86,39.07,66.29Z"/>
      <path className="cls-11" d="M320.71,534.05s5.87,2.84,1.55,7.28c-19.17,19.66-53.92,5.06-53.92,5.06l52.37-12.34Z"/>
      <path className="cls-27" d="M342.5,526.47l-49.75,19.18s-23.86,8.54-46.21-9.67c-22.34-18.21-124.77-156.91-124.77-156.91l77.93,32.25,142.81,115.15Z"/>
      <path className="cls-20" d="M154.31,382.92s17.66,43.59,42.74,65.16c25.08,21.57,65.8,59.86,85.16,66.47,19.37,6.61,60.29,11.93,60.29,11.93l25.59-14.05s41.25-27.16,38.06-39.04c-3.19-11.88-67.53-22.74-85.26-40.02-17.73-17.28-56.1-71.25-70.21-81.05-14.11-9.8-75.7-31.61-75.7-31.61l-20.66,62.21Z"/>
      <path className="cls-28" d="M187.74,404.42s13.58-10.97,19.01-19.5c5.44-8.53-7.11-82.35-7.11-82.35,0,0-22.7,14.3-38.08,30.62-15.38,16.32-22.85,23.44-22.85,23.44,0,0,10.18-4.49,27.96,7.08,17.78,11.57,21.07,40.71,21.07,40.71Z"/>
      <path className="cls-30" d="M199.64,302.56l-3.79-22.14s-34.7,30.07-44.49,40.12c-9.79,10.05-14.11,14.48-14.11,14.48,0,0-11.49,13.96-17.21,23.17-5.72,9.22-11.62,28.1-12.79,39.14-1.17,11.04,.42,21.23,.42,21.23,0,0,12.96-13.3,17.32-14.81s4.49-35.65,16.88-49.83c12.38-14.18,57.78-51.37,57.78-51.37Z"/>
      <path className="cls-29" d="M138.71,356.62s21.6-6.09,35.22,13.56,13.81,34.24,13.81,34.24l-4.86,10.82s-6.06-9.75-23.96-11.45c-17.9-1.7-33.93-.03-33.93-.03,0,0,4.37-38.13,9.18-42.5s4.54-4.63,4.54-4.63Z"/>
      <path className="cls-15" d="M406.15,473.38s-40.93,.47-68.15-16.06c-27.23-16.53-40.71-44.09-49.61-55.65-8.9-11.56-31.06-33.16-36.94-36.01-5.87-2.84-8.83-5.72-8.83-5.72,0,0,.98-35.99-8.84-63.11-9.82-27.12-23.34-57.59-24.82-59.03-1.48-1.44-19.61-6.21-27.81-6.94-8.2-.73-14.32-2.18-17.51,.22s-16.1,18.53-15.08,29.78c1.02,11.25,5.89,50.59,5.89,50.59,0,0-7.68-11.66-14.87-25.24-7.61-14.4-14.67-30.95-11.41-38.04,6.33-13.79,90.05-84.9,90.05-84.9l187.92,310.11Z"/>
      <path className="cls-2" d="M273.51,94.74c-1.83-3,2.64,1.81-.7,.71-.04-.01-10.15,1.6-10.19,1.59-2.03-.65-7.28,1.55-11.63,3.07-5.85,2.03-10.68,6.53-13,10.38-8.81,14.64-23.42,38.29-23.68,49.33-.32,13.72-5.75,45.89,10.83,87.99,16.59,42.1,19.31,47.64,36.51,70.17,17.2,22.53,17.96,20.9,21.29,26.77,3.33,5.88,22.91,53.78,36.28,72.58,13.37,18.8,51.67,47.49,72.17,53.07,20.5,5.58,61.38,7.97,72.98,1.99,11.6-5.98,18.58-10.19,19.32-19.81,.27-3.51,.42-6.19,.49-8.18,.12-3.03-.65-6.01-2.23-8.6L273.51,94.74Z"/>
      <path className="cls-24" d="M289.56,323.45l-20.17-160.53,37.05-75.02,65.88-63.29,56.98-15.77L480.26,.89s-75.97-7.79-129.56,23.54c-53.59,31.32-83.66,68.08-100.77,103.37-17.11,35.29-20.5,61.56-18.42,79.06,2.08,17.5,15.04,61.12,22.14,74.15,7.1,13.04,38.37,48.67,38.37,48.67l-2.47-6.24Z"/>
      <path className="cls-26" d="M94.83,415.83s-11.21,29.03-14.6,44.2c-3.39,15.17-5.03,26.67-5.03,26.67,0,0-12.49,11.75-18.11,23.36-5.62,11.61-6.97,18.84-6.97,18.84l72.58,36.98s12.72-4.54,19.92-13.46c7.2-8.92,26.13-41.17,26.13-41.17l-73.92-95.41Z"/>
      <path className="cls-6" d="M50.11,528.89s3.68-14.35,10.27-24.59c6.59-10.24,14.81-17.61,14.81-17.61,0,0,4.06-27.34,6.3-31.73,2.23-4.38,2.36-7.99,2.36-7.99,0,0,8.78,23.69,13.2,30.84,4.42,7.15,2.96,5.73,2.96,5.73,0,0-18.47,21.87-19.86,26.21-1.39,4.34-11.7,28.48-11.7,28.48l-18.35-9.35Z"/>
      <path className="cls-12" d="M23.38,540.9s6.58-8.87,19.54-10.48c12.96-1.61,52.67,7.49,52.67,7.49,0,0,8.04,3.28,13.84,6.09,5.8,2.81,14.6,14.24,14.6,14.24,0,0,2.39,3.9-.89,8.47-3.28,4.56-61.34-19.78-69.24-19.39-7.89,.38-25.12,6.37-28.01,6.41-2.88,.04-2.53-12.81-2.53-12.81Z"/>
      <path className="cls-5" d="M136.99,621.8s-37.84,20.4-59.52,16.35c-21.69-4.05-42.56-8.43-48.79-12.52-6.23-4.09-22.95-13.83-25.25-25.48-2.3-11.65,20.98-29.47,32.67-28.72,11.69,.75,30.3-9.58,50.6-1.19,20.3,8.4,34.39,14.24,43.14,20.24,8.53,5.85,6.97,5.01,10.25,7.52,.93,.72-.25,2.4,.04,3.39,.39,1.33,2.54,2.67,2.85,5.85,.46,4.73,.89,7.93-2.63,11.12l-3.35,3.44Z"/>
      <path className="cls-16" d="M25.91,553.72l-11.84,12.14c-2.4,2.46-12.36,12.68-12.29,18.45l.04,2.88s7.08-10.19,14.24-14.6c7.15-4.42,34.42-14.86,43.08-14.97,8.65-.11,31.69-3.29,44.76,3.76,13.07,7.04,15.92,4.12,29.06,16.94,13.14,12.81,17.56,19.97,17.56,19.97,0,0,.84-14.24-7.56-27.31-14.71-22.89-39.25-24.02-56.59-26.68-17.34-2.66-60.45,9.42-60.45,9.42Z"/>
      <path className="cls-10" d="M.43,591.54l1.26,8.06c.09,.56,2.21,1.17,1.74,.56-.79-1.03,6.38-13.28,18.51-18.98,15.77-7.41,45.92-17.89,64.76-10.92,18.84,6.97,42.66,15.96,51.71,29.48,9.05,13.52,7,16.88,7,16.88,0,0,6.68-8.25,6.57-16.9l-.5-1.43c-.73-2.09-1.75-4.07-3.07-5.84-2.22-3-6.31-8.05-12.57-14.16-10.22-9.97-21.87-18.47-40.67-22.56-5.64-1.23-18.8-4.09-38.95-.95-7.27,1.13-26.86,10.02-35.88,13.44-7.15,2.7-17.15,15.51-19.93,20.45-.75,1.33-.26,1.39,.04,2.88Z"/>
      <path className="cls-8" d="M106.5,621.56s-19.06-24.43-28.55-29.81c-9.49-5.38-17.24-8.72-17.24-8.72l40.26,42.36,5.53-3.82Z"/>
      <path className="cls-8" d="M79.37,623.33s-17.15-21.98-25.69-26.82c-8.54-4.84-15.51-7.85-15.51-7.85l36.23,38.11,4.98-3.44Z"/>
      <path className="cls-8" d="M53.23,623.01s-13.17-16.88-19.73-20.6c-6.56-3.72-11.91-6.03-11.91-6.03l27.82,29.27,3.82-2.64Z"/>
      <path className="cls-25" d="M616.75,362.62c1.82-2.11,4-3.98,5.53-6.27,3.14-4.67,5.9-9.58,8.92-14.33,10.41-16.36,18.44-33.65,24.37-51.86,6.3-19.35,9.61-39,10.75-58.86,.46-7.96,.13-15.92-.14-23.85-.55-16.45-3.42-32.39-7.82-47.92-4.87-17.21-11.85-33.4-20.73-48.68-6.48-11.15-13.9-21.63-22.49-31.09-6.87-7.57-14.7-14.27-22.3-21.14-7.98-7.2-17.04-13.02-26.47-18.21-14.67-8.08-25.54-15.75-40.69-23.38-21.01-10.58-43.43-11.03-65.07-8.56-15.32,1.75-26.23-2.53-40.9,2.8-12.8,4.65-18.86,5.58-30.37,13.08-7.49,4.88-14.1,13.52-21.31,18.85-1.79-3.72-9.12,6.35-9.12,6.35-.9,.64-9.75,7.27-10.65,7.91-4.06,3.58-25.89,23.54-27.43,28.14-9.14,9.38,6.5,1.81,6.04,2.84-1.98,4.44,2.04,7.35,2.96,12.03q1.47,7.53-.09,15.15c-1.54,4.59-2.86,15.27-6.17,18.38-2.93,2.75-7.43,11.59-10.68,13.97-.46,.34-5.46,6.17-6.1,6.25-.75,.47-5.34,5.78-6.1,6.25-1.91,1.15-4.09,5.35-6.1,6.25-2.27,1.02-6.88,6.28-9.12,6.35-3.03,.1-6.09,4.2-7.66,13.88-1.15,7.09-1.46-4.45-2,2.67-.64,8.39-1.22,16.82-1.06,25.16,.22,11.61,.56,23.26,1.91,34.69,1.59,13.46,4.13,26.75,6.64,40.01,1,5.28,3.01,10.29,4.54,15.43,.34,1.15,.56,2.35,.83,3.52,1.74,4.92,3.44,9.86,5.24,14.75,1.32,3.58,2.83,7.08,4.15,10.66,.73,1.97,21.03,43.51,23.46,42.06,.89-.53,2-.65,2.98-1.02,2.76-1.06,4.82-.17,8.21-.27,3.03-.1,.2,1.14,1.96,.54,1.09-.37,2.49-1.42,3.36-.73q3.74,2.94,6.75,5.87c5.45,5.31,3.59,3.5,9.03,8.81,9.03,8.81,6.33,6.73,15.05,14.68,5.17,4.71,1.25,.59,6.98,4.66,8.31,5.89,16.81,11.53,25.3,17.17,2.31,1.53,4.91,2.63,7.35,3.97,6.27,3.44,3.92,.97,10.16,4.46,2.27,1.27,3.47-3.54,1.52-1.56-1.49,1.51,1.63-1.38,0,0-.89,.74,2.29,.63,1.51,1.47-2.31,2.45,.25,.63-2.01,3.13,2.08,.3,4.17,.61,6.25,.91,6.74,.95,13.45,2.06,20.23,2.8,15.23,1.68,30.54,.43,45.89-2.59,16.74-3.3,32.79-9.16,48.3-16.78,8.46-4.16,16.56-9.15,24.9-13.6,2.58-1.38,5.01-2.79,6.54-4.35,4.57-4.69,5.5-3.55,7.46-5.84,4.76-4.08,9.52-8.16,14.28-12.25,2.02-1.73,4.09-3.42,6.03-5.24,4.05-3.79,8.04-7.66,12.05-11.49,1.05-1.07,.24-.24,1.28-1.31,.65-.93,3.08-3.83,3.82-4.69Z"/>
      <g className="cls-31">
        <path className="cls-4" d="M623.93,173.09c12.32,12.92,13.91,14.21,19.98,31.13,6.77,18.87,4.52,40.53-3.45,59.48-7.97,18.95-21.3,35.4-36.36,49.56-2.23,2.09-4.54,4.18-7.3,5.46-8.5,3.97-18.3-.77-24.88-7.03-11.82-11.26-18.22-27.28-21.18-43.55-4.52-24.87-1.55-51.32,8.43-75.1,4.7-11.19,11.38-22.29,21.84-28.51s35.68-.71,42.91,8.56"/>
      </g>
      <g className="cls-32">
        <path className="cls-4" d="M636.82,201.43c5.73,6.01,6.13,7.63,8.95,15.5,3.15,8.77,2.1,18.85-1.61,27.66-3.71,8.81-9.91,16.46-16.91,23.05-1.04,.97-2.11,1.94-3.39,2.54-3.95,1.85-8.51-.36-11.57-3.27-5.5-5.24-8.47-12.69-9.85-20.25-2.1-11.57-.72-23.87,3.92-34.93,2.18-5.21,5.29-10.36,10.16-13.26,4.86-2.89,14.34-3.89,17.7,.42"/>
      </g>
      <path className="cls-19" d="M306.43,87.89s32.62-47.55,70.48-65.66C414.76,4.13,455.91-4.92,487.75,2.73c31.85,7.66,54.75,17.99,71.58,28.4,16.83,10.41,42.97,35.89,42.97,35.89,0,0-67.49-53.8-122.13-53.1-54.65,.7-84.44,18.59-98.92,27.48-14.48,8.89-38.49,33.39-44.83,39.99-2.61,2.72-8.57,11.9-8.57,11.9l-21.41-5.4Z"/>
      <path className="cls-14" d="M335.25,374.29c-2.79,1.22-5.01,3.63-8.04,4.96-.29-.71-2.53,.43-7.07-4.24-13.21-13.57-24.08-33.78-30.59-51.56-7.23-19.76-15.07-42.85-18.85-56.87-8.96-33.22-10.96-70.33-1.32-103.66,.99-3.43,3.77-11.88,4.67-14.64,2.81,.37,12.25,7.59,14.68,7.91-.02,.77-1.53,4.11-1.56,4.88-3.07,11.14-5.55,20.94-6.07,32.48,.16,12.96,1.16,24.89,2.09,37.23,2.03,26.83,8.3,44.62,18.79,75.9,2.43,5.51,7.99,20.77,10.05,25.79,.27,.98,22.94,40.84,23.21,41.82Z"/>
      <path className="cls-7" d="M287.79,149.13c-8,6.37-10.11,11.93-19.13,16.5-1.18,.9-4.08,3.23-6,2.76-1.7-.42-8.2-7.86-9.78-9.54-.62-.65-2.97-6.97-2.16-7.57,15.46-11.39,26.3-23.32,36.78-39.43,3.04-4.68,4.47-10.75,6.62-16.19,.55-1.39,1.24-2.69,1.49-4.27,.33-2.1,5.81-3.31,7.8-2.07,1.48,.92,3.88,5.04,4.55,6.52,3.15,6.97,5.27,18.59,3.02,23-3.37,6.6-9.13,13.92-13.14,20.01-3.31,3.41-6.72,6.88-10.03,10.28Z"/>
      <path className="cls-7" d="M303.01,90.53c.72-.2,5.87-4.78,6.38-4.86,6.18-.94,11.65,.86,18.66,6.8,6.97,5.9,9.89,11.69,9.65,17.88-.43,11.15-5.87,27.24-11.94,35.49-10.18,13.82-23.23,24.11-39.97,31.76-.91,.41-1.8,.86-2.77,1.18-6.72,2.19-14.01-1.71-17.69-9.64-.46-.99,.25-2.38,.99-3.06,12.26-11.37,24.91-22.41,33.94-36.55,2.79-4.38,5.22-9.05,7.31-13.91,3.05-7.1,1.47-16.36-4.54-25.09Z"/>
      <path className="cls-9" d="M298.73,94.43s3.55,14.43-11.02,35.82c-14.57,21.39-25.73,33.83-31.71,31.64-5.98-2.19-11.5-16.93-1.31-37.52,10.19-20.58,24.19-30.32,33.34-34.2,9.15-3.87,10.7,4.25,10.7,4.25Z"/>
      <path className="cls-12" d="M341.93,366.96c3.58,4.11,9.79,11.18,13.57,15.13,7.38,7.71,14.51,14.34,22.96,20.97,11.93,9.35,28.67,14.43,41.77,21.72,4.93,2.74,10.51,6.02,17.36,7.1,1.08,.53,10.44,2.08,11.71,2.77-.53,.64-3.04,8.54-3.43,9.08-7.31-1.2-13.76,.44-19.04-1.97-18.31-7.32-43.27-16.92-60.71-32.41-10.78-9.58-21.08-20.38-28.77-32.58-1.63-2.32-3.97-5.99-4.2-6.39,.82-.6,3.31-1.14,4.97-1.7q2.61-1.72,3.79-1.71Z"/>
      <path className="cls-12" d="M434.82,441.47c-.11,.56,11.2,2.08,11.05,2.26-2.89,3.41-2.95,5.6-6.78,8.18-3.12,2.1-3.79,1.46-9.12,1.65-6.83-1.78-18.31-5.97-26.7-8.32-11.39-7.15-27.14-14.81-38.22-22.47-9.71-6.71-16.95-14.29-24.91-22.95-4.91-5.34-7.76-8.32-9.62-11.8-.47-.54-8.9-11.56-8.83-12.69,6.27-3.21,5.32-1.78,11.48-4.94,1.98,3.02,6.24,9.01,8.46,12.18,5.93,8.49,13.17,15.98,20.88,23.06,14.97,13.75,29.28,19.63,47.16,28.8,4.87,2.49,11.67,3.5,16.78,4.7,1.12,.26,7.15,2.01,8.39,2.35Z"/>
      <path className="cls-3" d="M368.09,512.42s19.29-14.25,25.05-20.16c5.76-5.91,13.01-18.89,13.01-18.89l77.48-33.73,8.45-1.47s5.58,11.9-.11,23.64c-5.69,11.75-24.22,31.79-38.85,35.93-14.63,4.14-48.09,11.87-56.84,11.98-8.75,.11-28.19,2.69-28.19,2.69Z"/>
      <path className="cls-23" d="M139.59,286.21s-8.78,3.98-10.14,10.4q-1.36,6.42-1.29,12.26c.07,5.84,1.59,10.19,3.1,14.55,1.51,4.36,5.98,11.6,5.98,11.6l23.08-20.72s-6.43-6.52-11.86-14.44c-5.95-8.68-8.88-13.65-8.88-13.65Z"/>
      <path className="cls-12" d="M160.33,314.3l31.56-30.43-6.04-28.26s-4.43-4.32-9.76-15.02c-5.33-10.7-21.12-3.55-21.12-3.55,0,0-1.44,1.48-2.84,5.87-3.38,10.59-4.82,11.33-5.61,17.58-1.52,12.01,7.42,46.7,7.42,46.7l4.92,5.67"/>
      <path className="cls-3" d="M205.54,427.54s40.33-41.37,38.67-57.4c-1.66-16.03-1.57-33.29-5.66-55.98-4.09-22.69-26.66-76.39-29.62-79.27-2.95-2.88-36.6-9.75-43.84-5.28-7.24,4.47-8.68,5.95-10.12,7.42,0,0,15.99-4.58,26.45,14.25,10.45,18.84,23.09,94.55,24.75,110.58,1.66,16.03-.62,65.67-.62,65.67Z"/>
      <ellipse className="cls-4" cx="211.27" cy="538.17" rx="39.43" ry="34.22" transform="translate(-321.56 313.78) rotate(-45.73)"/>
      <path className="cls-13" d="M190.92,517.37c11.88-12.64,31.86-15.04,43.65-3.59,12.01,11.66,11.15,31.66-2.07,45.07-14.41,14.62-33.07,15.13-44.93,3.38-12.69-12.58-8.77-32.44,3.35-44.86Zm-1.37,40.8c2.35,1.36,4.53,3.04,7.07,4.03,7.52,2.94,15.46,2.76,23.25-2.17,9.59-6.07,15.03-18.8,12.81-28.96-3.73-17.15-25.99-20.44-40.43-7.57-10.82,9.64-11.67,25.42-2.7,34.68Z"/>
      <path className="cls-13" d="M201.06,552.97c-4.9-4.81-5.21-12.99-.73-18.78,4.93-6.37,13.38-8.74,19.59-5.24,4.88,2.75,7.63,7.2,7.35,13.27-.3,6.42-6.15,13.41-14.25,14.55-4.59,.65-8.8-.69-11.95-3.79Z"/>
      <path className="cls-10" d="M215.02,484.91s-9.77-3.86-12.89-.66c-3.12,3.2-5.11,5.24-5.11,5.24,0,0,3.92,35.14,3.25,39.89s7.01,21.76,4.09,28.56c-2.92,6.81-10.04,20.03-15.84,23.02-5.8,2.99-20,3.28-20,3.28l18.85-115.74,27.55,8.53,.09,7.88Z"/>
      <path className="cls-12" d="M183.49,414.68s-11.39-11.52-24.97-12.81c-13.58-1.29-21.9-1.18-33.54,1.89-11.63,3.07-23.08,20.72-25.96,23.68s-3.32,15.97-2.21,21.86c1.11,5.88,2.02,11.75,6.07,18.97,4.05,7.22,14.28,8.55,14.28,8.55l-1.55-7.28s-5.98-11.6-3.18-20.39c2.81-8.79,12.89-19.13,28.95-17.88,16.07,1.25,27.74,1.11,38.04,8.27,10.31,7.16,13.26,10.05,17.62,8.53,4.36-1.51,4.26-6.07,3.48-11.81-.78-5.74-17.03-21.58-17.03-21.58Z"/>
      <path className="cls-3" d="M117.16,476.81s-3.42-7.43-6.97-16.05c-3.56-8.62-4.69-19.58,7.4-25.92,12.09-6.34,15.36-8.92,34.19-4.97,18.83,3.95,27.66,9.67,27.66,9.67,0,0,10.31,7.16,17.62,8.53,7.31,1.37,16.27,4.33,23.45,8.46,7.19,4.13,12.18,8.2,14.81,17.32,2.64,9.12,3.03,8.72,3.07,11.63s-1.33,10.23-10.12,7.42-11.78-8.6-17.66-11.45c-5.87-2.84-16.14-7.09-19.02-4.14-2.88,2.95-6.79,39.48-2.25,52.56,4.54,13.07,7.65,27.63,1.96,39.37-5.69,11.75-20.16,20.68-36.34,10.68-16.18-10.01-25.01-15.73-31.06-33.16-6.06-17.43-6.43-46.61-6.58-58.28l-.15-11.67Z"/>
    </g>
  </g>
</svg>
  
  )
}

export default React.memo(Flying)
