import React from 'react'

function Leaning (props) {
  const {
    className,
    width = '100%',
    height = '100%'
  } = props


  const css = `
  .cls-1 {
    fill: url(#Ikke-navngivet_forløb_151);
  }

  .cls-2 {
    fill: url(#Ikke-navngivet_forløb_153);
  }

  .cls-3 {
    fill: url(#Ikke-navngivet_forløb_115);
  }

  .cls-4 {
    fill: url(#Ikke-navngivet_forløb_110);
  }

  .cls-5 {
    fill: #e6e6e6;
  }

  .cls-6 {
    fill: #fff;
  }

  .cls-7 {
    fill: #999;
  }

  .cls-8 {
    fill: #32345c;
  }

  .cls-9 {
    fill: #a2f4f0;
  }

  .cls-10 {
    fill: #bfbfbf;
  }

  .cls-11 {
    fill: #0d064e;
  }

  .cls-12 {
    fill: #6f718d;
  }

  .cls-13 {
    fill: #563ac9;
  }

  .cls-14 {
    fill: url(#gråligt_forløb-2);
  }

  .cls-15 {
    fill: url(#gråligt_forløb);
  }

  .cls-16 {
    fill: url(#Ikke-navngivet_forløb_115-2);
  }

  .cls-17 {
    fill: url(#Ikke-navngivet_forløb_11);
  }

  .cls-18 {
    fill: url(#Ikke-navngivet_forløb_10);
  }

  .cls-19 {
    fill: url(#Ikke-navngivet_forløb_14);
  }

  .cls-20 {
    fill: url(#Ikke-navngivet_forløb_13);
  }

  .cls-21 {
    fill: url(#Ikke-navngivet_forløb_43);
  }

  .cls-22 {
    fill: url(#Ikke-navngivet_forløb_18);
  }

  .cls-23 {
    fill: url(#Ikke-navngivet_forløb_32);
  }

  .cls-24 {
    fill: url(#Ikke-navngivet_forløb_93);
  }

  .cls-25 {
    fill: url(#Ikke-navngivet_forløb_75);
  }

  .cls-26 {
    fill: url(#Ikke-navngivet_forløb_71);
  }

  .cls-27 {
    fill: url(#Ikke-navngivet_forløb_88);
  }

  .cls-28 {
    opacity: .45;
  }

  .cls-29 {
    opacity: .72;
  }
  `

  return (
    <svg className={className} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox="0 0 454.01 786.44">
  <defs>
    <style>
      {css}
    </style>
    <linearGradient id="Ikke-navngivet_forløb_115" data-name="Ikke-navngivet forløb 115" x1="3836.36" y1="718.59" x2="3982.04" y2="718.59" gradientTransform="translate(4116.69) rotate(-180) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset=".3" stopColor="#fff"/>
      <stop offset=".5" stopColor="#fcfcfc"/>
      <stop offset=".65" stopColor="#f2f2f2"/>
      <stop offset=".78" stopColor="#e2e2e2"/>
      <stop offset=".9" stopColor="#ccc"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_115-2" data-name="Ikke-navngivet forløb 115" x1="3727.87" y1="731.34" x2="3850.31" y2="731.34" xlinkHref="#Ikke-navngivet_forløb_115"/>
    <linearGradient id="Ikke-navngivet_forløb_151" data-name="Ikke-navngivet forløb 151" x1="3828.98" y1="617.2" x2="3934.64" y2="617.2" gradientTransform="translate(4116.69) rotate(-180) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#b3b3b3"/>
      <stop offset=".03" stopColor="#c3c3c3"/>
      <stop offset=".08" stopColor="#d9d9d9"/>
      <stop offset=".15" stopColor="#eaeaea"/>
      <stop offset=".22" stopColor="#f6f6f6"/>
      <stop offset=".33" stopColor="#fdfdfd"/>
      <stop offset=".63" stopColor="#fff"/>
      <stop offset=".72" stopColor="#fbfbfb"/>
      <stop offset=".8" stopColor="#f0f0f0"/>
      <stop offset=".88" stopColor="#ddd"/>
      <stop offset=".96" stopColor="#c3c3c3"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_153" data-name="Ikke-navngivet forløb 153" x1="3717.88" y1="607.89" x2="3835.83" y2="607.89" gradientTransform="translate(4116.69) rotate(-180) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#ccc"/>
      <stop offset=".1" stopColor="#dedede"/>
      <stop offset=".24" stopColor="#f0f0f0"/>
      <stop offset=".39" stopColor="#fbfbfb"/>
      <stop offset=".55" stopColor="#fff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_14" data-name="Ikke-navngivet forløb 14" x1="4201.37" y1="-215.61" x2="4228.58" y2="-237.13" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#fff"/>
      <stop offset=".47" stopColor="#fdfdfd"/>
      <stop offset=".61" stopColor="#f6f6f6"/>
      <stop offset=".72" stopColor="#eaeaea"/>
      <stop offset=".8" stopColor="#d9d9d9"/>
      <stop offset=".87" stopColor="#c3c3c3"/>
      <stop offset=".93" stopColor="#a8a8a8"/>
      <stop offset=".98" stopColor="#888"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_13" data-name="Ikke-navngivet forløb 13" x1="4185.69" y1="-218.6" x2="4185.69" y2="-165.56" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".17" stopColor="#e9e9e9"/>
      <stop offset=".54" stopColor="#bdbdbd"/>
      <stop offset=".83" stopColor="#a2a2a2"/>
      <stop offset="1" stopColor="#999"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_93" data-name="Ikke-navngivet forløb 93" x1="4167.84" y1="-114.2" x2="3925.93" y2="-114.2" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".23" stopColor="#e5e5e5"/>
      <stop offset=".53" stopColor="#c9c9c9"/>
      <stop offset=".8" stopColor="#b8b8b8"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_10" data-name="Ikke-navngivet forløb 10" x1="4180.09" y1="-165.92" x2="3923.47" y2="-165.92" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset=".31" stopColor="#fff"/>
      <stop offset=".4" stopColor="#eaeaea"/>
      <stop offset=".53" stopColor="#d1d1d1"/>
      <stop offset=".67" stopColor="silver"/>
      <stop offset=".82" stopColor="#b6b6b6"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_75" data-name="Ikke-navngivet forløb 75" x1="3954.8" y1="-142.15" x2="3954.8" y2="-211.89" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#e6e6e6"/>
      <stop offset=".14" stopColor="#cecece"/>
      <stop offset=".38" stopColor="#acacac"/>
      <stop offset=".61" stopColor="#939393"/>
      <stop offset=".82" stopColor="#858585"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_88" data-name="Ikke-navngivet forløb 88" x1="3925.93" y1="-93.41" x2="3925.93" y2="-231.17" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#ccc"/>
      <stop offset=".39" stopColor="#adadad"/>
      <stop offset=".78" stopColor="#8c8c8c"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_71" data-name="Ikke-navngivet forløb 71" x1="3956.26" y1="-98.46" x2="3956.26" y2="-149.02" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset=".08" stopColor="#ccc"/>
      <stop offset=".51" stopColor="#cacaca"/>
      <stop offset=".67" stopColor="#c3c3c3"/>
      <stop offset=".78" stopColor="#b7b7b7"/>
      <stop offset=".88" stopColor="#a6a6a6"/>
      <stop offset=".95" stopColor="#909090"/>
      <stop offset="1" stopColor="gray"/>
    </linearGradient>
    <linearGradient id="gråligt_forløb" data-name="gråligt forløb" x1="4175.73" y1="-239.34" x2="3844.16" y2="-239.34" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".24" stopColor="#ededed"/>
      <stop offset=".71" stopColor="#c1c1c1"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_110" data-name="Ikke-navngivet forløb 110" x1="4217.37" y1="-297.77" x2="3828.77" y2="-297.77" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".35" stopColor="#fcfcfc"/>
      <stop offset=".55" stopColor="#f4f4f4"/>
      <stop offset=".71" stopColor="#e6e6e6"/>
      <stop offset=".85" stopColor="#d2d2d2"/>
      <stop offset=".97" stopColor="#b9b9b9"/>
      <stop offset="1" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_18" data-name="Ikke-navngivet forløb 18" x1="3838.31" y1="-416.48" x2="4006.99" y2="-416.48" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".04" stopColor="#f3f3f3"/>
      <stop offset=".13" stopColor="#dcdcdc"/>
      <stop offset=".23" stopColor="#c9c9c9"/>
      <stop offset=".35" stopColor="#bcbcbc"/>
      <stop offset=".51" stopColor="#b5b5b5"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_32" data-name="Ikke-navngivet forløb 32" x1="3867.22" y1="-428.12" x2="4244.35" y2="-428.12" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset=".17" stopColor="#563ac9"/>
      <stop offset=".34" stopColor="#6b56d2"/>
      <stop offset=".71" stopColor="#a19eeb"/>
      <stop offset=".98" stopColor="#ccd7ff"/>
    </linearGradient>
    <linearGradient id="Ikke-navngivet_forløb_11" data-name="Ikke-navngivet forløb 11" x1="3840.01" y1="-495.2" x2="4060.84" y2="-551.36" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".35" stopColor="#d9d9d9"/>
      <stop offset=".66" stopColor="#bdbdbd"/>
      <stop offset=".85" stopColor="#b3b3b3"/>
    </linearGradient>
    <linearGradient id="gråligt_forløb-2" data-name="gråligt forløb" x1="4021.12" y1="-212.48" x2="3923.43" y2="-381.68" xlinkHref="#gråligt_forløb"/>
    <linearGradient id="Ikke-navngivet_forløb_43" data-name="Ikke-navngivet forløb 43" x1="3876.22" y1="-159.7" x2="3909.61" y2="-159.7" gradientTransform="translate(4240.13 -322.35) rotate(166.63) scale(1 -1)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stopColor="#fff"/>
      <stop offset=".02" stopColor="#f5f5f5"/>
      <stop offset=".1" stopColor="#d8d8d8"/>
      <stop offset=".19" stopColor="#c1c1c1"/>
      <stop offset=".29" stopColor="#afafaf"/>
      <stop offset=".42" stopColor="#a2a2a2"/>
      <stop offset=".58" stopColor="#9b9b9b"/>
      <stop offset=".93" stopColor="#999"/>
    </linearGradient>
  </defs>
  <g id="Lag_1-2" data-name="Lag 1">
    <g>
      <path className="cls-3" d="M170.12,694.27c-14.04,9.99-31.69,8.92-35.47,37.3,3.89,1.41,8.32,2.91,11.88,4.18,12.49,4.46,25.34,6.69,38.64,7.06,25.79,.72,50.55-3.96,74.44-13.38,7.99-3.15,11.07-5.5,18.84-9.22,.72-.35,1.83-1.19,1.89-1.89-4.33-7.71-2.68-16.89-5.68-22.7-37.69,14.3-68.01,18.5-104.53-1.36Z"/>
      <path className="cls-10" d="M278.43,712.66c-8.78,4.05-19.62,9.6-28.38,13.24-11.67,4.85-31.06,7.34-43.52,9.46-11.96,2.04-18.17,2.47-30.27,1.89-14.72-.71-26.16-.71-39.71-6.88-1.92-2.58-7.59,5.34-7.59,8.77,0,9.46-.38,10.06,3.78,11.35,7.57,2.35,18.74,5.9,26.49,7.57,9.16,1.97,31.29-2.99,40.57-3.68,16.21-1.22,32.11-4.26,47.44-9.88,9.03-3.31,25.94-7.72,34.97-11.03,4.19-1.54-4.97-16.65-3.78-20.81Z"/>
      <path className="cls-16" d="M385.54,704.18c-10.09,4.4-20.21,5.21-30.22,5.51-15.14,.46-30.34,.61-45.47,0-12.5-12.18-14.39-8.39-18.87-1.16-8.65,6.46-11.62,16.71-24.6,24.46,.68,3.33,.84,10.96,1.52,14.28,1.41,6.82,14.21,12.03,21.63,12.91,19.55,2.34,37.03-1.78,56.25-6.46,18.75-4.56,34.58-7.39,42.38-24.05,2.54,1.87-3.25-23.54-2.62-25.5Z"/>
      <path className="cls-10" d="M266.38,733c3.2,7.83,.45,12.64,8.74,18.16-.04,.03,11.58,5.14,11.54,5.17,6.17,.72,9.3,.59,15.49,.84,13.17,.54,26.36,.29,39.32-2.4,1.44-.3,2.88-.6,4.34-.91,0-.03-.01-.07-.01-.11,6.35-4.09,15.6-7.11,21.74-11.55,6.34-4.59,16.84-10.83,20.64-18.17,2.35,11.74,2.86,20.74-4.22,30.36-6.7,9.11-18.66,22.72-29.93,27.79,0-.06,0,.06,0,0-8.44,3.17-20.75,4.39-29.93,4.28-7.93-.1-22.08-1.17-29.93-2.14-10.53-1.3-18.36-5.31-27.79-10.69,0,.08-6.47,2.63,0,0-5.38-2.75-7.31-7.71-8.55-12.83-2.34-9.64-1.55-20.04,8.55-27.79Z"/>
      <polygon className="cls-11" points="174.13 673.46 194.63 666.14 204.51 685.19 178.59 678.8 174.13 673.46"/>
      <path className="cls-11" d="M287.12,673.43s11.53-2.43,13.12-2.79c1.59-.36,5.16,16.32,5.16,16.32l-14.06-4.72-4.22-8.81Z"/>
      <polygon className="cls-11" points="388.85 676.53 380.56 674.21 375.49 678.63 377.79 685.15 388.85 676.53"/>
      <path className="cls-1" d="M215.84,550.76c-8.46-1.3-24.64,2.53-33.56,1.15,.24,.71-.61,24.72,0,25.38,9.93,10.74,15.48,17.67,28.31,24.23,3.71,1.89,2.4-.98,2.1,3.46-.62,9.08-1.6,15.14-2.1,24.23-.69,12.56-9.73,28.37,3.15,39.23,.39,1.72-.47,7.53,0,9.23,.93,3.35,6.29,4.62,12.96,5.72,14.98,1.77,29.78-.97,43.74-7.34,2.52-14.92,4.7-29.66,7.58-44.24,1.95-9.88,4.18-19.78,8.47-28.89-.33-2.13,4.7-13.68-3.53-17.58-14.68-6.95-19.56,6.68-30.41,5.24-16.78-8.39-18.76-7.48-28.31-24.12-1.92-3.35-10.15-11.25-8.39-15.71Z"/>
      <path className="cls-5" d="M220.06,682.18c-4.64-10.48-6.18-21.38-6.57-32.53-.52-14.69,.55-29.29,2.3-43.84,.12-1.04,.15-2.09,.27-3.12,.18-1.56-.35-2.45-1.72-3.13-12.07-6.02-21.94-15.3-31.08-25.65-.12-.14-.31-.21-.77-.52-.14,1.31-.31,2.47-.37,3.64-1.25,24.27,.43,48.33,4.78,72.13,1.01,5.51,2.28,9.43,3.77,14.83,.3,1.1,1.07,5.2,1.05,6.29,1.05,5.24,2.12,8.45,5.24,9.44,7.02,2.22,14.17,.25,21.2,2.44,.52,.16,1.12,.02,1.9,.02Z"/>
      <path className="cls-2" d="M299.73,575.18c3.28-3.81,1.61-8.24,4.19-12.7,.81-1.4,18.78,1.16,19.93,1.15,2.17,0,3.64,.23,4.87,.56,.33-1.95,1.03-2.97,2.17-2.87,3.57,.33,7.48,.27,11.44,0,3.68-2.59,6.07-1.87,7.6-.75,7.02-.88,13.58-2.1,17.97-2.71,8.39-1.15,30.91-34.4,30.91-34.4l-2.49,44.73s-9.55,35.84-11.65,49.69c-2.1,13.85-2.74,70.73-2.74,70.73l-23.85,1.56c-.23,.14-.44,.29-.68,.43-1.71,.34-4.21,1.4-5.66,1.69-7.52-.04-15.04,.11-22.55-.17-7.45-.28-14.73-1.75-21.72-4.8-1.82-.79-7.03,2.02-6.7-.18,.31-2.11-1.77-5.97-2.1-8.08-2.05-13.31-1.07-22.48-3.15-35.78-1.67-10.73-5.24-23.09-8.39-32.32-.32-1.95-5.48-18.53-6.29-20.38,4.07-3.74,15.25-11.18,18.88-15.4Z"/>
      <path className="cls-5" d="M396.33,568.2l-45.17,33.68c2.06,14.85,9.4,40.25,8.36,51.79-1.05,11.54-7.76,38.63-7.76,38.63,0,0,27.45-2.21,30.19-3.68,2.74-1.47,14.39-120.42,14.39-120.42Z"/>
      <path className="cls-12" d="M269.55,674.23s2.3-1.75,6.34,0c4.04,1.75,6.68,11.34,5.94,17.59l-.74,6.25s-33.98,12.12-54.2,11.87c-20.22-.25-54.26-9.74-59.33-16.04-5.07-6.3,1.91-19.49,6.56-20.46,4.65-.96,24.48,6.83,39.56,7.58,15.08,.76,28.15,.7,37.32-.43,9.16-1.14,18.54-6.37,18.54-6.37Z"/>
      <path className="cls-9" d="M191.41,679.78c-2.91,.24-5.51,.31-6.91,2.99-1.53,2.91-.65,5.51,1.19,7.92,5.18,6.78,15.56,6.08,19.28-1.26,.79-1.56,.88-2.8-.46-4.24-3.68-3.94-8.56-4.6-13.09-5.41Z"/>
      <path className="cls-12" d="M388.25,676.87s4.3,1.16,3.9,12.81c-.39,11.65-5.55,19.36-5.55,19.36,0,0-11.3,4.4-42.61,5.32-31.3,.92-55.85-10.83-55.85-10.83,0,0-8.22-8.01-6.95-16.15,1.27-8.14,5.92-13.95,5.92-13.95,0,0,9.55,13.52,33.84,13.15,0,0,23.85,2.37,42.67-1.32,18.83-3.69,24.62-8.39,24.62-8.39Z"/>
      <path className="cls-9" d="M328.98,708.47c5.43-.01,10.12-3.08,10.78-7.05,.72-4.36-2.75-8.5-8.2-9.49-4.28-.78-8.29-.1-11.48,2.38-3.38,2.62-3.92,7.56,.04,11.09,2.24,2,5.36,3.08,8.86,3.07Z"/>
      <path className="cls-19" d="M115.07,427.44s-3.39,9.29-6.2,13.84c-2.8,4.55-22.63,13.15-22.63,13.15,0,0-11.79-10.8-17.75-11.33-5.96-.53,14.1-13.27,21.16-16.79s11.22-9.94,11.22-9.94l14.2,11.07Z"/>
      <path className="cls-7" d="M70.34,477.61l4.72-6.9s5.53,8.12,9.51,16.06c3.97,7.94,3.45,13.89,3.45,13.89l2.37-6.39s-14.02,19.69-25.71,21.09c-11.69,1.4-20.24,8.93-20.24,8.93l-8.66,2.06,16.99-43.02,17.59-5.72Z"/>
      <path className="cls-20" d="M159.14,456.54l-44.07-29.1s-4.79,11.56-9.88,14.71c-5.08,3.15-18.95,12.28-18.95,12.28,0,0,10.01,7.34,13.6,14.26,3.59,6.92,7.09,21.64,7.09,21.64,0,0,23.24-7.58,28.12-12.52s24.09-21.28,24.09-21.28Z"/>
      <path className="cls-12" d="M106.93,490.33s.96,2.48-5.61,9.11-9.21,2.95-13.31,1.22c-4.1-1.73,0,0-.87-3.68-2.77-11.63-10.96-22.66-10.96-22.66l-36.58-13.7,4.9-4.03s-2.12-10.63,9.37-14.59c11.48-3.97,32.38,12.44,32.38,12.44,0,0,9.57,5.5,13.6,14.26,4.03,8.76,7.09,21.64,7.09,21.64Z"/>
      <path className="cls-10" d="M84.09,443.29s6.7,7.81,3.22,15.6c-.44,.97-.94,1.94-1.48,2.9-3.7,6.51-9.11,11.89-15.49,15.82-1.48,.91-3.08,1.96-4.57,3.08-4.69,3.51-14.05,.29-14.28,5.34-.23,5.05,0,13.32-3.9,27.02-3.91,13.7-8.16,30.95-13.04,33.71-4.88,2.76-9.09,9.59-17.32,7.66l-8.23-1.93,19.02-58.94,17.21-29.36,38.86-20.9Z"/>
      <path className="cls-5" d="M85.84,450.65s2.02-16.03-17.34-7.54c-19.36,8.49-32.32,19.35-37.56,24.48-10.21,10.02-12.64,16.61-17.42,21.63s-8.13,13.59-10.27,21.88c-2.14,8.28-4.16,24.31-2.84,29.83,1.31,5.52,8.88,11.84,8.88,11.84,0,0,13.72-23.04,18.01-37.67,4.3-14.63,7.55-25.34,9.99-31.53,2.44-6.19,18.81-18.08,30.05-20.75,11.23-2.67,18.48-12.17,18.48-12.17Z"/>
      <path className="cls-8" d="M391.81,534.38s13.61-13.05,15.05-17.33c1.44-4.28-4.16-18.68-4.16-18.68l-14.11,17.24,3.22,18.78Z"/>
      <path className="cls-8" d="M231.45,548.5s-18.82,3.09-30.86,.1c-12.58-3.12-22-8.92-24.09-10.21-4.61-2.84-8.09-7-10.1-9.4-2.75-3.28-6.46-10.26-6.46-10.26,0,0,3.23,22.22,7.68,32.28,4.45,10.06,8.52,18.92,8.52,18.92,0,0,18.48,17.05,30.02,17.35,11.54,.3,14.53-1.44,26.94,.05,12.41,1.5,88.5-27.5,88.5-27.5l-27.5-45.47-62.64,34.14Z"/>
      <path className="cls-11" d="M164.71,513.67s-6.03-.53-4.7,5.05c5.89,24.76,40.48,29.7,40.48,29.7l-35.79-34.75Z"/>
      <path className="cls-24" d="M150.99,497.27l30.45,38.94s14.92,18.15,41.36,14.3c26.44-3.85,173.95-65.42,173.95-65.42l-77.34-11.44-168.42,23.62Z"/>
      <path className="cls-18" d="M369.02,472.66s-34.8,26.27-65.01,31.48c-30.22,5.22-80.83,16.3-99.39,12.34-18.55-3.96-53.64-19.21-53.64-19.21l-13.67-23.36s-19.89-41.24-11.69-49.17c8.19-7.93,64.54,14.06,86.87,8.75,22.33-5.31,78.54-29.97,94.44-31.05,15.89-1.08,75.27,10.89,75.27,10.89l-13.18,59.32Z"/>
      <path className="cls-25" d="M332.2,473.85s-5.58-15.19-5.84-24.57c-.26-9.37,44.86-62.12,44.86-62.12,0,0,11.25,22.18,15.71,42.48,4.47,20.3,7.02,29.52,7.02,29.52,0,0-5.96-8.41-25.61-7.67-19.65,.74-36.14,22.36-36.14,22.36Z"/>
      <path className="cls-27" d="M371.22,387.16l13.55-15.8s13.29,40.44,16.29,53.09c3.01,12.65,4.34,18.24,4.34,18.24,0,0,2.5,16.57,2.66,26.63,.16,10.06-4.13,27.88-8.46,37.22-4.33,9.34-10.44,16.69-10.44,16.69,0,0-3.98-16.75-6.73-20.03s13.39-30.5,10.29-47.67c-3.1-17.17-21.51-68.37-21.51-68.37Z"/>
      <path className="cls-26" d="M393.95,459.16s-14.29-15.13-34.47-5.98c-20.19,9.15-27.28,20.66-27.28,20.66l-1.29,10.92s9.46-4.87,24.51,2.3c15.05,7.17,27.01,16.12,27.01,16.12,0,0,14.68-32.41,12.93-38.18s-1.41-5.85-1.41-5.85Z"/>
      <path className="cls-15" d="M125.63,424.74s32.33,19.86,61.86,19.67c29.53-.19,53.37-15.69,65.95-20.65,12.58-4.96,40.5-11.59,46.52-11.06,6.03,.53,9.75-.35,9.75-.35,0,0,16.35-29.1,37.07-46,20.72-16.9,45.98-34.7,47.84-35.14,1.86-.44,18.55,4.4,25.43,7.72,6.88,3.32,12.42,5.08,13.82,8.51,1.4,3.43,3.99,22.4-2.18,30.87s-28.77,37.44-28.77,37.44c0,0,11.66-5.62,23.84-13.01,12.91-7.83,26.4-17.64,27.19-24.83,1.53-13.98-31.22-110.4-31.22-110.4L125.63,424.74Z"/>
      <path className="cls-4" d="M411.38,186.68c2.89-1.51-2.96,.18,.21,.9,.04,0,7.31,6.11,7.35,6.12,1.92,.45,5.05,4.7,7.8,7.98,3.68,4.4,5.39,10.28,5.4,14.44,.04,15.84,.4,41.61-4.64,50.51-6.27,11.07-17.27,39.24-50.51,64.84-33.24,25.6-38.04,28.71-62.44,38.44-24.41,9.74-24.23,8.07-29.68,11.16s-43.82,31.87-63.41,40.46c-19.59,8.59-63.71,13.18-82.67,7.86s-52.63-22.88-59-33.16c-6.38-10.28-9.93-16.95-5.93-24.96,1.46-2.92,2.61-5.12,3.5-6.74,1.35-2.46,3.38-4.47,5.87-5.78L411.38,186.68Z"/>
      <path className="cls-22" d="M289.75,360.97l92.46-118.1,6.24-77.31-22.28-81.7-37.83-39.67-36.76-30.58s64.14,29.97,91.86,80.4c27.72,50.43,34.14,93.98,30.96,130.2-3.19,36.22-13,58.73-22.99,71.66-9.99,12.93-41.05,41.46-52.91,48.45-11.85,6.99-53.7,20.45-53.7,20.45l4.93-3.79Z"/>
      <path className="cls-23" d="M10.82,236.38c-.45-2.55-1.28-5.07-1.42-7.62-.28-5.21-.13-10.43-.28-15.64-.49-17.97,1.35-35.55,5.3-52.85,4.2-18.39,10.92-35.6,19.46-51.94,3.43-6.55,7.48-12.73,11.46-18.91,8.27-12.83,18.14-24.14,29.03-34.4,12.07-11.37,25.32-20.93,39.66-28.86,10.47-5.79,21.35-10.58,32.69-14.03,9.07-2.75,18.48-4.36,27.8-6.2,9.77-1.93,19.75-2.24,29.72-1.88,15.52,.56,27.81-.37,43.49,.77,21.75,1.58,39.8,11.9,55.83,24.17,11.35,8.68,22.07,10.48,31.2,21.7,7.97,9.79,12.35,13.42,17.93,24.86,3.63,7.44,4.78,17.47,7.98,25.14,3.2-2.11,4.24,9.39,4.24,9.39,.41,.94,4.29,10.42,4.7,11.36,1.52,4.78,9.39,31.05,8.43,35.44,2.81,11.81-6.03-1.65-6.16-.62-.54,4.48-5.12,4.87-8.08,8.16q-4.75,5.29-7.14,12.1c-.96,4.39-4.99,13.51-3.84,17.55,1.02,3.58,.39,12.76,1.85,16.2,.21,.49,1.41,7.51,1.87,7.88,.38,.73,1.5,7.14,1.87,7.88,.97,1.83,.71,6.2,1.87,7.88,1.32,1.9,2.49,8.27,4.24,9.39,2.36,1.52,2.84,6.24-.52,14.68-2.46,6.19,3.28-2.85,.32,3.07-3.49,6.98-7.04,13.96-11.13,20.52-5.7,9.13-11.52,18.24-18.04,26.68-7.67,9.95-16.02,19.31-24.33,28.67-3.31,3.72-7.29,6.75-10.95,10.12-.82,.75-1.56,1.6-2.34,2.41-3.73,3.08-7.43,6.21-11.19,9.24-2.75,2.22-5.62,4.29-8.38,6.5-1.51,1.22-37.44,24.6-38.68,22.29-.46-.85-1.28-1.46-1.88-2.23-1.69-2.15-3.75-2.42-6.4-4.13-2.36-1.52-.7,.82-1.82-.51-.69-.81-1.3-2.32-2.33-2.18q-4.38,.56-8.17,1.46c-6.86,1.63-4.52,1.07-11.38,2.71-11.38,2.71-8.24,2.34-18.96,4.51-6.35,1.29-1.28-.12-7.77,.38-9.41,.73-18.86,1.17-28.3,1.61-2.56,.12-5.16-.25-7.74-.34-6.63-.25-3.58-1.1-10.2-1.29-2.41-.07-1.08-4.46-.47-1.97,.47,1.91-.64-1.87,0,0,.35,1.02-2.12-.59-1.9,.45,.67,3.05-.5,.39,.11,3.45-1.8-.75-3.61-1.5-5.41-2.26-5.81-2.45-11.68-4.77-17.43-7.4-12.91-5.92-24.5-14.19-35.27-23.91-11.74-10.59-21.73-22.9-30.43-36.34-4.75-7.34-8.82-15.16-13.34-22.67-1.39-2.32-2.66-4.6-3.13-6.57-1.4-5.91-2.69-5.44-3.15-8.2-1.84-5.51-3.69-11.03-5.53-16.54-.78-2.34-1.62-4.66-2.3-7.03-1.42-4.95-2.75-9.92-4.12-14.88-.32-1.35-.07-.3-.39-1.66-.07-1.05-.63-4.52-.81-5.55Z"/>
      <g className="cls-28">
        <path className="cls-6" d="M95.33,82.2c-15.95,4.42-17.83,4.68-30.71,15.25-14.36,11.79-22.89,30.09-25.57,48.96-2.68,18.87,.09,38.3,5.33,56.73,.77,2.73,1.63,5.49,3.21,7.82,4.87,7.2,14.92,8.1,23.13,6.25,14.77-3.33,27.48-13.03,37.58-24.56,15.43-17.63,25.66-40.09,29.05-63.76,1.59-11.14,1.56-23.15-3.8-33.08s-28.04-17.55-38.21-13.62"/>
      </g>
      <g className="cls-29">
        <path className="cls-6" d="M71.58,98.61c-7.42,2.05-8.5,3.15-14.5,8.07-6.68,5.48-10.64,13.99-11.89,22.77-1.25,8.78,.04,17.81,2.48,26.38,.36,1.27,.76,2.55,1.49,3.64,2.27,3.35,6.94,3.77,10.76,2.91,6.87-1.55,12.78-6.06,17.47-11.42,7.18-8.2,11.93-18.64,13.51-29.65,.74-5.18,.72-10.76-1.77-15.38-2.49-4.62-9.56-9.92-14.29-8.09"/>
      </g>
      <path className="cls-17" d="M388.45,165.55s-3.32-53.36-24.81-85.78c-21.49-32.42-49.91-59.21-78.89-68.28C255.77,2.43,232.63-.25,214.29,.02c-18.35,.27-51.26,8.1-51.26,8.1,0,0,79.29-10.67,122.43,15.9,43.14,26.57,58.32,54.98,65.61,68.95,7.29,13.96,14.73,44.89,16.63,53.15,.79,3.41,1.15,13.55,1.15,13.55l19.6,5.9Z"/>
      <path className="cls-14" d="M229.19,379.66c1.63,2.3,2.25,5.27,4.03,7.77,.57-.42,1.81,1.54,7.65,0,16.97-4.51,35.23-15.41,48.88-26.46,15.16-12.27,32.38-26.91,42.07-36.27,22.94-22.16,42.2-50.73,50.39-81.83,.84-3.2,2.65-11.25,3.25-13.87-2.41-1.04-13.36,.21-15.44-.69-.35,.63-.74,4-1.08,4.62-2.86,10.32-5.55,19.3-10.63,28.72-6.3,10.23-12.77,19.25-19.39,28.62-14.39,20.37-27.84,31.55-51.07,51.43-4.56,3.23-16.25,12.72-20.27,15.73-.69,.65-37.69,21.57-38.37,22.22Z"/>
      <path className="cls-8" d="M374.13,223.14c3.33,8.88,2.36,14.3,7.37,22.24,.51,1.28,1.7,4.51,3.46,5.05,1.55,.47,10.26-2.35,12.32-2.93,.8-.22,5.68-4.13,5.32-5-6.88-16.42-9.82-31.06-10.49-48.87-.19-5.17,1.56-10.68,2.44-16.03,.22-1.36,.29-2.73,.85-4.1,.74-1.82-3.05-5.4-5.22-5.36-1.62,.03-5.49,2.17-6.72,3.02-5.82,4.05-13.04,12.28-13.35,16.86-.46,6.85,.64,15.42,.93,22.17,1.01,4.28,2.07,8.67,3.08,12.95Z"/>
      <path className="cls-8" d="M389.91,169.28c-.47-.5-2.39-6.6-2.76-6.9-4.47-3.69-9.67-4.86-18.08-3.47-8.35,1.38-13.43,4.59-16.19,9.63-4.97,9.08-8.3,24.46-7.39,33.92,1.52,15.84,7,30.24,16.68,44.29,.52,.76,1.02,1.54,1.64,2.26,4.3,4.94,11.96,5.31,18.66,.76,.84-.57,.93-2.01,.67-2.91-4.34-14.88-9.15-29.68-9.6-45.23-.14-4.81,.16-9.69,.81-14.54,.96-7.1,6.62-13.71,15.55-17.8Z"/>
      <path className="cls-9" d="M391.46,174.43s-9.69,9.79-8.29,33.74c1.41,23.95,4.36,39.16,10.16,40.26s17.21-8,18.9-29.22c1.69-21.22-4.81-35.64-10.24-43.07-5.43-7.43-10.53-1.71-10.53-1.71Z"/>
      <path className="cls-12" d="M227.37,370.65c-4.8,1.57-13.11,4.23-17.99,5.57-9.54,2.62-18.37,4.5-28.25,5.75-13.94,1.76-29.68-2.16-43.57-2.61-5.23-.17-11.22-.21-17.19-2.62-1.12-.09-9.29-3.31-10.63-3.37,.12,.76-1.65,8.24-1.59,8.86,6.39,2.52,10.73,6.9,16.08,7.49,18.05,2.89,42.47,7.14,63.72,3.12,13.14-2.49,26.47-6.18,38.39-12.22,2.41-1.07,6.01-2.88,6.38-3.09-.36-.86-2.1-2.48-3.14-3.72q-1.26-2.61-2.2-3.17Z"/>
      <path className="cls-12" d="M118.01,385.7c-.18,.49-9.9-3.68-9.86-3.46,.67,4.09-.32,5.86,1.5,9.74,1.48,3.16,2.32,2.96,6.47,5.65,6.28,1.84,17.41,3.96,25.2,6.09,12.47-.27,28.64,1.14,41.1,.32,10.92-.72,20.29-3.3,30.74-6.39,6.45-1.91,10.14-2.92,13.27-4.81,.63-.21,12.58-4.96,13.07-5.89-3.46-5.54-3.38-3.95-6.78-9.39-3.01,1.46-9.26,4.2-12.53,5.67-8.76,3.93-18.08,6.45-27.58,8.41-18.45,3.81-32.64,1.68-51.22,.46-5.06-.33-10.95-2.77-15.58-4.25-1.02-.32-6.64-1.8-7.79-2.12Z"/>
      <path className="cls-5" d="M137.32,473.91s-8.56-20.51-10.33-27.96-1.36-21.22-1.36-21.22l-45.58-63.71-6.02-5.2s-10.1,6.81-11.17,18.86c-1.07,12.05,4.13,36.82,13.8,47.08,9.67,10.26,32.61,32.33,39.52,36.59,6.91,4.26,21.14,15.55,21.14,15.55Z"/>
      <path className="cls-21" d="M426.77,402.74s5.09,7.35,3.12,13.1q-1.97,5.76-4.81,10.36c-2.84,4.61-6.12,7.35-9.4,10.1-3.28,2.75-10.28,6.38-10.28,6.38l-8.5-27.47s8.22-2.13,16.31-5.84c8.86-4.07,13.56-6.63,13.56-6.63Z"/>
      <path className="cls-12" d="M396.9,415.21l-10.62-39.23,18.26-19.6s5.58-1.33,14.91-7.3c9.33-5.98,18.49,7.23,18.49,7.23,0,0,.44,1.86-.53,6.03-2.35,10.03-1.56,11.31-3.9,16.66-4.51,10.28-28.13,33.62-28.13,33.62l-6.61,2.17"/>
      <path className="cls-5" d="M307.03,483.76s-12.39-52.11-3.44-64.07c8.95-11.96,17.09-25.73,31.15-41.83,14.06-16.1,57.57-48.08,61.3-48.96,3.72-.88,33.76,9.67,37.39,16.67,3.63,7,4.07,8.86,4.52,10.72,0,0-10.54-11.26-27.82-1.25-17.28,10.01-63.38,64.22-72.33,76.18-8.95,11.96-30.76,52.54-30.76,52.54Z"/>
      <ellipse className="cls-6" cx="249.81" cy="569.05" rx="31.72" ry="36.55" transform="translate(-124.85 73.22) rotate(-13.37)"/>
      <path className="cls-13" d="M275.9,562.18c-3.43-15.71-18.19-27.13-33.01-23.64-15.1,3.56-23.94,19.88-19.8,36.83,4.51,18.49,19.11,27.78,34.13,24.08,16.08-3.96,22.41-21.63,18.69-37.28Zm-18.34,33.11c-2.51-.03-5.05,.26-7.54-.16-7.38-1.24-13.61-5.16-17.46-12.8-4.73-9.4-3.01-22.1,3.59-29.13,11.13-11.87,30.4-3.89,35.76,13.22,4.02,12.82-2.81,25.77-14.36,28.87Z"/>
      <path className="cls-10" d="M272.18,524.89s9.61,1.58,10.57,5.61,1.57,6.6,1.57,6.6c0,0-19.85,26.09-21.58,30.19-1.73,4.1-15.93,13.97-16.85,20.77s-1.55,20.71,1.64,25.86c3.19,5.14,14.35,12.13,14.35,12.13l40.1-101.04-25.97-6.33-3.82,6.22Z"/>
      <path className="cls-12" d="M330.69,484.04s14.54-3.75,25.96,1.69c11.42,5.44,17.98,9.49,25.78,17.46,7.8,7.98,8.5,27.47,9.38,31.19,.88,3.72-4.96,14.29-8.64,18.44-3.68,4.15-7.2,8.38-13.86,12.2s-15.43,0-15.43,0l4.7-5.05s10.28-6.38,12.23-14.71c1.95-8.33-1.15-21.36-14.52-28-13.38-6.65-22.59-12.32-34.2-11.53-11.61,.79-15.33,1.68-18.08-1.6-2.75-3.28-.5-6.86,2.86-11.05,3.35-4.19,23.82-9.06,23.82-9.06Z"/>
      <path className="cls-5" d="M353.88,565.03s6.25-4.28,13.19-9.45c6.93-5.16,13.05-13.34,6.45-24.14-6.6-10.8-7.97-14.41-24.83-20.23-16.86-5.82-26.6-5.47-26.6-5.47,0,0-11.61,.79-18.08-1.6-6.47-2.39-15-4.3-22.68-4.44-7.68-.14-13.59,.73-20.03,6.73-6.44,6-6.56,5.49-7.98,7.8-1.42,2.3-3.81,8.77,4.52,10.72,8.33,1.95,13.47-1.24,19.49-.7,6.03,.53,16.21,2.04,17.1,5.76,.88,3.72-13.39,34.64-23.23,42.88-9.84,8.24-19.23,18.34-20.3,30.39s6.19,26.05,23.83,25.79c17.63-.26,27.38-.61,40.5-11.59,13.12-10.98,27.3-34.02,32.98-43.23l5.67-9.21Z"/>
    </g>
  </g>
</svg>
  
  )
}

export default React.memo(Leaning)
