export default function Planet(props) {
  const {
    width = '100%',
    height = '100%',
    className
  } = props

  const css = `
  .planet-1 {
    fill: url(#planet-gradient_14);
  }

  .planet-2 {
    fill: url(#planet-gradient_13);
  }

  .planet-3 {
    fill: url(#planet-gradient_16);
  }

  .planet-4 {
    fill: url(#planet-gradient_14-5);
  }

  .planet-5 {
    fill: url(#planet-gradient_14-6);
  }

  .planet-6 {
    fill: url(#planet-gradient_14-2);
  }

  .planet-7 {
    fill: url(#planet-gradient_14-3);
  }

  .planet-8 {
    fill: url(#planet-gradient_14-4);
  }
  `
	return (
			<svg
				className={`${className} planet`}
        width={width}
        height={height}
				viewBox="0 0 1337.38 1323.11"
				xmlns="http://www.w3.org/2000/svg"
				xmlnsXlink="http://www.w3.org/1999/xlink"
			>
      <defs>
        <style>
          {css}
        </style>
        <radialGradient id="planet-gradient_13" data-name="Ikke-navngivet forløb 13" cx="1088.72" cy="1280.77" fx="1088.72" fy="1280.77" r="2308.88" gradientTransform="translate(-1347.05 -160.83) rotate(-33.48)" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#0a0a4d"/>
          <stop offset=".09" stopColor="#141059"/>
          <stop offset=".29" stopColor="#271b6f"/>
          <stop offset=".42" stopColor="#2f2077"/>
        </radialGradient>
        <linearGradient id="planet-gradient_14" data-name="Ikke-navngivet forløb 14" x1="-9434.59" y1="-4746.75" x2="-8909.56" y2="-4746.75" gradientTransform="translate(2498.53 10740.58) rotate(51.45)" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#0a0a4d"/>
          <stop offset=".18" stopColor="#141059"/>
          <stop offset=".58" stopColor="#271b6f"/>
          <stop offset=".84" stopColor="#2f2077"/>
        </linearGradient>
        <linearGradient id="planet-gradient_14-2" data-name="Ikke-navngivet forløb 14" x1="-7947.56" y1="-6590.02" x2="-7932.9" y2="-6410.58" gradientTransform="translate(11162.23 1289.71) rotate(-33.33)" xlinkHref="#planet-gradient_14"/>
        <linearGradient id="planet-gradient_14-3" data-name="Ikke-navngivet forløb 14" x1="-8776.82" y1="-5105.29" x2="-8813.51" y2="-5006.58" gradientTransform="translate(9480.04 -4956.18) rotate(-60.37)" xlinkHref="#planet-gradient_14"/>
        <linearGradient id="planet-gradient_14-4" data-name="Ikke-navngivet forløb 14" x1="-7942.8" y1="-6343.14" x2="-7971.03" y2="-6189.76" gradientTransform="translate(11162.23 1289.71) rotate(-33.33)" xlinkHref="#planet-gradient_14"/>
        <linearGradient id="planet-gradient_14-5" data-name="Ikke-navngivet forløb 14" x1="-5987.53" y1="-6789.52" x2="-6069.87" y2="-6549.78" gradientTransform="translate(14396.22 -462.72) rotate(-34.4) scale(1.58 1.09) skewX(12.39)" xlinkHref="#planet-gradient_14"/>
        <linearGradient id="planet-gradient_14-6" data-name="Ikke-navngivet forløb 14" x1="-8661.73" y1="-5124.16" x2="-8721.72" y2="-5183.27" gradientTransform="translate(-8443.23 -4296.53) rotate(174.84)" xlinkHref="#planet-gradient_14"/>
        <linearGradient id="planet-gradient_16" data-name="Ikke-navngivet forløb 16" x1="-8313.35" y1="-7268.89" x2="-8333.58" y2="-7209.31" gradientTransform="translate(11147.57 -884.02) rotate(-47.92)" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#0a0a4d"/>
          <stop offset=".12" stopColor="#141059"/>
          <stop offset=".39" stopColor="#271b6f"/>
          <stop offset=".56" stopColor="#2f2077"/>
        </linearGradient>
      </defs>
      <g id="Lag_1-2" data-name="Lag 1">
        <g>
          <path className="planet-2" d="M1026.37,103.27C866.1,2.77,664-30.08,468.85,30.16c-31.59,9.75-61.9,21.64-90.87,35.4C83.84,205.27-69.73,541.03,30.74,858.47c110.37,348.71,485.46,543.22,837.78,434.47,352.33-108.75,548.47-479.6,438.11-828.31-49.24-155.56-151.18-280.4-280.27-361.36Z"/>
          <g id="Group_10962" data-name="Group 10962">
            <path id="Ellipse_4912-2" data-name="Ellipse 4912-2" className="planet-1" d="M595.05,382.61c145.04,55.5,179.28,207.85,146.74,310.23-24.03,71.74-76.95,129.76-145.84,159.9-85.45,35.93-213.7,25.32-292.02-74.07-168.54-213.89,3.8-506,291.13-396.05Z"/>
          </g>
          <g id="Group_10963" data-name="Group 10963">
            <path id="Ellipse_4912-3" data-name="Ellipse 4912-3" className="planet-6" d="M877,236.72c-6.19-52.71,34.99-86.76,71.56-92.76,25.82-3.83,52.05,3.26,72.3,19.52,24.67,20.39,41.92,61.04,24.01,100.26-38.57,84.43-155.6,77.38-167.87-27.03Z"/>
          </g>
          <g id="Group_10975" data-name="Group 10975">
            <path id="Ellipse_4912-4" data-name="Ellipse 4912-4" className="planet-7" d="M710.25,215.01c-13.76-13.36-14.46-35.29-1.59-49.8,7.44-8.24,17.96-13.02,29.01-13.16,16.3,.05,30.53,10.78,35.01,26.41,10.19,38.14-30.13,67.5-62.42,36.55Z"/>
          </g>
          <g id="Group_10976" data-name="Group 10976">
            <path id="Ellipse_4912-5" data-name="Ellipse 4912-5" className="planet-8" d="M1006.23,446.68c-5.05-42.96,28.52-70.71,58.32-75.6,21.04-3.12,42.42,2.66,58.92,15.91,20.1,16.62,34.17,49.75,19.56,81.72-31.44,68.81-126.82,63.07-136.81-22.03Z"/>
          </g>
          <g id="Group_10963-2" data-name="Group 10963">
            <path id="Ellipse_4912-3-2" data-name="Ellipse 4912-3" className="planet-4" d="M336.85,237.79c-11.83-54.66,52.32-106.14,110.18-125.69,40.86-13.36,82.8-15.1,115.58-4.79,39.95,13.19,68.89,50.88,41.91,99.62-58.09,104.9-244.25,139.14-267.66,30.86Z"/>
          </g>
          <g id="Group_10975-2" data-name="Group 10975">
            <path id="Ellipse_4912-4-2" data-name="Ellipse 4912-4" className="planet-5" d="M715.79,66.75c-3.51,14.11-21.34,22.88-40.49,19.92-10.95-1.77-20.73-6.88-26.94-14.09-8.95-10.74-7.88-24.16,2.63-33.03,26.07-21.15,72.7-5.77,64.8,27.2Z"/>
          </g>
          <g id="Group_10975-3" data-name="Group 10975">
            <path id="Ellipse_4912-4-3" data-name="Ellipse 4912-4" className="planet-3" d="M177.78,466.2c-13.6-5.49-19.84-24.24-14.16-42.58,3.32-10.48,9.81-19.32,17.9-24.36,12-7.21,25.26-4.22,32.63,7.36,17.44,28.62-4.54,72.1-36.36,59.58Z"/>
          </g>
        </g>
      </g>
    </svg>
	)
}
