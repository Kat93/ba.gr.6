import { useContext } from "react"
import { ControlContext } from "../../contexts/ControlContext"
import styles from './MobileControls.module.scss'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faChevronLeft, faChevronRight, faChevronUp, faTriangleCircleSquare } from "@fortawesome/free-solid-svg-icons"

export default function MobileControls() {
  const {
     left, 
     setLeft, 
     right,
     setRight,
     up,
     setUp,
     space, 
     setSpace, 
    } = useContext(ControlContext)

  return (
    <div className={styles.container}>
      <button
        className={`${styles.left} ${left ? styles.pressed : null}`}
        onMouseDown={() => setLeft(true)}
        onMouseUp={() => setLeft(false)}
        onTouchStart={() => setLeft(true)}
        onTouchEnd={() => setLeft(false)}
      >
        <div className={styles.icon}>
          <FontAwesomeIcon icon={faChevronLeft} />
        </div>
      </button>

      <button
        className={`${styles.space} ${up ? styles.pressed : null}`}
        onMouseDown={() => setUp(true)}
        onMouseUp={() => setUp(false)}
        onTouchStart={() => setUp(true)}
        onTouchEnd={() => setUp(false)}
      >
        <div className={styles.icon}>
          <FontAwesomeIcon icon={faChevronUp} />
        </div>
      </button>

      <button
        className={`${styles.right} ${right ? styles.pressed : null}`}
        onMouseDown={() => setRight(true)}
        onMouseUp={() => setRight(false)}
        onTouchStart={() => setRight(true)}
        onTouchEnd={() => setRight(false)}
      >
        <div className={styles.icon}>
          <FontAwesomeIcon icon={faChevronRight} />
        </div>
      </button>
    </div>
  )
}