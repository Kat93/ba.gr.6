import styles from './Text.module.scss'
export default function Text(props) {
  const { text, purpose } = props;


  const purposeClass =
    purpose === "bannerTitleText"
      ? styles.bannerTitleText
      : purpose === "bannerText"
      ? styles.bannerText
      : purpose === "bannerPText"
      ? styles.bannerPText
      : null;

  return <p className={`${styles.text} ${purposeClass}`}>{text}</p>;
}
