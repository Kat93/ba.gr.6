import styles from "./Heading.module.scss";
export default function Heading(props) {
  const { headingLevel, text, size } = props;

  const Tag = headingLevel;

  const sizeClass =
    size === "small"
      ? styles.small
      : size === "medium"
      ? styles.medium
      : size === "large"
      ? styles.large
      : null;

  return <Tag className={`${styles.heading} ${sizeClass}`}>{text}</Tag>;
}
