import Heading from "./Heading";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "Components/UI/Headings",
  component: Heading,
  argTypes: {
    headingLevel: {
      description: "Select heading level between 1 and 6",
      control: "select",
      options: ["h1", "h2", "h3", "h4", "h5", "h6"],
      table: { defaultValue: { summary: "No default" } },
    },
    text: {
      control: { type: "text" },
      name: "text",
      description: "Write you text",
      type: { required: true },
      defaultValue: "Heading",
      table: { defaultValue: { summary: "No default" } },
    },
    size: {
      description: "Select size, pick between small, medium, large",
      control: "select",
      options: ["small", "medium", "large"],
      table: { defaultValue: { summary: "No default" } },
    },
  },
};

const Template = (args) => <Heading {...args} />;
export const HeadingH1 = Template.bind({});
export const HeadingH1Centered = Template.bind({});
export const HeadingH2 = Template.bind({});
export const HeadingH3 = Template.bind({});

HeadingH1.args = {
  headingLevel: "h1",
  size: "medium",
};

HeadingH1Centered.args = {
  headingLevel: "h1",
  size: "medium",
};
HeadingH1Centered.parameters = {
  layout: 'centered',
};

HeadingH2.args = {
  headingLevel: "h2",
};
HeadingH3.args = {
  headingLevel: "h3",
  size: "small"
};
