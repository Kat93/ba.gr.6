import styles from "./ProgressBar.module.scss";
import CountUp from "../CountUp/CountUp";
import { Tooltip } from "react-tippy";
import useProcentageFinder from "../../../hooks/useProcentageFinder";

export default function ProgressBar(props) {
  const { treesPlanted, goal } = props;
  const completed = useProcentageFinder(treesPlanted, goal);

  const progessStyle = {
    width: `${completed}%`,
  };

  const tooltipTitle = `${treesPlanted} trees planted`;
  const roundedCompleted = Math.round(completed);

  return (
    <>
      <div className={styles.ProgressBarWrapper}>
        <p>0</p>
        <div className={styles.ProgressBar}>
          <div style={progessStyle} className={styles.filler}>
            <Tooltip
              title={tooltipTitle}
              position="top"
              trigger="mouseenter"
              arrow="true"
              distance="25"
              theme="transparent"
            >
              <CountUp
                className={styles.LabelStyle}
                number={roundedCompleted.toString()}
                duration={4}
                label={"%"}
              />
            </Tooltip>
          </div>
        </div>
        <p> 1 Million</p>
      </div>
    </>
  );
}
