import ProgressBar from "./ProgressBar";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    title: "Components/UI/ProgressBar",
    component: ProgressBar
}

const Template = (args) => <ProgressBar {...args} />;
export const PrimaryProgressBar = Template.bind({});

PrimaryProgressBar.args ={
    treesPlanted: 60887, 
    goal: 1000000
}