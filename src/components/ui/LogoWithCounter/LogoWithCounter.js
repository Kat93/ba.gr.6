import styles from './LogoWithCounter.module.scss'
import Count from '../CountUp/CountUp'
import MorningwoodLogo from '../svg/MorningwoodLogo'

export default function LogoWithCounter(props) {
  const {
    trees = 'Trees Planted',
    number = 0,
    forestName
  } = props
  return (
    <div className={styles.container}>
      <div className={styles.container_logo}>
        <div className={styles.logo}>
          <MorningwoodLogo />
        </div>
        <div className={styles.text}>{forestName}</div>
      </div>
      <div className={styles.line}></div>
      <div className={styles.container_count}>
        <div className={styles.count}>
          <Count number={String(number)} duration={1} />
        </div>
        <div className={`${styles.text} ${styles.left}`}>{trees}</div>
      </div>
  </div>
  )
}