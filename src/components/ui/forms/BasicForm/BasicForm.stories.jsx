import BasicForm from "./BasicForm";
import { PrimaryTextInput } from "../../inputs/TextInput.stories";
import { ButtonPrimary } from "../../buttons/Button/Button.stories";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "Components/UI/forms/BasicForm",
  component: BasicForm,
};

const Template = (args) => <BasicForm {...args} />;
export const SmallForm = Template.bind({});
export const SmallCenteredForm = Template.bind({});
export const MediumForm = Template.bind({});
export const LargeForm = Template.bind({});

SmallForm.args = {
  size: "small",
  children: (
    <>
      <PrimaryTextInput {...PrimaryTextInput.args} />
      <PrimaryTextInput {...PrimaryTextInput.args}/>
      <div>
      <ButtonPrimary {...ButtonPrimary.args}/>
      </div>
      
    </>
  ),
};

MediumForm.args = {
  size: "medium",
  children: (
    <>
      <PrimaryTextInput {...PrimaryTextInput.args} />
      <PrimaryTextInput {...PrimaryTextInput.args}/>
      <div>
      <ButtonPrimary {...ButtonPrimary.args}/>
      </div>
      
    </>
  ),
};

LargeForm.args = {
  size: "large",
  children: (
    <>
      <PrimaryTextInput {...PrimaryTextInput.args} />
      <PrimaryTextInput {...PrimaryTextInput.args}/>
      <div>
      <ButtonPrimary {...ButtonPrimary.args}/>
      </div>
      
    </>
  ),
};

SmallCenteredForm.args = {
  size: "small",
  layout: "centered",
  children: (
    <>
      <PrimaryTextInput {...PrimaryTextInput.args} />
      <PrimaryTextInput {...PrimaryTextInput.args}/>
      <div>
      <ButtonPrimary {...ButtonPrimary.args}/>
      </div>
      
    </>
  ),
};