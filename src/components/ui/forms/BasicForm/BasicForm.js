import styles from "./BasicForm.module.scss";
import PropTypes from "prop-types";
import TextInput from "../../inputs/TextInput";
import Button from "../../buttons/Button/Button";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import GlowingButton from "../../buttons/GlowingButton/GlowingButton";

export default function BasicForm(props) {
  const { children, handleSubmit, size, layout } = props;

  const sizeClass =
    size === "small"
      ? styles.small
      : size === "medium"
      ? styles.medium
      : size === "large"
      ? styles.large
      : null;

  const layoutClass =
    layout === "right"
      ? styles.right
      : layout === "centered"
      ? styles.centered
      : layout === "left"
      ? styles.left
      : null;

  return (
    <form className={`${styles.BasicForm} ${sizeClass}  ${layoutClass}`} onSubmit={handleSubmit}>
      {children}
    </form>
  );
}
const FormTypes = [
  PropTypes.objectOf(TextInput),
  PropTypes.objectOf(Button),
  PropTypes.objectOf(PrimaryButton),
  PropTypes.objectOf(GlowingButton),
];

BasicForm.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.oneOfType([...FormTypes])),
    ...FormTypes,
  ]),
  size: PropTypes.oneOf(["small", "medium", "large"]),
  layout: PropTypes.oneOf(["right", "centered", "left"]),
};
