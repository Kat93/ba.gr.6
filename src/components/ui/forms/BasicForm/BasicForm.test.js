import Legend from "./BasicForm";
import { render } from "@testing-library/react";
import BasicForm from "./BasicForm";

test("the basic form will show its children", () => {
  //act
  const { getByText } = render(
    <BasicForm>
      <div>test</div>
      <div>test2</div>
    </BasicForm>
  );

  //assert

  expect(getByText("test")).toBeTruthy();
  expect(getByText("test2")).toBeTruthy();
});
