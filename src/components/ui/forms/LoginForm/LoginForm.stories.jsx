import LoginForm from "./LoginForm";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "Components/UI/forms/LoginForm",
  component: LoginForm,
};

const Template = (args) => <LoginForm {...args} />;

export const PrimaryLoginForm = Template.bind({});

PrimaryLoginForm.args = {
  headingLevel: "h3",
  headingText: "Log into Morningwood",
  formSize: "small",
  formLayout: "centered",
};
