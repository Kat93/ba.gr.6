import styles from "./LoginForm.module.scss";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import TextInput from "../../inputs/TextInput";
import Heading from "../../text/Heading/Heading";
import BasicForm from "../BasicForm/BasicForm";
import PropTypes from "prop-types";
import { useState } from "react";
import { useRouter } from "next/router";
import { signIn } from "next-auth/react";

export default function LoginForm(props) {
  const { headingLevel, headingText, formSize, formLayout, emailPlaceholder } =
    props;

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const router = useRouter();

  const backToHome = (e) => {
    e.preventDefault();
    router.push("/");
  };

  const signInUser = async (e) => {
    e.preventDefault();
    console.log(email, password);
    const options = {
      email,
      password,
      redirect: false,
    };
    const res = await signIn("credentials", options);

    console.log(res);

    router.push("/");
  };

  return (
    <>
      <div className={styles.loginFormWrapper}>
        <Heading text={headingText} headingLevel={headingLevel} />

        <BasicForm size={formSize} layout={formLayout}>
          <TextInput
            type="email"
            autoComplete="on"
            inputId="email"
            labelText="Email:"
            placeholder="Please insert your email"
            value={email}
            handleChange={setEmail}
          />
          <TextInput
            type="password"
            inputId="password"
            labelText="Password:"
            placeholder="Please insert your password"
            value={password}
            handleChange={setPassword}
          />
          <div className={styles.ButtonWrapper}>
            <PrimaryButton
              hover="primaryHover"
              focus="primaryFocus"
              type="submit"
              size="small"
              text="Sign in"
              handleClick={signInUser}
            />

            <PrimaryButton
              hover="primaryHover"
              focus="primaryFocus"
              type="submit"
              size="small"
              text="Back"
              handleClick={backToHome}
            />
          </div>
        </BasicForm>
      </div>
    </>
  );
}

LoginForm.propTypes = {
  headingLevel: PropTypes.oneOf(["h5", "h4", "h3"]),
  headingText: PropTypes.string,
  formSize: PropTypes.oneOf(["small", "medium", "large"]),
  formLayout: PropTypes.oneOf(["left", "centered", "right"]),
};
