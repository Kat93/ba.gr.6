import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import { useEffect, useRef } from 'react'
import Card from '../cards/Card'
import styles from './IconBox.module.scss'

export default function IconBox(props) {
    const {
        icon,
        text
    } = props


    
    useEffect(() => {
    })
    return (
        <Card>
            <div className={styles.icon}>
                {icon}
            </div>
            <div className={styles.content}  >
                {documentToReactComponents(text)}
            </div>
        </Card>
    )
}