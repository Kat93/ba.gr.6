import WithBackground from '../backgrounds/WithBackground/WithBackground'
import Text from '../text/Text/Text'
import styles from './Banner.module.scss'
import ProgressBar from '../ProgressBar/ProgressBar'

export default function Banner(props) {
  const {content, trees} = props

    return (
        <WithBackground src={"/img/galaxy-new-min.jpg"}>
          <div className={`${styles.banner} `}>
            <Text purpose={"bannerTitleText"} text={content?.BannerContent?.smallText} />
            <Text purpose={"bannerText"} text={content?.BannerContent?.bigText} />
            <div className={styles.bannerProgress}>
            <ProgressBar goal={1000000} treesPlanted={trees}/>
            </div>
            <Text purpose={"bannerPText"} text={content?.BannerContent?.pText} />
          </div>
        </WithBackground>
    )
}