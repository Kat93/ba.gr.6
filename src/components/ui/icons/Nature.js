import React from 'react'

export default function Local(props) {
    const {
        width = "100%",
        height,
        color = 'currentColor'
    } = props

    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 114.6 66.63" width={width} height={height} fill={color} {...props}>
            <path d="M48.87,7.85C30.23-7.32,12.19,3.1,4.35,9.23c0,0,7.7-1.04,25.1,13.28,29.55,24.32,40.94,25.82,50.11,28.5l-.63-.13c-22.16-2.05-40.56-15.19-54.42-24.37C11.79,18.07,1.06,16.99,1.06,16.99-5.82,51.81,22.11,61.66,47.21,62.81c26.21,1.19,50.29-9.95,50.29-9.95-18.31-.56-23.81-24.79-48.63-45h0Z"/>
        </svg>
    )
}
