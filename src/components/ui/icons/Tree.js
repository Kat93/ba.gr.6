export default function Tree(props) {
  const {
    width = '100%',
    height = '100%'
  } = props
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 129.057 164.702">
      <g id="Group_20529" data-name="Group 20529" transform="translate(-1590.813 -1343.881)">
        <path id="Path_1" data-name="Path 1" d="M1632.777,1493.214l7.677-98.984h9.616l.78,21.93,2.748,77.055s-3.177,2.561-10.412,2.736S1632.777,1493.214,1632.777,1493.214Z" transform="translate(10.522 12.625)" fill="#fff"/>
        <path id="Path_2" data-name="Path 2" d="M1670.6,1407.788l-24.35,14.026v6.97l28.2-19.144,5.676-24.35-3.441-1.853Z" transform="translate(13.901 9.919)" fill="#fff"/>
        <ellipse id="Ellipse_4" data-name="Ellipse 4" cx="52.024" cy="37.31" rx="52.024" ry="37.31" transform="translate(1603.628 1343.881)" fill="#fff"/>
        <ellipse id="Ellipse_5" data-name="Ellipse 5" cx="23.45" cy="27.17" rx="23.45" ry="27.17" transform="translate(1662.592 1419.132) rotate(-86.266)" fill="#fff"/>
        <path id="Path_3" data-name="Path 3" d="M1616.238,1413.585l33.11,19.076v9.477L1611,1416.1l-7.714-33.11,4.678-2.519Z" transform="translate(3.127 9.176)" fill="#fff"/>
        <ellipse id="Ellipse_6" data-name="Ellipse 6" cx="27.17" cy="23.45" rx="27.17" ry="23.45" transform="translate(1590.813 1389.232) rotate(-4.172)" fill="#fff"/>
      </g>
    </svg>

  )
}