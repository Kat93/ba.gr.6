import React from 'react'
import PropTypes from 'prop-types'

function Arrow (props) {
  const {
    color = 'currentColor',
    direction = 'right',
    className,
    width = '0.875rem',
    height = '0.875rem'
  } = props

  const arrows = {
    up:
  <svg className={className} fill={color} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox='0 0 12 14'>
    <path
      id='Path_11374' data-name='Path 11374'
      d='M110.707,1848.292l-5-5a1,1,0,0,0-1.414,0l-5,5a1,1,0,0,0,1.414,1.414l3.293-3.293V1856a1,1,0,0,0,2,0v-9.586l3.293,3.293a1,1,0,1,0,1.414-1.414Z'
      transform='translate(-99 -1842.999)' fill={color}
    />
  </svg>,
    right:
  <svg className={className} fill={color} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox='0 0 14 12'>
    <path
      id='Path_11376' data-name='Path 11376'
      d='M170.707,1855.706l5-5a1,1,0,0,0,0-1.414l-5-5a1,1,0,0,0-1.414,1.414l3.293,3.293H163a1,1,0,0,0,0,2h9.586l-3.293,3.293a1,1,0,0,0,1.414,1.414Z'
      transform='translate(-162 -1843.999)' fill={color}
    />
  </svg>,
    down:
  <svg className={className} fill={color} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox='0 0 12 14'>
    <path
      id='Path_11378' data-name='Path 11378'
      d='M238.707,1851.706l-5,5a1,1,0,0,1-1.414,0l-5-5a1,1,0,0,1,1.414-1.414l3.293,3.293V1844a1,1,0,0,1,2,0v9.586l3.293-3.293a1,1,0,0,1,1.414,1.414Z'
      transform='translate(-227 -1842.999)' fill={color}
    />
  </svg>,
    left:
  <svg className={className} fill={color} xmlns='http://www.w3.org/2000/svg' width={width} height={height} viewBox='0 0 14 12'>
    <path
      id='Path_11380' data-name='Path 11380'
      d='M295.293,1855.706l-5-5a1,1,0,0,1,0-1.414l5-5a1,1,0,1,1,1.414,1.414L293.414,1849H303a1,1,0,0,1,0,2h-9.586l3.293,3.293a1,1,0,1,1-1.414,1.414Z'
      transform='translate(-290 -1843.999)' fill={color}
    />
  </svg>

  }

  return (
    arrows[direction]
  )
}

Arrow.protoTypes = {
  /**
     * Arrow direction up | right | down | left
     */
  direction: PropTypes.string.isRequired
}

export default React.memo(Arrow)
