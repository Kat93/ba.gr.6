import { faHome, faTree } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import ButtonIcon from '../ButtonIcon/ButtonIcon'
import 'react-tippy/dist/tippy.css';
import { Tooltip } from 'react-tippy';

export default function ForestSwitcherButton({forestUrl = "/lottes-butik"}) {
    const router = useRouter()

    console.log(router)
  return router.pathname === "/" ? (
    <Tooltip title="Visit your part of Morningwood" position="top" trigger="mouseenter" arrow="true" distance="25" theme="transparent">
    <ButtonIcon layout={"secondary"} size={"small"} border={"glowing"} onClick={() => router.push(forestUrl)}>
        <FontAwesomeIcon icon={faTree} />
    </ButtonIcon>
    </Tooltip>
  ) : (
    <Tooltip title="Back to Morningwood" position="top" trigger="mouseenter" arrow="true" distance="25">
    <ButtonIcon layout={"secondary"} size={"small"} border={"glowing"} onClick={() => router.push("/")}>
        <FontAwesomeIcon icon={faHome} />
    </ButtonIcon>
    </Tooltip>
  )
}
