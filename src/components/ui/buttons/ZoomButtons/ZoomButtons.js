import ButtonIcon from "../ButtonIcon/ButtonIcon";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlassPlus, faMagnifyingGlassMinus} from "@fortawesome/free-solid-svg-icons";
import 'react-tippy/dist/tippy.css';
import { Tooltip } from 'react-tippy';

export default function ZoomButtons({onZoomIn, onZoomOut, startCounterZoomIn, startCounterZoomOut, stopCounter}) {
  
  return (
    <>
    <Tooltip title="Zoom in" position="top" trigger="mouseenter" arrow="true" distance="25" theme="transparent">
      <ButtonIcon onMouseDown={startCounterZoomIn} onMouseUp={stopCounter} onClick={onZoomIn} layout={"secondary"}  border={"glowing"} size={"small"}>
        <FontAwesomeIcon icon={faMagnifyingGlassPlus}/>
      </ButtonIcon>
      </Tooltip>

    <Tooltip title="Zoom out" position="top" trigger="mouseenter" arrow="true" distance="25" theme="transparent">
      <ButtonIcon  onMouseDown={startCounterZoomOut} onMouseUp={stopCounter} onClick={onZoomOut}  layout={"secondary"} border={"glowing"} size={"small"}>
        <FontAwesomeIcon icon={faMagnifyingGlassMinus} />
      </ButtonIcon>
      </Tooltip>
    </>
  );
}


