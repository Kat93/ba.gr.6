import ZoomButtons from "./ZoomButtons";

export default {
  title: "Components/UI/ButtonsZoom",
  component: ZoomButtons,
  argTypes: {
  }
};

const Template = (args) => <ZoomButtons {...args}/>;

export const ButtonsZoomDefault = Template.bind({});

ButtonsZoomDefault.args = {
}

