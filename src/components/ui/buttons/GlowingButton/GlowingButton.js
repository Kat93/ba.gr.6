import Button from "../Button/Button";

export default function GlowingButton(props) {
    return <Button layout={"secondary"} border={"glowing"} {...props} />
}

