import GlowingButton from "./GlowingButton";

export default {
  title: "Components/UI/ButtonGlowing",
  component: GlowingButton,
  argTypes: {

    text: {
      control: { type: "text" },
      name: "text",
      description: "Write you text",
      defaultValue: "click me",
      table: { defaultValue: { summary: "No default" } },
    },
  }
};

const Template = (args) => <GlowingButton {...args}/>;

export const ButtonGlowingDefault = Template.bind({});

ButtonGlowingDefault.args = {
  text: "click me",
};

