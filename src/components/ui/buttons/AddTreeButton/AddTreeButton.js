import ButtonIcon from '../ButtonIcon/ButtonIcon'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from './AddTreeButton.module.scss'
import {faCartShopping} from "@fortawesome/free-solid-svg-icons";
import 'react-tippy/dist/tippy.css';
import { Tooltip } from 'react-tippy';

export default function AddTreeButton() {

  return (
    <>
    <ButtonIcon className={styles.addTree} layout={"secondary"} border={"glowing"} size={"large "} text={"Buy a tree"}>
        <FontAwesomeIcon icon={faCartShopping}/>
      </ButtonIcon>
      </>
  )
}
