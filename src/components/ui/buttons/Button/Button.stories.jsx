import Button from "./Button";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "Components/UI/ButtonPrimary",
  component: Button,
  argTypes: {

    text: {
      control: { type: "text" },
      name: "text",
      description: "Write you text",
      defaultValue: "click me",
      table: { defaultValue: { summary: "No default" } },
    },

    layout:{
      control: "select",
      options: ["primary"],
      table: { defaultValue: { summary: "primary" } }
    },

    border:{
      controle: "select",
      options: ["none"],
      table: { defaultValue: { summary: "none" } }
    },

    hover:{
      controle: "select",
      options: ["primaryHover", "none"],
      table: { defaultValue: { summary: "primaryHover" } },
    },

    focus:{
      controle: "select",
      options: ["primaryFocus", "none"],
      table: { defaultValue: { summary: "primaryFocus" } },
    },

    active:{
      controle: "select",
      options: ["primaryActive", "none"],
      table: { defaultValue: { summary: "primaryActive" } },
    },

    disabled:{
      controle: "select",
      options: ["primaryDisabled", "none"],
      table: { defaultValue: { summary: "primaryDisabled" } },
    }

  }
};

const Template = (args) => <Button {...args}/>;

export const ButtonPrimary = Template.bind({});
export const ButtonHover = Template.bind({});
export const ButtonFocus = Template.bind({});
export const ButtonActive = Template.bind({});
export const ButtonDisabled = Template.bind({});

ButtonPrimary.args = {
  layout: "primary",
  size: "medium",
  text: "click me",
  hover: "primaryHover",
  focus: "primaryFocus",
  active: "primaryActive",
  disabled: "none",
  border: "none", 
  type: "button"
};

ButtonHover.args = {
  layout: "primary",
  size: "medium",
  text: "click me",
  hover: "primaryHover",
  focus: "none",
  active: "none",
  disabled: "none",
  border: "none", 
  type: "button"
  
};

ButtonFocus.args = {
  layout: "primary",
  size: "medium",
  text: "click me",
  hover: "none",
  focus: "primaryFocus",
  active: "none",
  disabled: "none",
  border: "none",
  type: "button"

};

ButtonActive.args = {
  layout: "primary",
  size: "medium",
  text: "click me",
  hover: "none",
  focus: "none",
  active: "primaryActive",
  disabled: "none",
  border: "none",
  type: "button"

};

ButtonDisabled.args = {
  layout: "primary",
  size: "medium",
  text: "click me",
  hover: "none",
  focus: "none",
  active: "none",
  disabled: "primaryDisabled",
  border: "none",
  type: "button"

};


