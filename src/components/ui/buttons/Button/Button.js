import styles from './Button.module.scss'
import PropTypes from 'prop-types'

export default function Button(props) {
    const {
        text,
        layout = 'primary',
        border,
        size = "medium",
        hover = "hoverPrimary",
        focus = "focusPrimary",
        active = "activePrimary",
        disabled = "disabledPrimary",
        type = "button",
        className,
        handleClick
    } = props

    const layoutClass = 
        layout === 'primary' ? styles.primary : 
        layout === 'secondary' ? styles.secondary :
        null

    const borderClass = 
        border === 'glowing' ? styles.glowing :
        null

    const sizeClass = 
        size === 'small' ? styles.small : 
        size === 'medium' ? styles.medium :
        size === 'large' ? styles.large :
        null

    const hoverClass = 
        hover === 'primaryHover' ? styles.hoverPrimary : 
        hover === 'secondaryHover' ? styles.hoverSecondary : 
        null

    const focusClass = 
        focus === 'primaryFocus' ? styles.focusPrimary : 
        null

    const activeClass = 
        active === 'primaryActive' ? styles.activePrimary : 
        active === 'secondaryActive' ? styles.activeSecondary : 
        null

    const disabledClass = 
        disabled === 'primaryDisabled' ? styles.disabledPrimary : 
        disabled === 'secondaryDisabled' ? styles.disabledSecondary : 
        null
    

    return (
        <button 
            onClick={(e) => handleClick(e)}
            type={type} 
            className={`
                ${className} 
                ${styles.button} 
                ${layoutClass} 
                ${borderClass} 
                ${sizeClass} 
                ${hoverClass} 
                ${focusClass} 
                ${activeClass} 
                ${disabledClass}`}
            >
            {text}
        </button>
    )
}

Button.propTypes = {
    layout: PropTypes.oneOf(['primary', 'secondary']),
    border: PropTypes.oneOf(['glowing', 'none']),
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    text: PropTypes.string,
    hover: PropTypes.oneOf(['primaryHover', 'secondaryHover','none']),
    focus: PropTypes.oneOf(['primaryFocus','none']),
    active: PropTypes.oneOf(['primaryActive', 'secondaryActive','none']),
    disabled: PropTypes.oneOf(['primaryDisabled', 'secondaryDisabled','none']),
    type: PropTypes.oneOf(['submit', 'reset','button']),
}