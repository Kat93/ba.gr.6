import styles from './ButtonIcon.module.scss'
import PropTypes from 'prop-types'

export default function ButtonIcon(props) {
    const {
        text,
        children,
        layout = 'primary',
        border,
        size = "medium",
        className,
        handleClick
    } = props

    const layoutClass = 
        layout === 'primary' ? styles.primary : 
        layout === 'secondary' ? styles.secondary :
        null

    const borderClass = 
        border === 'glowing' ? styles.glowing :
        null

    const sizeClass = 
        size === 'small' ? styles.small : 
        size === 'medium' ? styles.medium :
        size === 'large' ? styles.large :
        null
    

    return (
        <button className={`${styles.buttonIcon} ${layoutClass} ${borderClass} ${sizeClass} ${className}`} onClick={()=>handleClick()}>
         {children} {text}
        </button>
    )
}

ButtonIcon.propTypes = {
    layout: PropTypes.oneOf(['primary', 'secondary']),
    border: PropTypes.oneOf(['glowing']),
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    text: PropTypes.string
}