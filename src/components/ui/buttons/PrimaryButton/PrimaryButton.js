import React from 'react'
import Button from '../Button/Button'

export default function PrimaryButton(props) {
  return (
    <Button layout="primary" {...props} />
  )
}
