import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { signOut } from 'next-auth/react'
import GlowingButton from '../GlowingButton/GlowingButton' 
import styles from './LoginButton.module.scss'
import 'react-tippy/dist/tippy.css';
import { Tooltip } from 'react-tippy';

export default function LoginButton(props) {
    const {
        loggedIn,
    } = props

    const [text, setText] = useState()
    const [destination, setDetination] = useState()
    const router = useRouter()
    const handleClick = (e) => {
        e.preventDefault()
        if(loggedIn) {
            signOut()
        } else {
            router.push("/login")
        }
    }

    useEffect(() => {
        const buttonText = loggedIn ? "Log out" : "Log in"
        setText(buttonText)
    }, [loggedIn])

  return (
    <GlowingButton
        className={styles.login}
        text={text}
        handleClick={handleClick}
    />
  )
}
