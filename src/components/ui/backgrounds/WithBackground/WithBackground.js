import Image from 'next/image'
import React from 'react'
import styles from './WithBackground.module.scss'
import PropTypes from 'prop-types'

export default function WithBackground(props) {
    const {
        src,
        alt
    } = props
  return (
    <div className={styles.container}>
      <Image 
          src={src}
          alt={alt}
          layout={"fill"}
          objectFit={"cover"}
          className={styles.image}
      />
      <div className={styles.children}>
        {props.children}
      </div>
    </div>
  )
}

WithBackground.proptypes = {
  src: PropTypes.string,
  alt: PropTypes.string
}
