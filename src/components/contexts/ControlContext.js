import React from "react"
import { useEffect, useRef, useState } from "react"

export const ControlContext = React.createContext()

const ControlProvider = ({children}) => {
  const controls = useRef(null)
  const [left, setLeft] = useState(false)
  const [right, setRight] = useState(false)
  const [up, setUp] = useState(false)
  const [space, setSpace] = useState(false)
  const [isMoving, setIsMoving] = useState(false)

  const handleKeys = (event, isPressed) => {
    event.preventDefault()
    const key = event.key
    switch(key) {
      case 'a':
      case 'h':
      case 'ArrowLeft':
        setLeft(isPressed)
        break
      case 'd':
      case 'l':
      case 'ArrowRight':
        setRight(isPressed)
        break
      case 'w':
      case 'k':
      case 'ArrowUp':
      case ' ': // added space temporarily
        setUp(isPressed)
        break
      case ' ':
        setSpace(isPressed)
        break
      default:
        return
    }
  }

  useEffect(() => {
    controls.current.focus()
  }, [])

  return (
    <ControlContext.Provider value={{left, setLeft, right, setRight, up, setUp, space, setSpace, isMoving, setIsMoving}}>
      <div
        tabIndex="1"
        ref={controls}
        onKeyDown={() => handleKeys(event, true)}
        onKeyUp={() => handleKeys(event, false)}
        >
        {children}
      </div>
    </ControlContext.Provider>
  )
}

export default ControlProvider