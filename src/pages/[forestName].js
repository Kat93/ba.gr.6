import React from 'react'
import Header from "../components/layout/Header/Header";
import LogoWithCounter from "../components/ui/LogoWithCounter/LogoWithCounter";
import Footer from "../components/layout/Footer/Footer";
import content from "../data/data.json";
import Banner from "../components/ui/Banner/Banner";
import Forest from '../components/partials/Forest';
import MainContent from '../components/partials/MainContent';
import FooterInfo from '../components/partials/FooterInfo';
import * as contentful from 'contentful'
// import User from '../models/User';
// import dbConnect from '../lib/dbConnect';
import { useSession } from 'next-auth/react';

const client = contentful.createClient({
  space: 'kq6vfz8ffqad',
  accessToken: 'RRSlBA1ViSrwWlBROgru5F9VQYUja8SO7dhbQyk2jmg'
})


export default function ForestPage(props) {
  const session = useSession()
  const forest = {
    name: session.data?.user?.name || "Default",
    treesPlanted: session.data?.user?.treesPlanted || 1234
  }
    return (
      <>
			<Header>
				<LogoWithCounter number={forest.treesPlanted} forestName={forest.name} />
			</Header>

      <Forest trees={forest.treesPlanted} />

      <MainContent background="/img/trees.png" content={props.content} user={forest} />

      <Banner trees={forest.treesPlanted}  content={content} />

			<Footer>		
          <FooterInfo />
			</Footer>
		</>
  )
}

export const getStaticPaths = async (ctx) => {
  const res = await fetch('http://localhost:3000/api/user')
  const data = await res.json()
  console.log('sduheqwardhewqufeqw', data)

  const paths = data.map(user => {
    return { params: { forestName: user.slug }}
  })
  return {
    paths: paths,
    fallback: false
  }
}

// You should use getStaticProps when:
//- The data required to render the page is available at build time ahead of a user’s request.
//- The data comes from a headless CMS.
//- The data can be publicly cached (not user-specific).
//- The page must be pre-rendered (for SEO) and be very fast — getStaticProps generates HTML and JSON files, both of which can be cached by a CDN for performance.
export const getStaticProps = async (ctx) => {
  const contentIndex = await client.getEntry('3lVOzDkGzNuBM45VVwIDtm')
  return {
    props: {
      content: contentIndex
    }
  }
}
