import Header from "../components/layout/Header/Header";
import LogoWithCounter from "../components/ui/LogoWithCounter/LogoWithCounter";
import Footer from "../components/layout/Footer/Footer";
import bannerContent from "../data/data.json";
import Banner from "../components/ui/Banner/Banner";
import Forest from "../components/partials/Forest";
import FooterInfo from "../components/partials/FooterInfo";
import MainContent from "../components/partials/MainContent";
import * as contentfull from 'contentful'
import User from "../models/User";
import dbConnect from "../lib/dbConnect";
import { useSession } from "next-auth/react";

export default function Home(props) {	
	return (
		<>
			<Header>
				<LogoWithCounter number={props.forest.treesPlanted } forestName={"Overview"} />
			</Header>

      <Forest trees={props.forest.treesPlanted} />

      <MainContent background="/img/trees.png" content={props.content} />

      <Banner trees={props.forest.treesPlanted} content={bannerContent} />

			<Footer>
          <FooterInfo />
			</Footer>
		</>
	)
}
var client = contentfull.createClient({
  space: 'kq6vfz8ffqad',
  accessToken: 'RRSlBA1ViSrwWlBROgru5F9VQYUja8SO7dhbQyk2jmg'
})



export async function getServerSideProps() {
  const treesPlanted = await fetch(
    "https://v3.morningscore.io/storage/count.json"
  )
    .then((response) => response.json())
    .then((data) => data.trees);

   const contentIndex = await client.getEntry('3lVOzDkGzNuBM45VVwIDtm')
   
   await dbConnect()

   /* find all the data in our database */
   const result = await User.find({})
   const users = result.map((doc) => {
     const user = doc.toObject()
     user._id = user._id.toString()
     return user
   }) 
   

  return {
    props: {
      forest: { _id: 1, name: "Morningscore", treesPlanted: treesPlanted },
      content: contentIndex,
      users: users
    },
  };
}
