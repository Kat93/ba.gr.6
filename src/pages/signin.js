import { getCsrfToken, signIn } from 'next-auth/react'
import React, { useState } from 'react'
import { useRouter } from 'next/router'

export default function SignIn({ csrfToken }) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')
    const router = useRouter()

    const signInUser = async (e) => {
        e.preventDefault()
        console.log(email, password)
        const options = {
            email,
            password,
            redirect: false
        }
        const res = await signIn("credentials", options)

        console.log(res)
        
        router.push("/")

    }
    
  return (
        <form>
        {/* <form method="post" action="/api/auth/callback/credentials"> */}
        <input type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
        <input type="text" value={password} onChange={(e) => setPassword(e.target.value)} />
        {/* <button type="submit">Sign in</button> */}
        <button onClick={(e) => signInUser(e)}>Sign in</button>
        <p>{message}</p>
    </form>
  )
}


export async function getServerSideProps(context) {

    const token = await getCsrfToken(context)
    console.log('token:', token)
    return {
      props: {
        csrfToken: token,
      },
    }
  }