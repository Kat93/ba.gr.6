import NextAuth from "next-auth"
import GithubProvider from "next-auth/providers/github"
import CredentialsProvider from "next-auth/providers/credentials"
import dbConnect from "../../../lib/dbConnect"
import UserModel from "../../../models/User"


export const authOptions = {
  session: {
    strategy: "jwt"
  },
  // Configure one or more authentication providers
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      type: 'credentials',
      credentials: {
        username: { label: "Username", type: "text", placeholder: "jsmith" },
        password: {  label: "Password", type: "password" },
        email: {label: "Email", type: "text"}
      },
      async authorize(credentials, req) {
        const {email, password} = credentials

        // const user = {id: 1, name: "hans", email: "swag@swag.swag", slug: "sluggg"}

        await dbConnect()

        const user = await UserModel.findOne({email})
        console.log('KIG HER !!! ', user)
    
        if(!user) {
          throw new Error("User not found")
        }
    
        if(!password){
            throw new Error("Please enter password")
        }
    
        const isMatch = password === user.password
    
        if(!isMatch){
            throw new Error("Password doesnt match")
        }
        
        return user
      }
    })
  ],
  pages: {
    signIn: "/signin"
  },
  callbacks: {
    async jwt({ token, user }) {
      console.log('token user', user)
      if (user) {
        token.token = user
        token.user = user
      }
      return token
    },
  async session({ session, token, user }) {
    // Send properties to the client, like an access_token and user id from a provider.
    session.user = token.user
    
    return session
  },
}
}

export default NextAuth(authOptions)