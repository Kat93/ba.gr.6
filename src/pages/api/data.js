
import {apiData} from "../../data/apiData"

export default function handler(req, res) {
    res.status(200).json(apiData);
  }