import { connectMongo } from "../../../lib/connectDb"
import dbConnect from "../../../lib/dbConnect"
import User from "../../../models/User"

export default async function addUser(req, res) {
    const { name } = req.body
    console.log('Connecting to mongo', req.body)
    //await connectMongo()
    await dbConnect()
    console.log('Connected to mongo', name)

    console.log('Creating document', name)
    const hat = await User.create(req.body)
    console.log('Document created', name)

    res.json(hat)

}