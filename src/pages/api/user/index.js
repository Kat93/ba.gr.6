import dbConnect from "../../../lib/dbConnect"
import User from "../../../models/User"

export default async function getUsers (req, res) {
    await dbConnect()

    const users = await User.find()

    res.status(200).json(users)
}