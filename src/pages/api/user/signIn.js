import { userEvent } from "@storybook/testing-library"
import dbConnect from "../../../lib/dbConnect"
import User from "../../../models/User"

// TODO: Change error handling to respons to API request

export default async function signInUser(req, res) {
    const { email, password} = req.body
    
    await dbConnect()
    const currentUser = await User.findOne({email})
    
    if(!currentUser) {
      throw new Error("User not found")
    }

    if(!currentUser.password){
        throw new Error("Please enter password")
    }

    const isMatch = password === currentUser.password

    if(!isMatch){
        throw new Error("Passwod doesnt match")
    }

    res.json(currentUser)

}