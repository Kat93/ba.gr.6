import { signIn } from 'next-auth/react'
import React, { useState } from 'react'
import { useRouter } from 'next/router'
import LoginForm from '../components/ui/forms/LoginForm/LoginForm'
import AnimatedFullScreenWrapper from '../components/layout/AnimatedFullscreenWrapper/AnimatedFullscreenWrapper'

export default function SignIn() {

    
  return (
    <>
    <AnimatedFullScreenWrapper>
    <LoginForm headingText={"Sign into Morningwood"} headingLevel="h3" formLayout={"centered"} formSize={"small"} />
    </AnimatedFullScreenWrapper>
    </>
  )
}