import { useEffect, useState } from "react"
import TreeSelector from "../components/forest/TreeSelector/TreeSelector"

const useTreePlanter = (trees) => {
    const [plantedTrees, setPlantedTrees] = useState(trees)
  
    useEffect(() => {
      const renderTrees = []
      for (let treesLeft = trees; treesLeft > 0; treesLeft--) {
        let value = 0
        let width = "100%"
        if(treesLeft >= 2000) {
          width = "70%"
          value = 1000
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 220) {
          width = "40%"
          value = 100
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 120) {
          width = "90%"
          value = 50
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 30) {
          width = "50%"
          value = 20
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 15) {
          width = "40%"
          value = 10
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 6) {
          width = "25%"
          value = 5
          treesLeft -= (value-1)
        }
        else if(treesLeft >= 1) {
          width = "20%"
          value = 1
          treesLeft -= (value-1)
        }

        // console.log(`this tree is worth ${value}`, treesLeft)
    
        renderTrees.push(<TreeSelector width={width} value={value} />)
      }
      
      setPlantedTrees(renderTrees)
      
    }, [trees])
  
  return (
    {plantedTrees}
  )
}

export default useTreePlanter