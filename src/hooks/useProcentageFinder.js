import { useState, useEffect } from "react";
const useProcentageFinder = (number, goal) =>{
  if (
    typeof number !== 'number' || typeof goal !== 'number' 
  ) {
    throw new Error('These must be of type number!');
  }

  const [completed, setCompleted] = useState(0);
  useEffect(() => {
    setCompleted((number / goal) * 100);
  }, [number, goal]);
  return completed;
}

export default useProcentageFinder
