import useProcentageFinder from "./useProcentageFinder";
import { renderHook } from "@testing-library/react";

describe("This is a test that checks if useProcentageFinder works properly", () => {
    it("Check if it render equal numbers to the right procentage", () => {
        const { result } = renderHook(() => useProcentageFinder(500000, 1000000))

        expect(result.current).toBe(50)
    })

    it("Check if it not only renders 50 procentage ", () => {
        const { result } = renderHook(() => useProcentageFinder(36789, 1000000))

        expect(result.current).not.toBe(50)
    })

    it("Check if it renders unequal numbers", () => {
        const { result } = renderHook(() => useProcentageFinder(58783, 1000000))

        expect(result.current).toBe(5.8783)
    })

    it("Check if it can render a bigger goal", () => {
        const { result } = renderHook(() => useProcentageFinder(58783, 10000000))

        expect(result.current).toBe(0.5878300000000001)
    })

  
})